using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace LoadData
{
    public class SubWave
    {
        public int totalMonster;
        public List<MonsterData> monsters;
    }
    public class Wave
    {
        public int totalMonster;
        public List<MonsterData> monsters;
        public List<SubWave> subWave;
    }
    public class Resource
    {
        public int paper;
        public int plastic;
        public int compost;
        public int glass;
        public int metal;

    }

    public class LevelData
    {
        public String background;
        public int roboUnlock;
        public string assistantUnlock;
        public int threeStar;
        public int twoStar;
        public Resource resource;
        public int totalWave;
        public List<Wave> waves;
    }
}
