﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData
{
    public class AssistantInfo
    {
        public string type;
        public int defence;
        public int speed;
        public int damage;
        public int heal;
    }

    public class AssistantData
    {
        public List<AssistantInfo> Assistants;
    }
}
