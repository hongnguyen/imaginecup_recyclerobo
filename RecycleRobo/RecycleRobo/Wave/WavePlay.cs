using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class WavePlay
    {
        GameManager game;
        public List<Monster[]> listMonster = new List<Monster[]>();
        public Monster[] monsters;
        public int numberMonsterInArray = 0;
        int timeToNext = 0;
        public AssNormal assCollect;


        private MouseState previousMouseState;
        public WavePlay(GameManager g, List<Monster[]> listMonster)
        {
            assCollect = new AssNormal();
            this.game = g;
            this.listMonster = listMonster;

            numberMonsterInArray += listMonster[0].Length;
            monsters = new Monster[numberMonsterInArray];
            for (int i = 0; i < listMonster[0].Length; i++)
            {
                monsters[i] = listMonster[0][i];
            }
            listMonster.RemoveAt(0);
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;


        }

        public void Update(GameTime gameTime, Player[] player, List<Item> sewages)
        {
            //Console.WriteLine("CCCCCC" + listMonster.Count);
            timeToNext += gameTime.ElapsedGameTime.Milliseconds / 16;
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            MouseState curMouseState = Mouse.GetState();

            for (int i = 0; i < player.Length; i++)
            {

                if (player[i].isAlive)
                {
                    if (player[i].GetType().Name == "NumberFour")
                    {
                        NumberFour numberfourTmp = (NumberFour)player[i];
                        numberfourTmp.UpdateEvent(gameTime, player);
                    }
                    else
                        player[i].UpdateEvent(gameTime, monsters);
                    player[i].checkCursorTarget(monsters);
                    player[i].CheckOrderWithEnemy(monsters);
                    player[i].checkDead();
                }
            }

            GetTheTopRobot(player);

            for (int i = 0; i < monsters.Length; i++)
            {
                if (monsters[i] !=null && monsters[i].isAlive)
                {
                    monsters[i].checkAlive(sewages);
                    monsters[i].updatePosition(gameTime);
                }
            }
            if (timeToNext > new Random().Next(600,800) && listMonster.Count > 0)
            {
                int newsize = numberMonsterInArray + listMonster[0].Length;
                Array.Resize<Monster>(ref monsters, newsize);
                for (int i = 0; i < listMonster[0].Length; i++)
                {
                    monsters[numberMonsterInArray++] = listMonster[0][i];
                }
                listMonster.RemoveAt(0);
                timeToNext = new Random().Next(-100,100);
            }

            foreach (Item sewage in sewages.ToArray())
            {
                sewage.UpdatePosition(gameTime);
                if (sewage.assCollect.isDone)
                {
                    sewages.Remove(sewage);
                }
            }


            previousMouseState = curMouseState;

        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, List<Item> sewages)
        {
            assCollect.Draw(spriteBatch);
            for (int i = 0; i < monsters.Length; i++)
                if (monsters[i]!=null)
                    monsters[i].Draw(gameTime, spriteBatch);

            foreach (Item item in sewages)
            {
                item.Draw(spriteBatch);
            }
        }
        public bool isOver()
        {
            if (listMonster.Count > 0)
            {
                return false;
            }
            for (int i = 0; i < this.monsters.Length; i++)
            {
                if (this.monsters[i].isAlive)
                    return false;
            }
            return true;
        }
        public void GetTheTopRobot(Player[] player)
        {
            float minOrderLayer = 10;
            Player minOrderRobot = null;

            for (int i = 0; i < player.Length; i++)
            {
                if (player[i].curHealth <= 0) continue;
                if (player[i] != null)
                {
                    if (player[i].isSelected == true && player[i].sprite.OrderLayer < minOrderLayer)
                    {
                        minOrderLayer = player[i].sprite.OrderLayer;
                        minOrderRobot = player[i];
                    }
                    player[i].isSelected = false;
                    player[i].sprite.color = Color.White;
                }
            }
            if (minOrderRobot != null)
            {
                minOrderRobot.isSelected = true;
                //minOrderRobot.sprite.color = minOrderRobot.colorSelected;
            }
        }

    }
}
