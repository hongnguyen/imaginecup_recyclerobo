﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;

namespace RecycleRobo.Scenes.ChallengeItem
{
    public class Challengeitem
    {
        public GameManager game;

        private Vector2[] Pos;
        private Texture2D[] IconBackground;
        private Rectangle[] BoundingBox;
        private List<Texture2D> Allitems;
        private int[] completed;
        public static float scale = 1.0f;

        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };
        private Rectangle clippingRec;

        private MouseState cur;
        private MouseState pre;

        private int TextBoxID;
        private Rectangle borderRectangle;
        private ChallengeScene challenge;

        private Texture2D tick;
        public SpriteFont font;
        private bool inside;
        private bool isCompletedItems;

        public Challengeitem(int[] completed, GameManager game, Rectangle clippingRec, ChallengeScene challenge,
            List<Texture2D> AllItem, bool isCompletedItems) 
        {
            this.challenge = challenge;
            this.game = game;
            this.clippingRec = clippingRec;
            this.completed = completed;
            this.Allitems = AllItem;
            this.isCompletedItems = isCompletedItems;
            borderRectangle = new Rectangle((int)clippingRec.X, (int)GlobalClass.ScreenHeight - 250,
                (int)((GlobalClass.ScreenWidth - clippingRec.X) - 10), 200);
            loadChallenge();

            font = game.Content.Load<SpriteFont>("newFont");
            TextBoxID = -1;
            inside = false;
        }

        public void loadChallenge() 
        {
            Pos = new Vector2[Allitems.Count()];
            if(Pos.Length > 0)
                Pos[0] = new Vector2(clippingRec.X, clippingRec.Y);
            IconBackground = new Texture2D[Allitems.Count()];
            BoundingBox = new Rectangle[Allitems.Count];
            tick = game.Content.Load<Texture2D>("Challenge/tick");
            for (int i = 0; i < Allitems.Count; i++)
            {
                IconBackground[i] = game.Content.Load<Texture2D>("Challenge/ChallengeAccepted");
                if (i >= 1) Pos[i] = new Vector2(Pos[i - 1].X + IconBackground[i].Width * scale + 20, clippingRec.Y);
                BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clippingRec.Y,
                    (int)(IconBackground[i].Width * scale),
                    (int)(IconBackground[i].Height * scale));
            }
            for (int i = 0; i < Allitems.Count(); i++)
            {
                if (completed.Contains(i) || isCompletedItems)
                {
                    IconBackground[i] = game.Content.Load<Texture2D>("Challenge/ChallengeCompleted");
                }
            }
        }
        public List<String> getText(int ID)
        {
            String[] typesOfRes = { "plastic", "paper", "glass", "compost", "metal" };
            List<String> result = new List<string>();
            //result.Add("Challenge number " + (ID + 1));
            if (ID == 0) 
            {
                result.Add("PLASTIC-MAN");
                result.Add("Collect 50 plastic materials from general waste stream equipping Plastic KIT.");
            }
            else if (ID == 1) 
            {
                result.Add("PLASTIC-MAN RETURN");
                result.Add("Collect 100 plastic materials from general waste stream.");
            }
            else if (ID == 2) 
            {
                result.Add("TONY STARK");
                result.Add("Collect 20 metal materials from general waste stream in one game.");
            }
            else if (ID == 3) 
            {
                result.Add("TONY STARK FOREVER");
                result.Add("Collect 150 metal materials from general waste stream.");
            }
            String reward = "Reward : ";
            for (int j = 0; j < typesOfRes.Length; j++) 
            {
                if (ChallengeScene.addResource[ID, j] > 0)
                {
                    reward += ChallengeScene.addResource[ID, j] + " " + typesOfRes[j] + ", ";
                }
            }
            reward = reward.Remove(reward.Length-2,2);
            result.Add(reward);
            if (completed.Contains(ID) || isCompletedItems)
            {
                result.Add("\nYou have finished this Challenge.");
            }
            return result;
        }

        public void Update(GameTime gametime) 
        {
            cur = Mouse.GetState();
            if (!clippingRec.Contains(new Point((int)MouseHelper.MousePosition(cur).X, (int)MouseHelper.MousePosition(cur).Y)))
            {
                inside = false;
                return;
            }
            else 
            {
                inside = true;
            }


            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Released)
            {
                for (int i = 0; i < Allitems.Count; i++)
                {
                    if ((BoundingBox[i]).Contains(new Point(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y)))
                    {
                        TextBoxID = i;
                        IconBackground[i] = game.Content.Load<Texture2D>("Recycle/selectedState");
                    }
                    else
                    {
                        if(completed.Contains(i) || isCompletedItems)
                            IconBackground[i] = game.Content.Load<Texture2D>("Challenge/ChallengeCompleted");
                        else 
                            IconBackground[i] = game.Content.Load<Texture2D>("Challenge/challengeAccepted");
                    }
                }
            }

            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Pressed)
            {
                Vector2 Target = new Vector2(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2(MouseHelper.MousePosition(pre).X, MouseHelper.MousePosition(pre).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 40)
                {
                    direction.Normalize();
                    direction = direction * 40;
                }
                if (Pos.Length > 0)
                {
                    if (Pos[0].X > clippingRec.X + 10 && direction.X > 0) return;
                    if (Pos[Pos.Count() - 1].X + IconBackground[0].Width < GlobalClass.ScreenWidth
                        && direction.X < 0) return;
                }

                for (int i = 0; i < Allitems.Count; i++)
                {
                    Pos[i] = new Vector2((Pos[i]).X + (direction.X), (Pos[i]).Y);
                    BoundingBox[i] = (new Rectangle((int)((Pos[i]).X), (int)((Pos[i]).Y),
                    (IconBackground[i]).Width, (IconBackground[i]).Height));
                }
            }
            pre = cur;
        }

        public void DrawTextBox(SpriteBatch spriteBatch, int ID)
        {
            if (!inside) return;
            if (TextBoxID == -1) return;
            Texture2D boxTexture = game.Content.Load<Texture2D>("Recycle/infoBackground");
            spriteBatch.Draw(boxTexture, borderRectangle, null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
            List<String> textToWrite = getText(ID);
            Color[] colors = {Color.White,Color.Red,Color.Purple,Color.RoyalBlue};

            for (int i = 0; i < textToWrite.Count; i++)
            {
                String text = textToWrite[i];
                Vector2 size = font.MeasureString(text);
                spriteBatch.DrawString(font, textToWrite[i],
                    new Vector2(borderRectangle.X + borderRectangle.Width / 2 - size.X / 2, borderRectangle.Y + 10 + i * 40)
                    , colors[i]);
            }
        }

        public void DrawChallenge(GameTime gametime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                      null, null, _rasterizerState, null, Resolution.getTransformationMatrix());
            spriteBatch.GraphicsDevice.ScissorRectangle = clippingRec;
            for (int i = 0; i < Allitems.Count; i++)
            {
                int pos1 = (int)(Pos[i]).X + (int)((IconBackground[i]).Width / 2 - (Allitems[i]).Width / 2 * 0.5);

                int pos2 = (int)(Pos[i]).Y + (int)((IconBackground[i]).Height / 2 - (Allitems[i]).Height / 2 * 0.5);

                int width = (int)((Allitems[i]).Width*0.5);

                int height = (int)((Allitems[i]).Height*0.5);
                spriteBatch.Draw((IconBackground[i])
                    , (Rectangle)BoundingBox[i]
                    , null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                spriteBatch.Draw((Allitems[i]), new Rectangle(pos1, pos2, width, height),
                    null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                if (completed.Contains(i) || isCompletedItems) 
                {
                    spriteBatch.Draw(tick, new Rectangle(pos1, pos2, width, height),
                        Color.White);
                }
            }
            DrawTextBox(spriteBatch, TextBoxID);
            spriteBatch.End();
        }
    }
}
