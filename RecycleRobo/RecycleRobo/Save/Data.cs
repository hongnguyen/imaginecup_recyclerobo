﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace RecycleRobo
{
    public class Data
    {
        private string runningDirectoryPath;

        public Data() {
            runningDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public T Load<T>(string filename)
        {

            string saveGameFileFullPath = Path.Combine(runningDirectoryPath, filename);

            try
            {
                if (!File.Exists(saveGameFileFullPath))
                {
                    return default(T);
                }
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                using (FileStream stream = File.Open(saveGameFileFullPath, FileMode.Open, FileAccess.Read))
                {
                    return (T)serializer.Deserialize(stream);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Save(object saveData, string filename)
        {
            string saveGameFileFullPath = Path.Combine(runningDirectoryPath, filename);
            try
            {

                XmlSerializer serializer = new XmlSerializer(saveData.GetType());

                using (FileStream stream = File.Open(saveGameFileFullPath, FileMode.Create, FileAccess.Write))
                {
                    serializer.Serialize(stream, saveData);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public T LoadEncrypt<T>(string filename)
        {
            string saveGameFileFullPath = Path.Combine(runningDirectoryPath, filename);

            try
            {
                if (!File.Exists(saveGameFileFullPath)) return default(T);

                using (FileStream stream = new FileStream(saveGameFileFullPath, FileMode.Open))
                {
                    BinaryFormatter formater = new BinaryFormatter();
                    return (T)formater.Deserialize(stream);
                }
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveEncrypt(object saveData, string filename)
        {
            string saveGameFileFullPath = Path.Combine(runningDirectoryPath, filename);
            try
            {
                using (FileStream stream = new FileStream(saveGameFileFullPath, FileMode.Create))
                {
                    BinaryFormatter formater = new BinaryFormatter();
                    formater.Serialize(stream, saveData);
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                    formater = null;
                }
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
