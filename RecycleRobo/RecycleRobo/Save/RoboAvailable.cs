﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class RoboAvailable
    {
        public int[] available;
        public RoboAvailable() { }
        public RoboAvailable(int[] value)
        {
            available = value;
        }
    }
}
