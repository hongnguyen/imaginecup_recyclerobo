﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class Level
    {
        public bool isUnlock;
        public int rating;

        public Level() { }
        public Level(bool value, int pRating)
        {
            isUnlock = value;
            rating = pRating;
        }
    }
}
