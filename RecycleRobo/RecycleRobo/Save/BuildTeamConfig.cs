﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    [Serializable]
    public class BuildTeamConfig
    {
        public string nameRobo;
        public string nameAss;
        public Vector2 positionRobo;
        public Vector2 positionAss;
        public int index_slot;
        public BuildTeamConfig() { }
        public BuildTeamConfig(string namerobo, string nameass, Vector2 pos, Vector2 positionAss, int index_slot)
        {
            this.nameRobo = namerobo;
            this.nameAss = nameass;
            this.positionRobo = pos;
            this.positionAss = positionAss;
            this.index_slot = index_slot;
        }
    }
}
