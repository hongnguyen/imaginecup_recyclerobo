﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo.MGame.Carousel
{
    public class Garbage
    {
        public string type;
        public Vector2 position;
        public Texture2D texture;
        public bool isUsed = false;
        public float scale = 0.2f;
        public bool isShowType = false;
        MouseState currentMouseState, preMouseState;
        public Texture2D typeShow;
        GameManager game;
        SoundEffect effectCorrect, effectFailure;
        public int speed = 1;
        Random rd = new Random();
        float rotate = 0.0f;
        bool isDown = true;
        bool isUp = false;
        Vector2 assPos;
        bool isShowAmount = false;
        public int bonus = 30;

        public bool isOut = false;

        public Garbage(GameManager game, string t, Vector2 position, int speed, int bonus)
        {
            this.rotate = (float)rd.Next(-50,50) / 100;
            this.game = game;
            this.type = t;
            this.bonus = bonus;
            this.speed = speed;
            this.texture = game.Content.Load<Texture2D>("Minigame\\Garbage\\" + type);
            this.position = position;
            effectCorrect = Asset.correctSound;
            effectFailure = Asset.wrongSound;
            assPos = position + new Vector2(0,-15);
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            
        }
        public void Update(GameTime gametime)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            Vector2 direction = new Vector2(position.X, GlobalClass.ScreenHeight - 40) - position;
            Vector2 directionUp = new Vector2(position.X, -250) - assPos;
            Vector2 directionDown = new Vector2(position.X, 200) - assPos;
            directionUp.Normalize();
            directionDown.Normalize();
            direction.Normalize();
            if (assPos.Y < 200 && isDown)
            {
                assPos += directionDown * (float)gametime.ElapsedGameTime.TotalMilliseconds * speed/60;
            }
            else if(isUp && assPos.Y > -250){
                assPos += directionUp * (float)gametime.ElapsedGameTime.TotalMilliseconds * speed/15;
            }
            if (assPos.Y >= 200)
            {
                isDown = false;
                isUp = true;
            }
            if (position.Y > 200 && position.Y < 300)
            {
                isShowAmount = true;
            }

            if (position.Y > 300)
            {
                isShowAmount = false;
            }
            if (position.Y < GlobalClass.ScreenHeight - 40)
            {
                position += direction * (float)gametime.ElapsedGameTime.TotalMilliseconds * speed/60;
            }
            currentMouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(currentMouseState).Y);
            if (checkBoundKey(mousePosition))
            {
                this.scale = 0.5f;
                typeShow = game.Content.Load<Texture2D>("Minigame\\Hide\\" + this.type.Remove(this.type.Length - 1, 1));
                isShowType = true;
            }
            else
            {
                this.scale = 0.47f;
                isShowType = false;
            }

            preMouseState = currentMouseState;
        }
        public bool checkMatchKeyAndLock(Bins bins)
        {
            if (bins.type.Contains(this.type.Remove(this.type.Length - 1, 1)) || bins.type.Equals("all"))
            {
                return true;
            }
            return false;
        }
        public Bins getLockTarget(Vector2 mousePosition, List<Bins> lockList)
        {
            foreach (Bins lockItem in lockList)
            {
                if (checkBoundLock(mousePosition,lockItem))
                    return lockItem;
            }

            return null ;

        }

        public bool checkBoundKey(Vector2 mousePosition)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);

            Rectangle box = new Rectangle((int)position.X, (int)position.Y, Convert.ToInt32(texture.Width*scale), Convert.ToInt32(texture.Height*scale));
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public bool checkBoundLock(Vector2 mousePosition, Bins lockItem)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);
            Rectangle box = new Rectangle((int)lockItem.position.X, (int)lockItem.position.Y, Convert.ToInt32(lockItem.texture.Width), Convert.ToInt32(lockItem.texture.Height));
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Asset.Ass_normal, assPos, null, Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 0.4f);


            if (isShowType && !this.isUsed)
            {
                spriteBatch.Draw(typeShow, new Vector2(position.X + texture.Width*scale + 10, position.Y), null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.5f);

            }
            if (isShowAmount)
            {
               // spriteBatch.DrawString(Asset.mediumFont, "+" + Convert.ToString(bonus) + " " + this.type.Remove(this.type.Length - 1, 1), new Vector2(position.X + 60, position.Y), Color.Black);
            }
            if(!this.isUsed)
                spriteBatch.Draw(texture, position, null, Color.White, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.5f);
        }
        
    }
}
