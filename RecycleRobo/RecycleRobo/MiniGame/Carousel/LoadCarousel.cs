﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData;
using RecycleRobo.Scenes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.MGame.Carousel;
namespace RecycleRobo
{
    public class LoadCarousel
    {
        GameManager game;
        int[] column;
        string[] typeGarbage = { "compost0", "glass0", "compost1", "metal1", "metal0", "paper0", "plastic0", "compost1", "glass1", "metal1", "paper1", "plastic1" };
        public LoadCarousel(GameManager g) { game = g; }
        public void loadData(List<Garbage> gbList, List<Bins> binsList)
        {
            Random rd = new Random();
            int[] xPos = {330, (int)GlobalClass.ScreenWidth/ 2 - 30, (int)GlobalClass.ScreenWidth - 410};
            column = new int[rd.Next(3, 5)];
            for (int i = 0; i < column.Length; i++)
            {
                column[i] = rd.Next(2, 5);
            }
            Dictionary<int, string[]> mapGarbage = new Dictionary<int, string[]>();
            string[] one = { "compost0", "glass0", "compost1", "metal0", "paper0" };
            string[] two = { "compost1", "glass1", "compost1", "metal1", "paper1" };
            string[] three = { "compost0", "glass0", "compost1", "metal1", "metal0" };
            string[] four = { "compost0", "glass0", "compost1", "metal1", "metal0" };
            string[] five = { "compost0", "glass0", "compost1", "metal1", "metal0" };

            mapGarbage.Add(1, one);
            mapGarbage.Add(2, two);
            mapGarbage.Add(3, three);
            mapGarbage.Add(4, four);
            int yPos = -100;
            int t = 0;
            for (int i = 0; i < column.Length; i++)
            {
                for (int j = 0; j < column[i]; j++ )
                {
                    gbList.Add(new Garbage(game, mapGarbage[i+1][j], new Vector2(xPos[t++ % xPos.Length], yPos + rd.Next(100)), rd.Next(3,8), rd.Next(10,30)));
                    yPos -= rd.Next(100,200);
                }
                yPos -= 100 + rd.Next(100);
            }
            int type = rd.Next(0,4);
            binsList.Add(new Bins(this.game, Bins.typeBinInit[type], new Vector2(300, GlobalClass.ScreenHeight - 150)));
            binsList.Add(new Bins(this.game, Bins.typeBinInit[++type % 5], new Vector2(GlobalClass.ScreenWidth/2 - 50, GlobalClass.ScreenHeight - 150)));
            binsList.Add(new Bins(this.game, Bins.typeBinInit[++type % 5], new Vector2(GlobalClass.ScreenWidth - 430, GlobalClass.ScreenHeight - 150)));

        }
    }
}
