using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using RecycleRobo.MGame.Carousel;

namespace RecycleRobo
{

    public class Carousel: MiniGame
    {
        GameManager game;
        List<Bins> binsList;
        List<Garbage> garbageList;
        Background bg;
        Texture2D guide, energyBar;
        LiveTile tile1, tile2;
        Texture2D guideTexture, barTexture;
        public Carousel(GameManager game)
        {
            this.game = game;
            guideTexture = game.Content.Load<Texture2D>("Minigame/Guide/carousel");
            barTexture = game.Content.Load<Texture2D>("Minigame/energybar");
            binsList = new List<Bins>();
            garbageList = new List<Garbage>();
            bg = new Background(game.Content, "MiniGame/background", 1f);
            energyBar = game.Content.Load<Texture2D>("MiniGame\\Catcher\\energy bar");
            tile1 = new LiveTile(game,1);
            tile2 = new LiveTile(game,4);
            tile1.position = new Vector2(0, 0);
            tile2.position = new Vector2(GlobalClass.ScreenWidth - 258, 0);
            Load();
        }
        

        public override void Play(GameTime gameTime)
        {
            tile1.Update(gameTime);
            tile2.Update(gameTime);

            foreach (Bins bin in binsList)
            {
                bin.Update(gameTime, garbageList, binsList);
            }
            foreach (Garbage gb in garbageList.ToArray())
            {
                if (gb.position.Y >= GlobalClass.ScreenHeight - 40)
                {
                    gb.isOut = true;
                }
                gb.Update(gameTime);
            }

        }
        public override bool isWin(){
            foreach (Garbage gb in garbageList)
                if (!gb.isOut)
                    return false;
            return true;
        }
        private void Load()
        {
            LoadCarousel minigame;
            minigame = new LoadCarousel(game);
            minigame.loadData(garbageList, binsList);
        }
        public override int calculateEnergyBall()
        {
            return Bins.energyBall;
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            bg.Draw(spriteBatch);
            tile1.Draw(spriteBatch);
            tile2.Draw(spriteBatch);
            spriteBatch.Draw(guideTexture, new Vector2(0, GlobalClass.ScreenHeight - guideTexture.Height * 0.625f), null, Color.White, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.9f);
            spriteBatch.Draw(barTexture, new Vector2(GlobalClass.ScreenWidth - barTexture.Width * 0.625f, GlobalClass.ScreenHeight - barTexture.Height * 0.625f), null, Color.White, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.9f);

            //spriteBatch.Draw(guide, new Vector2(0,GlobalClass.ScreenHeight - guide.Height*0.625f), null, Color.White, 0.0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.8f);
            //spriteBatch.Draw(energyBar, new Vector2(GlobalClass.ScreenWidth - energyBar.Width*0.625f , GlobalClass.ScreenHeight - energyBar.Height * 0.625f), null, Color.White, 0.0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.8f);
            spriteBatch.DrawString(Asset.fontMinigame, Convert.ToString(Bins.energyBall), new Vector2(GlobalClass.ScreenWidth - 150, GlobalClass.ScreenHeight - 80), Color.DimGray);

            foreach (Bins bin in binsList)
            {
                bin.Draw(gameTime,spriteBatch);
            }
            foreach (Garbage gb in garbageList)
            {
                gb.Draw(spriteBatch);
            }
        }
    }
}
