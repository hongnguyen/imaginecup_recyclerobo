using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.MGame.Circle;
using RecycleRobo.Scenes;

namespace RecycleRobo
{

    public class Circle: MiniGame
    {
        GameManager game;
        List<Bins> binsList;
        List<List<Garbage>> garbageList;
        Background bg;
        Texture2D guide, energyBar;
        LiveTile tile1, tile2;
        Texture2D guideTexture, barTexture;
        bool isClockWise = true;
        MouseState curMouse, preMouse;
        public Circle(GameManager game)
        {
            this.game = game;
            guideTexture = game.Content.Load<Texture2D>("Minigame/Guide/circle");
            barTexture = game.Content.Load<Texture2D>("Minigame/energybar");
            binsList = new List<Bins>();
            garbageList = new List<List<Garbage>>();
            energyBar = game.Content.Load<Texture2D>("MiniGame\\Catcher\\energy bar");
            tile1 = new LiveTile(game,2);
            tile2 = new LiveTile(game,4);
            tile1.position = new Vector2(0, 0);
            tile2.position = new Vector2(GlobalClass.ScreenWidth - 258, 0);
            bg = new Background(Asset.bgMiniGame, 0.091f);

            Load();
        }
        

        public override void Play(GameTime gameTime)
        {
            curMouse = Mouse.GetState();

            tile1.Update(gameTime);
            tile2.Update(gameTime);
            if (!isWin())
            {
                foreach (Garbage gb in garbageList[0])
                {
                    gb.Update(gameTime, binsList);
                }

                foreach (Bins bin in binsList)
                {
                    bin.Update(gameTime, garbageList[0], binsList);
                    if (curMouse.LeftButton == ButtonState.Pressed)
                    {
                        bin.rotate -= (float)gameTime.ElapsedGameTime.TotalMilliseconds / 180;
                    }
                    else if (curMouse.RightButton == ButtonState.Pressed)
                    {
                        bin.rotate += (float)gameTime.ElapsedGameTime.TotalMilliseconds / 180;
                    }
                }
                if (nextWave(garbageList[0]))
                {
                    garbageList.RemoveAt(0);
                }
                
            }

            preMouse = curMouse;

        }
        public bool nextWave(List<Garbage> gbLits)
        {
            foreach (var gb in gbLits)
	        {
                if (gb.isUpdate)
                {
                    return false;
                }
        	}
            return true;
        }
        public override bool isWin(){
            if (garbageList.Count == 0)
            {
                return true;
            }
            return false;
        }
        private void Load()
        {
            LoadCircle minigame;
            minigame = new LoadCircle(game);
            minigame.loadData(garbageList, binsList);
        }
        public override int calculateEnergyBall()
        {
            return Bins.energyBall;
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //bg.Draw(spriteBatch);
            tile1.Draw(spriteBatch);
            tile2.Draw(spriteBatch);
            spriteBatch.Draw(guideTexture, new Vector2(0, GlobalClass.ScreenHeight - guideTexture.Height * 0.625f), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.038f);
            spriteBatch.Draw(barTexture, new Vector2(GlobalClass.ScreenWidth - barTexture.Width * 0.625f, GlobalClass.ScreenHeight - barTexture.Height * 0.625f), null, Color.White, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.038f);
            bg.Draw(spriteBatch);
            spriteBatch.Draw(Asset.bgMiniGameForce3, new Vector2(262, 0), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);
            
            //spriteBatch.Draw(guide, new Vector2(0,GlobalClass.ScreenHeight - guide.Height*0.625f), null, Color.White, 0.0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.8f);
            //spriteBatch.Draw(energyBar, new Vector2(GlobalClass.ScreenWidth - energyBar.Width*0.625f , GlobalClass.ScreenHeight - energyBar.Height * 0.625f), null, Color.White, 0.0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.8f);
            spriteBatch.DrawString(Asset.fontMinigame, Convert.ToString(Bins.energyBall), new Vector2(GlobalClass.ScreenWidth - 150, GlobalClass.ScreenHeight - 80), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.03f);

            foreach (Bins bin in binsList)
            {
                bin.Draw(gameTime,spriteBatch);
            }
            if (garbageList.Count > 0)
            {
                foreach (Garbage gb in garbageList[0])
                {
                    gb.Draw(spriteBatch, gameTime);
                }
            }

        }
    }
}
