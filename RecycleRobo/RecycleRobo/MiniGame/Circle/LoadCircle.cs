﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.MGame.Circle;
using RecycleRobo.Scenes;
namespace RecycleRobo
{
    public class PartCircle
    {
        public string type;
        public int amount;
        public PartCircle(int amount, string type)
        {
            this.type = type;
            this.amount = amount;
        }
    }
    public class LoadCircle
    {
        GameManager game;
        int[] column;
        public LoadCircle(GameManager g) { game = g; }
        public void loadData(List<List<Garbage>> garbageList, List<Bins> binsList)
        {
            Random rd = new Random();
            Dictionary<int, List<PartCircle>> setCircle = new Dictionary<int, List<PartCircle>>();
            List<PartCircle> setCircleOne = new List<PartCircle>();
            List<Garbage> gbList = new List<Garbage>();
            setCircleOne.Add(new PartCircle(2, "compost"));
            setCircleOne.Add(new PartCircle(4, "metal"));
            setCircleOne.Add(new PartCircle(4, "glass"));

            setCircle.Add(1, setCircleOne);

            setCircleOne = new List<PartCircle>();
            setCircleOne.Add(new PartCircle(3, "plastic"));
            setCircleOne.Add(new PartCircle(3, "paper"));
            setCircleOne.Add(new PartCircle(4, "glass"));

            setCircle.Add(2, setCircleOne);


            setCircleOne = new List<PartCircle>();
            setCircleOne.Add(new PartCircle(5, "plastic"));
            setCircleOne.Add(new PartCircle(5, "metal"));


            setCircle.Add(3, setCircleOne);

            setCircleOne = new List<PartCircle>();
            setCircleOne.Add(new PartCircle(4, "plastic"));
            setCircleOne.Add(new PartCircle(3, "metal"));
            setCircleOne.Add(new PartCircle(3, "compost"));

            setCircle.Add(4, setCircleOne);


            setCircleOne = new List<PartCircle>();


            setCircle.Add(5, setCircleOne);
            float baseCircle = 0.628f;
            float rotate = 0;
            Vector2 pos = new Vector2(600, 335);
            int t = 1;

            int SET_CIRCLE_NUM = rd.Next(1,setCircle.Count);

            foreach (PartCircle part in setCircle[SET_CIRCLE_NUM])
            {
                for (int i = 0; i < part.amount; i++)
                {
                    binsList.Add(new Bins(game, part.type, pos, rotate));
                    rotate = baseCircle * t++;
                }

            }

            List<Vector2> coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(1250), -rd.Next(200))).ToList();
            List<PartCircle> type_Gb = setCircle[SET_CIRCLE_NUM];

            List<string> typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();

            for (int i = 0; i < rd.Next(5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 8)));
            }
            garbageList.Add(gbList);
            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(0, 200), rd.Next(800))).ToList();

            for (int i = 0; i < rd.Next(0, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5,6)));
            }
            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(200, 1000), -rd.Next(200))).ToList();

            for (int i = 0; i < rd.Next(2, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 9)));
            }
            
            garbageList.Add(gbList);
            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(200,1000), rd.Next(700, 1000))).ToList();

            for (int i = 0; i < rd.Next(0,5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 7)));
            }
            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(200, 1000), -rd.Next(200))).ToList();

            for (int i = 0; i < rd.Next(2, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 9)));
            }
            garbageList.Add(gbList);


            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(1000,1400), rd.Next(1000))).ToList();

            for (int i = 0; i < rd.Next(1, 4); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 8)));
            }

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)].type + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(200, 1000), -rd.Next(200))).ToList();

            for (int i = 0; i < rd.Next(2, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 9)));
            }

            garbageList.Add(gbList);

        }
    }
}
