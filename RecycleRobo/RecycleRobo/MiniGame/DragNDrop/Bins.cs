﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
namespace RecycleRobo.MGame.DrapNDrop
{
    public class Bins
    {
        public int timeAction = 0;
        public string type;
        public Texture2D texture;
        public Texture2D textureUnlocked;
        public Vector2 position;
        public float rotation = 0;
        public float scale = 0.625f;
        int timeMove = 0;
        public bool isWarning = false;
        MouseState currentMouseState, orderMouseState;
        GameManager game;
        public Color[] textureData;
        public Rectangle rect, baseRect;
        public Matrix transform;

        public Vector2 basePosition = new Vector2(0, 0);
        public Bins(GameManager game, string type, Vector2 position, Texture2D texture, Texture2D textureUnlocked)
        {
            this.game = game;
            this.type = type;
            this.texture = texture;
            this.textureUnlocked = textureUnlocked;
            this.position = position;
            textureData = new Color[texture.Width * texture.Height];
            this.texture.GetData(textureData);
            baseRect = new Rectangle(0, 0, texture.Width, texture.Height);
        }
        public void Update(GameTime gametime){
            timeMove += gametime.ElapsedGameTime.Milliseconds;
            currentMouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(currentMouseState).Y);
            if (timeMove < 1000)
            {
                this.position.Y += 3;
            }
            if (checkBoundBin(mousePosition))
            {
                this.scale = 0.75f;
            }
            else
            {
                this.scale = 0.7f;
            }
            orderMouseState = currentMouseState;
        }
        public bool checkBoundBin(Vector2 mousePosition)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);

            Rectangle box = new Rectangle((int)position.X, (int)position.Y, Convert.ToInt32(texture.Width * scale), Convert.ToInt32(texture.Height * scale));
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, null, Color.White * MiniGameScene.opacity, rotation, new Vector2(0.0f, 0.0f), scale, SpriteEffects.None, 0.022f);
        }
    }
}
