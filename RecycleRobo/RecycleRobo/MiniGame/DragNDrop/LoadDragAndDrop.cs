﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData;
using RecycleRobo.Scenes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.MGame.DrapNDrop;
namespace RecycleRobo
{
    public class LoadDragAndDrop
    {
        GameManager game;
        MiniGameData miniGameData;

        public LoadDragAndDrop(GameManager g) { game = g; }
        public void loadData(List<Garbage> gbList, List<Bins> binsList)
        {
            Random rd = new Random();

            miniGameData = game.Content.Load<MiniGameData>("Data/Minigame/Map"+GlobalClass.curMapPlaying
                                                                +"/Level"+GlobalClass.curLevelPlaying);
            int nextPos = 0;
            int totalWidthGb = 0;
            int totalWidthGbRow2 = 0;
            foreach (GarbageData gb in miniGameData.garbageList)
            {
                string lockasset = "MiniGame\\Garbage\\" + gb.type + Convert.ToString(rd.Next(8));

                Texture2D texture = game.Content.Load<Texture2D>(lockasset);
                //Console.WriteLine(miniGameData.garbageList.IndexOf(gb));
                if (miniGameData.garbageList.IndexOf(gb) < 3)
                {
                    totalWidthGb += Convert.ToInt32(texture.Width * 0.8f);
                }
                if (miniGameData.garbageList.IndexOf(gb) >= 3)
                {
                    totalWidthGbRow2 += Convert.ToInt32(texture.Width * 0.8f);

                }

            }
            totalWidthGb += 120 * (miniGameData.garbageList.Count < 3? (miniGameData.garbageList.Count - 1):2);
            totalWidthGbRow2 += 120 * (miniGameData.garbageList.Count - 4);

            int initPos = (int)(GlobalClass.ScreenWidth - totalWidthGb) / 2;
            int i = 0;
            float t = 2.6f;
            foreach (GarbageData gb in miniGameData.garbageList)
            {
                if (miniGameData.garbageList.IndexOf(gb) == 3)
                {
                    nextPos = 0;
                    initPos = (int)(GlobalClass.ScreenWidth - totalWidthGbRow2) / 2;
                      t = 1.5f;
                }
                string index = Convert.ToString(rd.Next(8));
                Texture2D texture = game.Content.Load<Texture2D>("Minigame\\Garbage\\" + gb.type + index);
                Garbage gb1 = new Garbage(game, gb.type + index, new Vector2(initPos + nextPos, GlobalClass.ScreenHeight + 140 * t), texture, rd.Next(6,12));
                gbList.Add(gb1);
                nextPos += Convert.ToInt32(texture.Width * 0.625f) + 120;
            }
            initPos = 0;
            nextPos = 0;
            totalWidthGb = 0;

            Texture2D bin1 = game.Content.Load<Texture2D>("MiniGame\\Bins\\" + miniGameData.binsList[0].type);
            int nBins = miniGameData.binsList.Count;
            int next = (int)(GlobalClass.ScreenWidth - 600) / (nBins-1) - Convert.ToInt16(bin1.Width*0.5f/(nBins-1));
            initPos = 300;

            foreach (BinsData binsItem in miniGameData.binsList)
            {
                string binsAsset = "MiniGame\\Bins\\" + binsItem.type;
                Texture2D texture = game.Content.Load<Texture2D>(binsAsset);
                binsList.Add(new Bins(this.game, binsItem.type, new Vector2(initPos + nextPos, -texture.Height * 0.5f - 50), texture, texture));
                nextPos += next;
            }
        }
    }
}
