using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo
{

    public class LiveTile
    {
        GameManager game;
        Texture2D[] livetile;
        public Vector2 position;
        Texture2D tileShow;
        Random rd = new Random();
        int index = 1;
        int time = 0;
        float opacity = 0f;
        float scale = 0.625f;
        SpriteEffects effect;
        int next = 0;

        public LiveTile(GameManager game, int index)
        {
            this.game = game;
            livetile = new Texture2D[5];
            string[] name = {"Cans", "Compost", "Paper", "Glass", "Plastic"};
            for (int i = 0; i < 5; i++)
            {
                livetile[i] = game.Content.Load<Texture2D>("MiniGame\\Catcher\\bar" + name[i]);
            }
            effect = SpriteEffects.None;
            this.index = index;
            tileShow = livetile[index];

        }
        public void Initialize()
        {

        }

        public void Update(GameTime gameTime)
        {
            time += 1;
            next = rd.Next(250, 480);
            if (time % next == next - 1)
            {
                time = 0;
                opacity = 0f;
                tileShow = livetile[++index%5];
            }
            else
            {
                scale = 0.625f;
                opacity += 0.05f;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tileShow, position + new Vector2(tileShow.Width * 0.625f / 2f, tileShow.Height * 0.625f / 2f), null, Color.White * opacity, 0.0f, new Vector2(tileShow.Width / 2f, tileShow.Height / 2f), scale, effect, 0.0097f);
        }

    }
}
