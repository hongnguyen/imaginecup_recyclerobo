﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using LoadData;

using ProjectMercury;
using ProjectMercury.Emitters;
using ProjectMercury.Modifiers;
using ProjectMercury.Renderers;

using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Reflection;
using RecycleRobo;
using System.Threading;
namespace RecycleRobo.Scenes
{
    enum Scenes
    {
        LoadingScene,
        MenuScene,
        GamePlayScene,
        MapScene,
        MiniGameScene,
        BuildTeamScene,
        RecycleScene,
        VictoryScene,
        GameOverScene,
        MapSelectScene,
        IntroductionScene,
        ChallengeScene,
        WorlMapScene,
        Pause
    }

    public class GameManager : Microsoft.Xna.Framework.Game
    {
        //VirtualScreen virtualScreen;
        private Thread backgroundThread;
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        bool isMusicPlaying;
        private MenuScene menuScene;
        private GamePlayScene gamePlayScene;
        private MapScene mapScene;
        private BuildTeamScene buildTeamScene;
        private RecycleScene recycle;
        private VictoryScene victory;
        private GameOverScene gameOver;
        private MiniGameScene miniGame;
        private IntroductionScene introScene;
        private WorldMapScene worldMap;
        private ChallengeScene challenge;
        private LoadingScene loadingGame;
        public static Matrix transform;
        public static Cursor cursorDefault;
        public static Cursor cursorAttack;
        public static Cursor cursorHealing;
        public static Cursor cursorPointer, cursorMove;
        public static Form winForm;
        private bool isLoading;

        Scenes currentScene;

        public static SpriteBatchRenderer2 particlesRender;
        public static KeyboardDispatcher keyboard_dispatcher;


        public GameManager()
        {

            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
            isMusicPlaying = true;
            isLoading = false;
            // mouse win form
            IsMouseVisible = true;
            cursorDefault = GlobalClass.LoadCustomCursor(@"Content\Cursor\normal.ani");
            cursorAttack = GlobalClass.LoadCustomCursor(@"Content\Cursor\attack.ani");
            cursorHealing = GlobalClass.LoadCustomCursor(@"Content\Cursor\healCursor.ani");
            cursorPointer = GlobalClass.LoadCustomCursor(@"Content\Cursor\point.ani");
            cursorMove = GlobalClass.LoadCustomCursor(@"Content\Cursor\Move.ani");
            winForm = (Form)Form.FromHandle(this.Window.Handle);
            winForm.Cursor = cursorDefault;
            /******************* MUON RESET TAT CA VE DEFAULT THI UNCOMMENT DONG DUOI **********************/
            // ResetToDefault.reset();
            /***********************************************************************************************/
            Data loadData = new Data();
            SettingData loadSetting = loadData.Load<SettingData>("Setting\\Setting.xml");
            if (loadSetting != null)
            {
                GlobalClass.isFullScreen = loadSetting.isFullScreen;
                GlobalClass.volumeMusic = loadSetting.volumeMusic;
                GlobalClass.volumeSoundFx = loadSetting.volumeSoundFx;
            }
            getTheBestResolution();

            Resolution.Init(ref graphics);
            Resolution.SetVirtualResolution(1200, 675);
            //Resolution.SetResolution(800, 450, true);
            //Change resolution in here: 16:9
            if (GlobalClass.isFullScreen)
            {
                Resolution.SetResolution((int)GlobalClass.fullWidthDisplay, (int)GlobalClass.fullHeightDisplay, true);
            }
            else
                Resolution.SetResolution(Convert.ToInt16(GlobalClass.fullWidthDisplay * 0.85f), Convert.ToInt16(GlobalClass.fullHeightDisplay * 0.85f), false);

            transform = Resolution.getTransformationMatrix();

            GlobalClass.ScreenWidth = 1200;
            GlobalClass.ScreenHeight = 675;
            GameManager.transform = Resolution.getTransformationMatrix();

            particlesRender = new SpriteBatchRenderer2
            {
                GraphicsDeviceService = graphics
            };
            keyboard_dispatcher = new KeyboardDispatcher(this.Window);
            Exp.InitExpNumber();
        }
        private void getTheBestResolution()
        {
            GlobalClass.fullWidthDisplay = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            GlobalClass.fullHeightDisplay = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            if (GlobalClass.fullWidthDisplay / GlobalClass.fullHeightDisplay != 16 / 9)
            {
                GlobalClass.fullHeightDisplay = GlobalClass.fullWidthDisplay * 9 / 16;
            }
        }

        protected override void Initialize()
        {

            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Button.Content = Content;

            //Muon reset tat ca ve default thi uncomment dong duoi
            //ResetToDefault.reset();

            StartLoading();

            particlesRender.LoadContent(Content);

            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (Mouse.GetState().LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed)
            {
                winForm.Cursor = cursorPointer;
            }
            else
                winForm.Cursor = cursorDefault;
            if (!IsActive && isMusicPlaying)
            {
                MediaPlayer.Pause();
                isMusicPlaying = false;
                GlobalClass.isPause = true;
                GlobalClass.isGotItFalse = true;
            }
            if (this.IsActive)
            {
                GlobalClass.isPause = false;
                GlobalClass.isGotItFalse = false;

                if (!isMusicPlaying)
                {
                    MediaPlayer.Resume();
                    isMusicPlaying = true;

                }
                switch (currentScene)
                {
                    case Scenes.LoadingScene:
                        if (!isLoading)
                        {

                            backgroundThread = new Thread(LoadGame);
                            isLoading = true;
                            backgroundThread.Start();

                        }
                        else if (loadingGame != null && isLoading)
                        {
                             loadingGame.Update(gameTime);

                        }
                        break;
                    case Scenes.IntroductionScene:
                        if (introScene != null)
                            introScene.Update(gameTime);
                        break;
                    case Scenes.MenuScene:
                        if (menuScene != null)
                        {
                            isLoading = false;
                            menuScene.Update(gameTime);
                        }
                        break;
                    case Scenes.GamePlayScene:
                        if (gamePlayScene != null)
                            gamePlayScene.Update(gameTime);
                        break;
                    case Scenes.MapScene:
                        if (mapScene != null)
                            mapScene.Update(gameTime);
                        break;
                    case Scenes.BuildTeamScene:
                        if (buildTeamScene != null)
                            buildTeamScene.Update(gameTime);
                        break;
                    case Scenes.RecycleScene:
                        if (recycle != null)
                            recycle.Update(gameTime);
                        break;
                    case Scenes.VictoryScene:
                        if (victory != null)
                            victory.Update(gameTime);
                        break;
                    case Scenes.GameOverScene:
                        if (gameOver != null)
                            gameOver.Update(gameTime);
                        break;
                    case Scenes.WorlMapScene:
                        if (worldMap != null)
                        {
                            worldMap.Update(gameTime);
                        }
                        break;
                    case Scenes.ChallengeScene:
                        if (challenge != null)
                        {
                            challenge.Update(gameTime);
                        }
                        break;
                    case Scenes.MiniGameScene:
                        if (miniGame != null)
                        {
                            miniGame.Update(gameTime);
                        }
                        break;
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            Resolution.BeginDraw();
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());
            //virtualScreen.Draw(spriteBatch);

            switch (currentScene)
            {
                case Scenes.LoadingScene:
                    if (loadingGame != null)
                        loadingGame.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.IntroductionScene:
                    if (introScene != null)
                        introScene.Draw(spriteBatch);
                    break;
                case Scenes.MenuScene:
                    if (menuScene != null)
                        menuScene.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.GamePlayScene:
                    if (gamePlayScene != null)
                        gamePlayScene.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.MapScene:
                    if (mapScene != null)
                        mapScene.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.BuildTeamScene:
                    if (buildTeamScene != null)
                        buildTeamScene.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.RecycleScene:
                    if (recycle != null)
                        recycle.Draw(gameTime, spriteBatch);
                    break;
                case Scenes.VictoryScene:
                    if (victory != null)
                    {
                        if (miniGame != null)
                            miniGame.Draw(spriteBatch, gameTime);
                        victory.Draw(gameTime, spriteBatch);
                    }
                    break;
                case Scenes.GameOverScene:
                    if (gameOver != null)
                    {
                        if (gamePlayScene != null)
                            gamePlayScene.Draw(gameTime, spriteBatch);
                        gameOver.Draw(spriteBatch);
                    }
                    break;
                case Scenes.WorlMapScene:
                    if (worldMap != null)
                    {
                        worldMap.Draw(spriteBatch);
                    }
                    break;
                case Scenes.ChallengeScene:
                    if (challenge != null)
                    {
                        challenge.Draw(gameTime, spriteBatch);
                    }
                    break;
                case Scenes.MiniGameScene:
                    if (miniGame != null)
                    {
                        if (gamePlayScene != null)
                            gamePlayScene.Draw(gameTime, spriteBatch);
                        miniGame.Draw(spriteBatch, gameTime);
                    }
                    break;
            }

            spriteBatch.End();

            //spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, null, null, null, null, Resolution.getTransformationMatrix());
            if (currentScene == Scenes.GamePlayScene && !GlobalClass.isSetting)
            {
                gamePlayScene.RenderParticles();
            }
            if (currentScene == Scenes.VictoryScene)
            {
                gamePlayScene.RenderVictoryParticle();
            }
            //spriteBatch.End();
            base.Draw(gameTime);
        }

        public void StartLoading()
        {
            loadingGame = new LoadingScene(Content);
            currentScene = Scenes.LoadingScene;
        }
        public void StartMenuScene()
        {
            menuScene = new MenuScene(this);
            currentScene = Scenes.MenuScene;
        }
        public void StartMiniGame()
        {
            miniGame = new MiniGameScene(this);
            currentScene = Scenes.MiniGameScene;
        }
        public void StartIntroScene()
        {
            introScene = new IntroductionScene(this);
            currentScene = Scenes.IntroductionScene;
        }

        public void PlayGame()
        {
            gamePlayScene = new GamePlayScene(this);
            currentScene = Scenes.GamePlayScene;
            menuScene = null;
        }

        public void StartMap()
        {
            if (GlobalClass.curMapPlaying == 1)
                mapScene = new MapScene(this, 7);
            else
            {
                mapScene = new MapScene(this, 8);
            }
            currentScene = Scenes.MapScene;
            menuScene = null;
            gamePlayScene = null;
            victory = null;
            gameOver = null;
        }

        public void WorldMap()
        {
            worldMap = new WorldMapScene(this);
            currentScene = Scenes.WorlMapScene;
            menuScene = null;
        }

        public void StartBuildTeam()
        {
            buildTeamScene = new BuildTeamScene(this);
            currentScene = Scenes.BuildTeamScene;
        }

        public void RecycleStart()
        {
            recycle = new RecycleScene(this);
            currentScene = Scenes.RecycleScene;
            menuScene = null;
        }

        public void StartVictoryScene()
        {
            victory = new VictoryScene(this);
            currentScene = Scenes.VictoryScene;
        }

        public void StartGameOverScene()
        {
            gameOver = new GameOverScene(this);
            currentScene = Scenes.GameOverScene;
        }

        public void StartChallenge()
        {
            challenge = new ChallengeScene(this);
            currentScene = Scenes.ChallengeScene;
            menuScene = null;
        }
        private void LoadGame()
        {
            LoadingAssets loading = new LoadingAssets(this.Content);
            StartMenuScene();
            //StartVictoryScene(new List<Item>());
            //StartBuildTeam();
            //StartMiniGame();
            //StartMiniGame();
            isLoading = false;
        }
    }
}
