using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    public abstract class Slider
    {
        public Texture2D baseTexture;
        Texture2D coverTexture;
        public Texture2D circleTexture;
        GameManager game;
        public Vector2 basePos;
        public Vector2 circlePos;
        public float scale = 0.625f;
        MouseState curMouse, preMouse;
        bool isDragging = false;
        public int width = 0;
        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };

        public Slider(GameManager game, Vector2 basePos)
        {
            this.basePos = basePos;
            this.game = game;
            Initialize();
        }

        public void Initialize()
        {
            baseTexture = game.Content.Load<Texture2D>("Setting/official/blackBar");
            coverTexture = game.Content.Load<Texture2D>("Setting/official/whiteBar");
            circleTexture = game.Content.Load<Texture2D>("Setting/official/whiteCircle");
            width = Convert.ToInt16((baseTexture.Width * scale) * LoadValue() / 100);
            circlePos = new Vector2(basePos.X + width - circleTexture.Height * scale / 2, basePos.Y - circleTexture.Height * scale / 2 + 0.1f);
        }
        public abstract int LoadValue();
        public abstract void ChangeValue();
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(circleTexture, circlePos, null, Color.White, 0f, Vector2.Zero, this.scale, SpriteEffects.None, 0.03f);
            spriteBatch.Draw(baseTexture, basePos, null, Color.White, 0f, Vector2.Zero, this.scale, SpriteEffects.None, 0.05f);
            spriteBatch.Draw(coverTexture, new Rectangle((int)basePos.X,(int)basePos.Y,width,4), null, Color.White, 0f, Vector2.Zero,SpriteEffects.None, 0.04f);
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            curMouse = Mouse.GetState();
            Vector2 mousePos = new Vector2(MouseHelper.MousePosition(curMouse).X, MouseHelper.MousePosition(curMouse).Y);
            if (curMouse.LeftButton == ButtonState.Released)
            {
                isDragging = false;
            }
            if (curMouse.LeftButton == ButtonState.Pressed && preMouse.LeftButton == ButtonState.Released)
            {
                if (checkBoundSlider(mousePos))
                    isDragging = true;
                else
                    isDragging = false;
            }
            if (isDragging)
            {
                float slidePos = MouseHelper.MousePosition(preMouse).X - MouseHelper.MousePosition(curMouse).X;
                if (circlePos.X > basePos.X - circleTexture.Width * scale / 2 + slidePos - 1&& circlePos.X < basePos.X + baseTexture.Width * scale + circleTexture.Width * scale / 2 + slidePos - 14)
                {
                    circlePos.X -= slidePos;
                }
            }
            ChangeValue();

            preMouse = curMouse;
        }
        private bool checkBoundSlider(Vector2 mousePos)
        {
            Rectangle circleRect = new Rectangle(Convert.ToInt16(circlePos.X), Convert.ToInt16(circlePos.Y),Convert.ToInt16(circleTexture.Width),Convert.ToInt16(circleTexture.Height));
            Point mousePoint = new Point((int)mousePos.X, (int)mousePos.Y);
            if (circleRect.Contains(mousePoint))
            {
                return true;
            }
            return false;

        }
    }
}
