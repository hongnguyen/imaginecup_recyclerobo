using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class FxSlider : Slider
    {
        public FxSlider(GameManager game, Vector2 basePos): base(game, basePos)
        {
            base.Initialize();
        }
        public override int LoadValue()
        {
            return GlobalClass.volumeSoundFx;
        }
        public void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public override void ChangeValue()
        {
            float unit = (float)100 / (base.baseTexture.Width * scale);
            base.width = Convert.ToInt16((base.circlePos.X - base.basePos.X + circleTexture.Width * scale / 2));
            GlobalClass.volumeSoundFx = Convert.ToInt16(base.width * unit) > 100 ? 100 : Convert.ToInt16(base.width * unit);
        }
    }
}
