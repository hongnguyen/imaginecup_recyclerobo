﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class OldSetting
    {
        GameManager game;
        Vector2 position, positionSlider, oriPositionSliderBar, positionSliderBar;
        Vector2 positionSliderBarMusic, positionSliderMusic, oriPositionSliderBarMusic;
        Texture2D settingBox, optionTexture, sliderBar, sliderTexture, full, normal, full_Hover, normal_Hover, aboutTexture;
        public Button optionButton;
        float opacity = 0.5f, opacityAbout = 1f;
        int timeAction = 0, timeShow = 0;
        public bool isClick = false;
        bool isDragging = false, isMusic = false, isSound = false, isAboutClicked = false;
        MouseState currentMouseState, preMouseState;
        SpriteFont font;
        Vector2 pos;
        Button doneButton, fullScreenButton, aboutButton;
        Texture2D aboutNormal;
        Texture2D aboutHover;
        Vector2 aboutPos;
        public OldSetting(GameManager game)
        {
            this.game = game;
            settingBox = game.Content.Load<Texture2D>("Setting\\SETTING");
            this.position = new Vector2(GlobalClass.ScreenWidth / 2 - settingBox.Width / 2, GlobalClass.ScreenHeight / 2 - settingBox.Height / 2 - 25);
            optionTexture = game.Content.Load<Texture2D>("Setting\\optionButton");
            sliderBar = game.Content.Load<Texture2D>("Setting\\slider");
            sliderTexture = game.Content.Load<Texture2D>("Setting\\button");
            font = game.Content.Load<SpriteFont>("materials");
            optionButton = new Button(optionTexture, game.Content.Load<Texture2D>("Setting\\optionButtonHover"), optionTexture, 0.7f, 0f, new Vector2(GlobalClass.ScreenWidth - optionTexture.Width, 40));
            optionButton.Clicked += new EventHandler(showPopup);

            positionSlider = new Vector2(525 + GlobalClass.volumeSoundFx * 2 - sliderTexture.Width / 2, 368 - sliderTexture.Height / 2 - 50);
            positionSliderBar = new Vector2(527, 364 - 50);
            oriPositionSliderBar = positionSliderBar;

            positionSliderMusic = new Vector2(525 + GlobalClass.volumeMusic * 2 - sliderTexture.Width / 2, 332 - sliderTexture.Height / 2 - 50);
            positionSliderBarMusic = new Vector2(527, 327 - 50);
            oriPositionSliderBarMusic = positionSliderBarMusic;

            full = game.Content.Load<Texture2D>("Setting//full");
            full_Hover = game.Content.Load<Texture2D>("Setting//full_Hover");

            normal = game.Content.Load<Texture2D>("Setting//normal");
            normal_Hover = game.Content.Load<Texture2D>("Setting//normal_Hover");

            pos = new Vector2(532, 357);
            
            if (GlobalClass.isFullScreen)
            {
                fullScreenButton = new Button(full, normal_Hover, full, 1f, 0f, pos);
            }
            else
            {
                fullScreenButton = new Button(normal, full_Hover, normal, 1f, 0f, pos);
            }
            fullScreenButton.Clicked += new EventHandler(changeResolution);
            fullScreenButton.layer = 0.00001f;

            Texture2D buttonNormal = game.Content.Load<Texture2D>("Setting//doneButton");
            Texture2D buttonhover = game.Content.Load<Texture2D>("Setting//doneButtonHover");
            Vector2 buttonPos = new Vector2(550 + buttonNormal.Width * 0.8f / 2, 365 + buttonNormal.Height * 0.8f / 2);
            doneButton = new Button(buttonNormal, buttonhover, buttonNormal, 0.8f, 0f, buttonPos);
            doneButton.Clicked += new EventHandler(saveSetting);
            doneButton.layer = 0.00001f;

            aboutNormal = game.Content.Load<Texture2D>("Setting//aboutButton");
            aboutHover = game.Content.Load<Texture2D>("Setting//aboutButton_Hover");
            aboutPos = new Vector2(640 + aboutNormal.Width * 0.8f / 2, 365 + aboutNormal.Height * 0.8f / 2);
            aboutButton = new Button(aboutNormal, aboutHover, aboutNormal, 0.8f, 0f, aboutPos);
            aboutButton.Clicked += new EventHandler(aboutButton_Clicked);
            aboutButton.layer = 0.00001f;
            aboutTexture = game.Content.Load<Texture2D>("Setting\\about");
        }
        private void aboutButton_Clicked(object o, EventArgs e)
        {
            isAboutClicked = true;

        }
        private void changeResolution(object o, EventArgs e)
        {
            GlobalClass.isFullScreen = !GlobalClass.isFullScreen;
            if (GlobalClass.isFullScreen)
            {
                setFullScreen();
                fullScreenButton = new Button(full, normal_Hover, full, 1f, 0f, pos);
            }
            else
            {
                setNormalScreen();
                fullScreenButton = new Button(normal, full_Hover, normal, 1f, 0f, pos);
            }
            fullScreenButton.Clicked += new EventHandler(changeResolution);

        }
        public static void setFullScreen()
        {
            Resolution.SetResolution((int)GlobalClass.fullWidthDisplay, (int)GlobalClass.fullHeightDisplay, true);
            GameManager.transform = Resolution.getTransformationMatrix();
        }
        public static void setNormalScreen()
        {
            Resolution.SetResolution(Convert.ToInt16(GlobalClass.fullWidthDisplay * 0.85f), Convert.ToInt16(GlobalClass.fullHeightDisplay * 0.85f), false);
            GameManager.transform = Resolution.getTransformationMatrix();

        }
        public void saveSetting(object o, EventArgs e)
        {
            SettingData save = new SettingData(GlobalClass.volumeMusic, GlobalClass.volumeSoundFx, GlobalClass.isFullScreen);
            Data saveData = new Data();
            saveData.Save(save, "Setting\\Setting.xml");
            this.isClick = !this.isClick;
        }
        public void showPopup(object o, EventArgs e)
        {
            this.isClick = !this.isClick;
        }
        public void Update(GameTime gameTime)
        {


            if (isClick)
            {
                if (isAboutClicked && opacityAbout < 0.95)
                {
                    opacityAbout += 0.01f;

                }
                aboutButton.Update(gameTime);
                fullScreenButton.Update(gameTime);
            }
            if (isClick && !isAboutClicked)
            {
                doneButton.Update(gameTime);

            }
            optionButton.Update(gameTime);
            if (isClick)
            {
                timeAction += gameTime.ElapsedGameTime.Milliseconds / 16;
                if (timeAction < 50)
                {
                    opacity += 0.03f;
                }
            }
            else
            {
                isAboutClicked = false;
                opacityAbout = 0f;
                opacity = 0.5f;
            }
            currentMouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(currentMouseState).Y);
            if (currentMouseState.LeftButton == ButtonState.Released)
            {
                isDragging = false;
            }
            if (currentMouseState.LeftButton == ButtonState.Pressed && preMouseState.LeftButton == ButtonState.Released)
            {
                if (isAboutClicked && checkBoundAbout(mousePosition))
                {
                    isAboutClicked = false;
                }
                if (checkBoundSlider(mousePosition))
                {
                    isDragging = true;
                }
                else
                {
                    isDragging = false;
                }
            }
            if (isDragging)
            {
                if (isSound)
                {
                    if (positionSlider.X > 513 + MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X && positionSlider.X < 725 + MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X - 10)
                        positionSlider.X -= (MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X);
                    GlobalClass.volumeSoundFx = (int)(positionSlider.X - 514) / 2 > 100 ? 100 : (int)(positionSlider.X - 514) / 2;
                }
                else if (isMusic)
                {
                    if (positionSliderMusic.X > 513 + MouseHelper.MousePosition(preMouseState).X -MouseHelper.MousePosition(currentMouseState).X && positionSliderMusic.X < 725 + MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X - 10)
                        positionSliderMusic.X -= (MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X);
                    GlobalClass.volumeMusic = (int)(positionSliderMusic.X - 514) / 2 > 100 ? 100 : (int)(positionSliderMusic.X - 514) / 2;
                }

            }

            preMouseState = currentMouseState;
            timeAction = 0;
        }
        private bool checkBoundAbout(Vector2 mousePos)
        {
            Rectangle aboutRect = new Rectangle((int)aboutPos.X - Convert.ToInt32(aboutTexture.Width * 0.625f / 2), (int)aboutPos.Y - Convert.ToInt32(aboutTexture.Height * 0.625f / 2),
                                        Convert.ToInt32(aboutTexture.Width * 0.625f), Convert.ToInt32(aboutTexture.Height * 0.625f));
            if (aboutRect.Contains(new Point((int)mousePos.X, (int)mousePos.Y)))
            {
                return true;
            }
            return false;

        }
        public bool checkBoundSlider(Vector2 mousePos)
        {
            Rectangle sliderButtonRectMusic = new Rectangle((int)positionSliderMusic.X - 20, (int)positionSliderMusic.Y, sliderTexture.Width + 40, sliderTexture.Height);

            Rectangle sliderButtonRect = new Rectangle((int)positionSlider.X - 20, (int)positionSlider.Y, sliderTexture.Width + 40, sliderTexture.Height);
            if (sliderButtonRect.Contains(new Point((int)mousePos.X, (int)mousePos.Y)))
            {
                isSound = true;
                isMusic = false;
                return true;
            }
            if (sliderButtonRectMusic.Contains(new Point((int)mousePos.X, (int)mousePos.Y)))
            {
                isMusic = true;
                isSound = false;
                return true;
            }

            return false;
        }
        public void Draw(SpriteBatch spritebatch)
        {

            optionButton.Draw(spritebatch);
            //spritebatch.DrawString(font, Convert.ToString("X= " + PointX + ":" + "Y= " + PointY), new Vector2(0, 0), Color.White);

            if (isClick)
            {
                aboutButton.Draw(spritebatch);
                fullScreenButton.Draw(spritebatch);
                doneButton.Draw(spritebatch);
                spritebatch.Draw(sliderTexture, positionSlider, null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.00002f);
                spritebatch.Draw(sliderTexture, positionSliderMusic, null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.00002f);
                spritebatch.Draw(settingBox, position, null, Color.White * opacity, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.065f);
                spritebatch.DrawString(font, Convert.ToString(GlobalClass.volumeSoundFx), new Vector2(735, 357 - 50), Color.White);
                spritebatch.DrawString(font, Convert.ToString(GlobalClass.volumeMusic), new Vector2(735, 322 - 50), Color.White);


                spritebatch.Draw(sliderBar, new Rectangle((int)oriPositionSliderBarMusic.X, (int)oriPositionSliderBarMusic.Y, (int)positionSliderMusic.X - 527, sliderBar.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.0001f);

                spritebatch.Draw(sliderBar, new Rectangle((int)oriPositionSliderBar.X, (int)oriPositionSliderBar.Y, (int)positionSlider.X - 527, sliderBar.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.0001f);
            }
            if (isAboutClicked)
            {
                spritebatch.Draw(aboutTexture, new Vector2(GlobalClass.ScreenWidth / 2, GlobalClass.ScreenHeight / 2), null, Color.White * opacityAbout, 0f, new Vector2(aboutTexture.Width / 2, aboutTexture.Height / 2), 0.625f, SpriteEffects.None, 0.0000001f);

            }
        }
    }
}
