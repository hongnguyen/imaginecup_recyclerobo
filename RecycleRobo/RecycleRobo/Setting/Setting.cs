using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using RecycleRobo.Scenes;


namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Setting
    {
        GameManager game;
        Button resoButton, exitButton, resumeButton, volumeButton, aboutButton, retreatButton;
        Button checkButton, crossButton;
        Vector2 basePos;
        float buttonScale = 0.625f;
        float margin = 9f;
        Texture2D resoTextureFull, resoTextureNormal, exitTexture, resumeTexture, volumeTexture, aboutTexture;
        Vector2 resoPos, exitPos, resumePos, volumePos, aboutPos, volume2Pos;
        //Slider volume, volume2;
        FxSlider volume;
        SoudSlider volume2;
        public bool isPlaySene = false;
        public bool isResume = true;
        public bool isRetreat = false;
        HowToPlay howToPlay;
        public Setting(GameManager game, Vector2 basePos)
        {
            this.game = game;
            this.basePos = basePos;
            howToPlay = new HowToPlay(game);
            Initialize();
        }

        public void Initialize()
        {
            //resoTextureFull = game.Content.Load<Texture2D>("Setting/official/fullOnTile");
            //resoTextureNormal = game.Content.Load<Texture2D>("Setting/official/fullOffTile");

            //exitTexture = game.Content.Load<Texture2D>("Setting/official/exitTile");
            //Texture2D exitTextureHover = game.Content.Load<Texture2D>("Setting/official/exitHover");
            //resumeTexture = game.Content.Load<Texture2D>("Setting/official/resumeTile");
            //Texture2D resumeTextureHover = game.Content.Load<Texture2D>("Setting/official/resumeHover");

            //Texture2D retreatTexture = game.Content.Load<Texture2D>("Setting/official/retreatTile");
            //Texture2D retreatTextureHover = game.Content.Load<Texture2D>("Setting/official/retreatHover");

            //volumeTexture = game.Content.Load<Texture2D>("Setting/official/volumeTile");
            //aboutTexture = game.Content.Load<Texture2D>("Setting/official/aboutTile");

            resoTextureFull = Asset.resoTextureFull;
            resoTextureNormal = Asset.resoTextureNormal;

            exitTexture = Asset.exitTexture;
            Texture2D exitTextureHover = Asset.exitTextureHover;
            resumeTexture = Asset.resumeTexture;
            Texture2D resumeTextureHover = Asset.resumeTextureHover;

            Texture2D retreatTexture = Asset.retreatTexture;
            Texture2D retreatTextureHover = Asset.retreatTextureHover;

            volumeTexture = Asset.volumeTexture;
            aboutTexture = Asset.aboutTexture;


            volumePos = new Vector2(0, 0) + basePos;
            volume = new FxSlider(game, volumePos + new Vector2(-33, 33));
            volume2 = new SoudSlider(game, volumePos + new Vector2(-33, -3));
            resoPos = new Vector2(0, resoTextureFull.Height * buttonScale + margin + 2) + basePos;
            resumePos = new Vector2(resoTextureFull.Width * buttonScale + margin + 2, 0) + basePos;
            exitPos = new Vector2(resoTextureFull.Width * buttonScale + margin + 2, resoTextureFull.Height * buttonScale + margin) + basePos;
            aboutPos = new Vector2(aboutTexture.Width*buttonScale/2 - resoTextureFull.Width * buttonScale/2, 2 * (resoTextureFull.Height * buttonScale + margin)) + basePos;

            if (GlobalClass.isFullScreen)
            {
                resoButton = new Button(resoTextureFull, resoTextureNormal, resoTextureFull, buttonScale, 0f, resoPos);
            }
            else
            {
                resoButton = new Button(resoTextureNormal, resoTextureFull, resoTextureNormal, buttonScale, 0f, resoPos);
            }
            retreatButton = new Button(retreatTexture, retreatTextureHover, retreatTexture, buttonScale, 0f, exitPos);


            resumeButton = new Button(resumeTexture, resumeTextureHover, resumeTexture, buttonScale, 0f, resumePos);
            exitButton = new Button(exitTexture, exitTextureHover, exitTexture, buttonScale, 0f, exitPos);
            volumeButton = new Button(volumeTexture, volumeTexture, volumeTexture, buttonScale, 0f, volumePos);
            aboutButton = new Button(aboutTexture, aboutTexture, aboutTexture, buttonScale, 0f, aboutPos);
            resoButton.Clicked += new EventHandler(changeResolution);
            resumeButton.Clicked += new EventHandler(resumeButton_Clicked);
            exitButton.Clicked += new EventHandler(exitButton_Clicked);
            retreatButton.Clicked += new EventHandler(retreatButton_Clicked);
            
            resoButton.layer = 0.06f;
            exitButton.layer = 0.06f;
            retreatButton.layer = 0.06f;
            resumeButton.layer = 0.06f;
            aboutButton.layer = 0.06f;
            volumeButton.layer = 0.06f;


            InitConfirmButton();
        }
        public void retreatButton_Clicked(object o, EventArgs e)
        {
            isRetreat = true;
            isResume = true;
            //GlobalClass.isPause = true;
        }
        public void exitButton_Clicked(object o, EventArgs e)
        {
            game.Exit();
        }
        public void resumeButton_Clicked(object o, EventArgs e)
        {
            isResume = !isResume;
            GlobalClass.isSetting = false;
        }
        public void InitConfirmButton()
        {
            Texture2D checkTexture = Asset.checkTexture;
            Texture2D crossTexture = Asset.crossTexture;
            Texture2D checkTextureHover = Asset.checkTextureHover;
            Texture2D crossTextureHover = Asset.crossTextureHover;

            checkButton = new Button(checkTexture, checkTextureHover, checkTexture, 0.625f, 0, new Vector2(GlobalClass.ScreenWidth / 2 - 120, GlobalClass.ScreenHeight / 2));
            checkButton.Clicked += new EventHandler(checkButton_Clicked);
            checkButton.layer = 0.05f;

            crossButton = new Button(crossTexture, crossTextureHover, crossTexture,
                                        0.625f, 0f, new Vector2(GlobalClass.ScreenWidth / 2 + 160, GlobalClass.ScreenHeight / 2));
            crossButton.layer = 0.05f;

            crossButton.Clicked += new EventHandler(crossButton_Clicked);
        }
        public void checkButton_Clicked(object o, EventArgs e)
        {
            MediaPlayer.Stop();
            MediaPlayer.Play(game.Content.Load<Song>("sounds\\main_sound"));
            MediaPlayer.IsRepeating = true;
            this.game.StartMap();
            GlobalClass.isSetting = false;
        }
        public void crossButton_Clicked(object o, EventArgs e)
        {
            isRetreat = false;
            GlobalClass.isSetting = false;
        }
        public void SaveData()
        {
            SettingData save = new SettingData(GlobalClass.volumeMusic, GlobalClass.volumeSoundFx, GlobalClass.isFullScreen);
            Data saveData = new Data();
            saveData.Save(save, "Setting\\Setting.xml");
        }
        private void changeResolution(object o, EventArgs e)
        {
            GlobalClass.isFullScreen = !GlobalClass.isFullScreen;
            if (GlobalClass.isFullScreen)
            {
                setFullScreen();
                Matrix transfrom = RotatedRectangle.GetMatrixTransfrom(new Vector2(howToPlay.clipRect.X, howToPlay.clipRect.Y), Vector2.Zero, 0f, 1f);
                howToPlay.clipRect = RotatedRectangle.CalculateBoundingRectangle(howToPlay.clipRect, GameManager.transform);

                resoButton = new Button(resoTextureFull, resoTextureNormal, resoTextureFull, buttonScale, 0f, resoPos);
                resoButton.layer = 0.06f;
            }
            else
            {
                setNormalScreen();
                //howToPlay.clipRect = RotatedRectangle.CalculateBoundingRectangle(howToPlay.clipRect, GameManager.transform);
                howToPlay.clipRect = howToPlay.originRect;
                resoButton = new Button(resoTextureNormal, resoTextureFull, resoTextureNormal, buttonScale, 0f, resoPos);
                resoButton.layer = 0.06f;

            }
            resoButton.Clicked += new EventHandler(changeResolution);
        }
        public static void setFullScreen()
        {
            Resolution.SetResolution((int)GlobalClass.fullWidthDisplay, (int)GlobalClass.fullHeightDisplay, true);
            GameManager.transform = Resolution.getTransformationMatrix();
        }
        public static void setNormalScreen()
        {
            Resolution.SetResolution(Convert.ToInt16(GlobalClass.fullWidthDisplay * 0.85f), Convert.ToInt16(GlobalClass.fullHeightDisplay * 0.85f), false);
            //HowToPlay.clipRect = RotatedRectangle.CalculateBoundingRectangle(HowToPlay.clipRect, GameManager.transform);
            GameManager.transform = Resolution.getTransformationMatrix();
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!isResume)
            {
                if (isPlaySene){
                    retreatButton.Draw(spriteBatch);
                }
                else
                    exitButton.Draw(spriteBatch);
                resumeButton.Draw(spriteBatch);
                resoButton.Draw(spriteBatch);
                volumeButton.Draw(spriteBatch);
                aboutButton.Draw(spriteBatch);
                volume.Draw(spriteBatch);
                volume2.Draw(spriteBatch);
                howToPlay.Draw(spriteBatch);
            }
            if (isRetreat)
            {
                spriteBatch.DrawString(Asset.mediumFont, "Are you sure? You want to retreat!", new Vector2(GlobalClass.ScreenWidth / 2 - 160, GlobalClass.ScreenHeight / 2 - 100), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.01f);
                checkButton.Draw(spriteBatch);
                crossButton.Draw(spriteBatch);
            }


        }
        public void Update(GameTime gameTime)
        {
            if (!isResume)
            {

                volume.Update(gameTime);
                volume2.Update(gameTime);
                if (isPlaySene){
                    retreatButton.Update(gameTime);
                }
                else
                    exitButton.Update(gameTime);
                resumeButton.Update(gameTime);
                resoButton.Update(gameTime);
                howToPlay.Update(gameTime);
                SaveData();
            }
            if (isRetreat)
            {
                checkButton.Update(gameTime);
                crossButton.Update(gameTime);
            }
        }
    }
}
