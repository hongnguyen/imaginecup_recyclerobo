using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AssNormal
    {
        public Vector2 curPosition;
        public GameManager game;
        public float scale = 0.35f;
        public bool isDown = true;
        public float speed = 1f; //Need if we want to upgrade;
        public bool isDone = false;
        public Color[] textureData;
        public Rectangle baseRect, rect;
        public Matrix transformMatrix;
        public Texture2D texture;
        public float layer = 0.5f;
        public AssNormal()
        {
            curPosition = new Vector2(100, -60);
            texture = Asset.Ass_normal;
            textureData = new Color[texture.Width * texture.Height];
            texture.GetData(textureData);
            baseRect = new Rectangle(0, 0, Asset.Ass_normal.Width, Asset.Ass_normal.Height);


        }
        public void Reset()
        {
            if (isDone)
            {
                isDown = true;
                curPosition.Y = -30;
                isDone = false;
            }
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>

        public void Update(GameTime gameTime, Item sewage)
        {

            this.curPosition.X = sewage.originPosition.X + 30;
            transformMatrix = RotatedRectangle.GetMatrixTransfrom(curPosition, Vector2.Zero, 0f, scale);
            rect = RotatedRectangle.CalculateBoundingRectangle(baseRect, transformMatrix);
            if (this.rect.Intersects(sewage.rect) && isDown)
            {
                if (!RotatedRectangle.IntersectPixels(sewage.transformMatrix, sewage.Image.Width,
                sewage.Image.Height, sewage.textureData,
                this.transformMatrix, this.texture.Width,
                this.texture.Height, this.textureData))
                {
                    curPosition.Y += 4;

                } else
                    isDown = false;
            }
            else if (isDown && !this.rect.Intersects(sewage.rect))
            {
                curPosition.Y += 4;    
            }
            else if (!isDone)
            {
                isDown = false;
                curPosition.Y -= 4;
                sewage.position.Y -= 4;
            }
            if (sewage.position.Y < - 120)
            {
                isDone = true;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Asset.Ass_normal, curPosition, null, Color.White, 0.0f, Vector2.Zero, scale, SpriteEffects.None, layer);
        }
    }
}
