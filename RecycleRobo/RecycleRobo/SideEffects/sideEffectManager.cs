﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class sideEffectManager
    {
        AnimationPlayer animation;
        public List<ParticleEffect> effects;
        private static String[] sideEffects = { "point", "PhoenixFire", "StramoniumThorn" ,"OceanPrison", "VolcanoFire", "leafcut"
                                              ,"iceFang", "healing", "blueWave", "hotWater"};
        private static String[] sideEffectanimation = {"OceanPrison" };
        public sideEffectManager(ContentManager content) 
        {
            effects = new List<ParticleEffect>();
            effects.Add(content.Load<ParticleEffect>("Effect/points"));
            effects.Add(content.Load<ParticleEffect>("Effect/Campfire"));
            effects.Add(content.Load<ParticleEffect>("Effect/stramorniumthorn"));
            effects.Add(content.Load<ParticleEffect>("Effect/water"));
            effects.Add(content.Load<ParticleEffect>("Effect/volcanoFlame"));
            effects.Add(content.Load<ParticleEffect>("Effect/leaves"));
            effects.Add(content.Load<ParticleEffect>("Effect/ice"));
            effects.Add(content.Load<ParticleEffect>("Effect/healingEffect"));
            effects.Add(content.Load<ParticleEffect>("Effect/waterwall"));
            effects.Add(content.Load<ParticleEffect>("Effect/hotWater"));
            foreach(ParticleEffect effect in effects)
            {
                effect.LoadContent(content);
                effect.Initialise();
            }
            animation.Scale = 0.65f;
            animation.OrderLayer = 0.001f;
            animation.color = Color.White;
        }
        public void Update(GameTime gameTime, float severity)
        {
            for (int i = 0 ; i < effects.Count ; i++)
            {
                effects[i].Update(severity);
            }
        }
        public void Draw() 
        {
        }
        public void sideEffectTrigger(Vector2 pos, String type) 
        {
            for (int i = 0; i < sideEffects.Length;i++ )
                if (sideEffects[i].Contains(type))
                {
                    effects[i].Trigger(pos);
                    return;
                }
        }
    }
}
