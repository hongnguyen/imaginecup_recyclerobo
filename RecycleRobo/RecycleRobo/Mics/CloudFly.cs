using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CloudFly
    {
        bool isFlip = false;
        Texture2D texture;
        Vector2 position;
        Vector2 originalPos;
        float scale;
        float rotate = -0.05f;
        public CloudFly(Texture2D tex, Vector2 pos, float s)
        {
            texture = tex;
            position = pos;
            originalPos = pos;
            scale = s;
        }
        public void Update(GameTime gameTime)
        {
            rotate += 0.006f;
            if (rotate > 1.5f)
            {
                rotate = -1.5f;
            }
            //if (position.X > 2500)
            //{
            //    position = originalPos;
            //    rotate = 0.0f;
            //}

            //if (position.X >= GlobalClass.ScreenWidth / 2)
            //{
            //    position.Y += 1f;
            //    rotate += 0.0005f;
            //}
            //if (position.X > 1500)
            //{
            //    rotate += 0.001f;
            //    position.Y += 2;
            //}
            //if (position.X > 1800)
            //{
            //    rotate += 0.002f;
            //    position.Y += 4;
            //}
            //position.X += 2;
            //Console.WriteLine(rotate);



        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, null, Color.White, rotate, new Vector2(GlobalClass.ScreenWidth/2 + 140,GlobalClass.ScreenHeight + 380), scale, SpriteEffects.None, 0.1f);

        }
    }
}
