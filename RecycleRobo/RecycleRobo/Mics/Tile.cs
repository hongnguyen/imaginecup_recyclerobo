﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Windows.Forms;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    public class IconTile
    {
        public enum TileState
        {
            NORMAL,
            NAME,
            DETAIL
        }

        public TileState tileState;

        public Texture2D texture;
        public Vector2 position;
        public Rectangle[] frames;
        public float height, width;
        public const int numberOfFrame = 40;
        public float range;
        private float delayNextFrame = 0;
        private int iterator = 0;
        private const float scale = 0.625f;
        private float timeDelayDetail, timeDelayName, timetoNextFrame = 20, timeDelayCountDown = 0;
        public int timeEnableBack;
        private Random rd;
        public bool isActive = true;
        public bool isEnable = true;
        public Color color = Color.White;

        public IconTile(Texture2D texture, Vector2 position, int seed)
        {
            tileState = TileState.NAME;
            this.texture = texture;
            this.position = position;
            height = texture.Height / 3;
            width = texture.Width;
            range = (2*texture.Height/3) / (numberOfFrame - 1);
            frames = new Rectangle[numberOfFrame];
            for (int i = 0; i < numberOfFrame; i++)
            {
                frames[i] = new Rectangle(0, (int)(2 * texture.Height / 3 - i * range), (int)width, (int)height);
            }
            rd = new Random(seed);
        }

        public void Update(GameTime gameTime)
        {
            if (!isActive) return;

            if (tileState == TileState.NORMAL)
            {
                delayNextFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (delayNextFrame > timetoNextFrame)
                {
                    delayNextFrame = 0;
                    iterator+=2;

                    if (iterator >= numberOfFrame / 2 - 1 && iterator < numberOfFrame / 2)
                    {
                        timetoNextFrame = 50;
                    }
                    else if (iterator >= numberOfFrame - 1 && iterator < numberOfFrame)
                    {
                        timetoNextFrame = 50;
                    }
                    else
                    {
                        timetoNextFrame = 20;
                    }

                    if (iterator == numberOfFrame)
                    {
                        iterator = 0;
                        timeDelayName = rd.Next(2000, 3500);
                        tileState = TileState.NAME;
                    }
                    else if (iterator == numberOfFrame / 2)
                    {
                        tileState = TileState.DETAIL;
                        timeDelayDetail = rd.Next(3000, 6000);
                    }
                    
                }
            }

            if (tileState == TileState.DETAIL)
            {
                delayNextFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (delayNextFrame > timeDelayDetail)
                {
                    delayNextFrame = 0;
                    tileState = TileState.NORMAL;
                }
            }

            if (tileState == TileState.NAME)
            {
                delayNextFrame += gameTime.ElapsedGameTime.Milliseconds;
                if (delayNextFrame > timeDelayName)
                {
                    delayNextFrame = 0;
                    tileState = TileState.NORMAL;
                }
            }

            if (!isEnable)
            {
                timeDelayCountDown += gameTime.ElapsedGameTime.Milliseconds;
                if (timeDelayCountDown >= 1000)
                {
                    timeDelayCountDown = 0;
                    timeEnableBack--;
                    if (timeEnableBack % 3 == 0) color = Color.White;
                    else if (timeEnableBack % 3 == 1) color = Color.Orange;
                    else color = Color.Red;
                    
                    if (timeEnableBack <= 0)
                    {
                        isEnable = true;
                        isActive = false;
                        iterator = 0;
                        tileState = TileState.NAME;
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, frames[iterator], Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.13f);
            
            if (!isEnable && tileState==TileState.DETAIL)
            {
                Vector2 delta = Vector2.Zero;
                if (timeEnableBack >= 100)
                    delta = new Vector2(40, 40);
                else if (timeEnableBack >=10)
                    delta = new Vector2(50, 40);
                else
                    delta = new Vector2(60, 40);
                spriteBatch.DrawString(Asset.fontMinigame, timeEnableBack.ToString(), position + delta, color, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.122f);
            }
        }

        public bool isHover(Point mousePoint)
        {
            if (!isEnable) return false;
            return getRectangle().Contains(mousePoint);
        }

        public Rectangle getRectangle()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)(width * scale), (int)(height * scale));
        }
    }
}
