using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ProjectMercury;
using LoadData;


namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Asset
    {
        //Global
        public static SpriteFont fontVictory;
        public static SpriteFont font;
        public static SpriteFont fontMinigame;
        public static SpriteFont smallFont;
        public static SpriteFont largeFont;
        public static SpriteFont mediumFont;
        // sound effect
        public static SoundEffect correctSound;
        public static SoundEffect wrongSound;
        public static SoundEffect soundButton;

        // song
        public static Song bgSound1;
        public static Song bgSound2;
        public static Song bgSound3;
        public static Texture2D guidePlayScene;

        // button map:
        public static Texture2D backButton;
        public static Texture2D achieveButton;
        public static Texture2D introButton1;
        public static Texture2D introButton2;
        public static Texture2D shopMapButton;
        public static Texture2D skipButton;

        public static Texture2D introOne;
        public static Texture2D introTwo;


        //Vic tory;
        public static Texture2D victoryBg, loseBg, bgMiniGame, bgMiniGameForce, bgMiniGameForce3;
        public static Texture2D  barDemo_1, barDemo_2, barDemo_3;
        public static Texture2D barRay_1, barRay_2, barRay_3;
        public static Texture2D barSoul_1, barSoul_2, barSoul_3;
        public static Texture2D barCrusher_1, barCrusher_2, barCrusher_3;
        public static Texture2D barGeorge_1, barGeorge_2, barGeorge_3;

        //Menu
        public static Texture2D howToButton;
        public static Texture2D howToButtonHover;
        public static Texture2D cancelButton;
        public static Texture2D cancelButtonHover;

        public static Texture2D startButton, startText;
        public static Texture2D shopButton, shopText;
        public static Texture2D achievementButton, achievementText;
        public static Texture2D optionButtonMenu, optionButtonHoverMenu;

        public static Texture2D menuBackground, blackMist, menuCircle, titleGame;
        public static Texture2D demo, ray, crusher, soul, george;
        public static Texture2D fade;

        //Setting
        public static Texture2D resoTextureFull;
        public static Texture2D resoTextureNormal;
        public static Texture2D exitTexture;
        public static Texture2D exitTextureHover;
        public static Texture2D resumeTexture;
        public static Texture2D resumeTextureHover;
        public static Texture2D retreatTexture;
        public static Texture2D retreatTextureHover;
        public static Texture2D volumeTexture;
        public static Texture2D aboutTexture;

        public static Texture2D checkTexture;
        public static Texture2D checkTextureHover;
        public static Texture2D crossTexture;
        public static Texture2D crossTextureHover;

        //Recycle
        public static Texture2D defentantBought;
        public static Texture2D defentantBuy;
        public static Texture2D defentantLock;
        public static Texture2D megadroidBought;
        public static Texture2D megadroidBuy;
        public static Texture2D megadroidLock;
        public static Texture2D gigadroidBought;
        public static Texture2D gigadroidBuy;
        public static Texture2D gigadroidLock;

        public static Texture2D knightBought;
        public static Texture2D knightBuy;
        public static Texture2D knightLock;

        public static Texture2D thunderBought;
        public static Texture2D thunderBuy;
        public static Texture2D thunderLock;

        public static Texture2D thunderbolt2Bought;
        public static Texture2D thunderbolt2Buy;
        public static Texture2D thunderbolt2Lock;

        public static Texture2D engine;
        public static Texture2D healer1;
        public static Texture2D healer2;
        
        public static Texture2D mini;
        public static Texture2D resourceBar;

        public static Texture2D taskBarAssistant;
        public static Texture2D taskBarCyclebot;

        public static Texture2D levelDisplay;
        public static Texture2D level1_1;
        public static Texture2D level2_1;
        public static Texture2D level3_1;
        public static Texture2D level4_1;
        public static Texture2D pointer;

        public static Texture2D selected_1;
        public static Texture2D selected_2;
        public static Texture2D selected_3;
        public static Texture2D selected_4;
        public static Texture2D selected_5;
        public static Texture2D cyclebotList1;
        public static Texture2D cyclebotList2;
        public static Texture2D cyclebotList3;
        public static Texture2D cyclebotList4;
        public static Texture2D cyclebotList5;
        public static Texture2D unknown;
        public static Texture2D num1_lv1;
        public static Texture2D num1_lv2;
        public static Texture2D num1_lv3;
        public static Texture2D num2_lv1;
        public static Texture2D num2_lv2;
        public static Texture2D num2_lv3;
        public static Texture2D num3_lv1;
        public static Texture2D num3_lv2;
        public static Texture2D num3_lv3;
        public static Texture2D num4_lv1;
        public static Texture2D num4_lv2;
        public static Texture2D num4_lv3;
        public static Texture2D num5_lv1;
        public static Texture2D num5_lv2;
        public static Texture2D num5_lv3;
        public static Texture2D descriptionBox;

        public static Texture2D desc1_1;
        public static Texture2D desc1_2;
        public static Texture2D desc1_3;
        public static Texture2D desc1_4;

        public static Texture2D desc2_1;
        public static Texture2D desc2_2;
        public static Texture2D desc2_3;
        public static Texture2D desc2_4;

        public static Texture2D desc3_1;
        public static Texture2D desc3_2;
        public static Texture2D desc3_3;
        public static Texture2D desc3_4;

        public static Texture2D desc4_1;
        public static Texture2D desc4_2;
        public static Texture2D desc4_3;
        public static Texture2D desc4_4;

        public static Texture2D desc5_1;
        public static Texture2D desc5_2;
        public static Texture2D desc5_3;
        public static Texture2D desc5_4;
        public static Texture2D desc5_5;

        public static Texture2D selected1;

        public static Texture2D energyBall;

        //MapSelect
        public static Texture2D map1;
        public static Texture2D land1;
        public static Texture2D land2;

        public static Texture2D mapLock;
        public static Texture2D ocean;
        public static Texture2D cloudTexture;
        
        //Challenge
        public static Texture2D challengeBackground;
        public static Texture2D challengeTaskBar;

        public static Texture2D challenge1;
        public static Texture2D challenge2;
        public static Texture2D challenge3;
        public static Texture2D challenge4;
        public static Texture2D challenge5;
        public static Texture2D challenge6;
        public static Texture2D challenge7;
        public static Texture2D challenge8;
        public static Texture2D challenge9;
        public static Texture2D challenge10;
        public static Texture2D challenge11;
        public static Texture2D challenge12;
        public static Texture2D challenge13;

        public static Texture2D challenge1completed;
        public static Texture2D challenge2completed;
        public static Texture2D challenge3completed;
        public static Texture2D challenge4completed;
        public static Texture2D challenge5completed;
        public static Texture2D challenge6completed;
        public static Texture2D challenge7completed;
        public static Texture2D challenge8completed;
        public static Texture2D challenge9completed;
        public static Texture2D challenge10completed;
        public static Texture2D challenge11completed;
        public static Texture2D challenge12completed;
        public static Texture2D challenge13completed;

        public static Texture2D ChallengeBacknormal;
        public static Texture2D ChallengeBackHover;
        //Map

        public static Texture2D backgroundMap1_0;
        public static Texture2D backgroundMap1_1;
        public static Texture2D backgroundMap1_2;
        public static Texture2D backgroundMap1_3;
        public static Texture2D backgroundMap1_4;
        public static Texture2D backgroundMap1_5;
        public static Texture2D backgroundMap1_6;
        public static Texture2D backgroundMap1_7;
        public static Texture2D backgroundMap1_8;

        public static Texture2D backgroundMap2_0;
        public static Texture2D backgroundMap2_1;
        public static Texture2D backgroundMap2_2;
        public static Texture2D backgroundMap2_3;
        public static Texture2D backgroundMap2_4;
        public static Texture2D backgroundMap2_5;
        public static Texture2D backgroundMap2_6;
        public static Texture2D backgroundMap2_7;

        public static Texture2D star;

        //Ass
        public static Texture2D Ass_normal;
        public static Texture2D Ass_megadroid;
        public static Texture2D Ass_gigadroid;
        public static Texture2D Ass_defentant;
        public static Texture2D Ass_steelKnight;
        public static Texture2D Ass_thunderbolt1;
        public static Texture2D Ass_thunderbolt2;

        // Live Tile
        public static Texture2D selectedTile;
        //1
        public static Texture2D balanceTile;
        public static Texture2D thornTile;
        public static Texture2D sealTile;
        public static Texture2D fireTile;
        //2
        public static Texture2D speedTile;
        public static Texture2D bombTile;
        public static Texture2D swordTile;
        public static Texture2D phoenixTile;
        //3
        public static Texture2D angryTile;
        public static Texture2D cutterTile;
        public static Texture2D slashTile;
        public static Texture2D iceFangTile;
        //4
        public static Texture2D healTile;
        public static Texture2D herbTile;
        public static Texture2D hornTile;
        //5
        public static Texture2D shieldTile;
        public static Texture2D waveTile;
        public static Texture2D steamTile;

        //GestureAnimation
        public static Animation gestureVerticle;
        public static Animation gestureHorizontal;
        public static Animation gestureTap;
        public static Animation gestureU;
        public static Animation gestureV;
        public static Animation gestureN;
        public static Animation gestureZ;
        public static Animation gestureO;
        public static Animation gestureNULL;
        public static Animation gestureNone;

        //WorldMap
        public static Texture2D world1;
        public static Texture2D world2;
        public static Texture2D world3;


        // Victory Scene
        public static Texture2D quangsang;
        public static Texture2D vicBackground;
        // Notification
        public static Texture2D notifBackground;
        public static Texture2D facebookButton;
        public static Texture2D twitterButton;
        public static Texture2D facebookButton_Hover;
        public static Texture2D twitterButton_Hover;
        public static Texture2D nextButton;
        public static Texture2D nextButtonHover;
        public static Texture2D backButtonRight;

        public static Texture2D inforAchie;
        public static Texture2D typeAchie;
        
        public static Texture2D inforAss;
        public static Texture2D typeAss;
        public static Texture2D circle;
        
        public static Texture2D inforCycle;
        public static Texture2D typeCycle;
        public static Texture2D crusher1;
        public static Texture2D crusher2;
        public static Texture2D crusher3;
        public static Texture2D george1;
        public static Texture2D george2;
        public static Texture2D george3;
        public static Texture2D ray1;
        public static Texture2D ray2;
        public static Texture2D ray3;
        public static Texture2D soul1;
        public static Texture2D soul2;
        public static Texture2D soul3;

        public static Texture2D demo1;
        public static Texture2D demo2;
        public static Texture2D demo3;



        public static Texture2D inforEle;
        public static Texture2D typeEle;

        /* BuidlTeamScene */
        // vung giua
        public static Texture2D bottomHalf;
        public static Texture2D topHalf;
        public static Texture2D middlePart;
        public static Texture2D positionI;
        public static Texture2D positionII;
        public static Texture2D positionIII;
        public static Texture2D positionFull;
        public static Texture2D positionHalfFull;

        // canh trai
        public static Texture2D card1_lv1;
        public static Texture2D card1_lv2;
        public static Texture2D card1_lv3;

        public static Texture2D card2_lv1;
        public static Texture2D card2_lv2;
        public static Texture2D card2_lv3;

        public static Texture2D card3_lv1;
        public static Texture2D card3_lv2;
        public static Texture2D card3_lv3;

        public static Texture2D card4_lv1;
        public static Texture2D card4_lv2;
        public static Texture2D card4_lv3;

        public static Texture2D card5_lv1;
        public static Texture2D card5_lv2;
        public static Texture2D card5_lv3;

        public static Texture2D blankCardRobo;
        public static Texture2D emptyPosRobo;
        public static Texture2D leftBg;
        public static Texture2D textLeft;
        public static Texture2D upSymbolLeft;
        public static Texture2D downSymbolLeft;

        // canh phai.
        public static Texture2D megadroidCard;
        public static Texture2D gigadroidCard;
        public static Texture2D defentantCard;
        public static Texture2D steelKnightCard;
        public static Texture2D thunderbolt1Card;
        public static Texture2D thunderbolt2Card;

        public static Texture2D blankCardAss;
        public static Texture2D emptyPosAss;
        public static Texture2D rightBg;
        public static Texture2D textRight;
        public static Texture2D upSymbolRight;
        public static Texture2D downSymbolRight;

        // button
        public static Texture2D backButtonAtBuildTeam;
        public static Texture2D backTextAtBuildTeam;
        public static Texture2D startButtonAtBuildTeam;
        public static Texture2D startTextAtBuildTeam;

        // bullet
        public static Texture2D bulletNum2;
        public static Texture2D bulletTrash6;
        public static Texture2D bulletTrash10;
        public static Texture2D bulletTrash11;

        //health
        public static Texture2D healthTexture;

        // boss 2 animation
        public static Animation appear;
        public static Animation disappear;
        public static Animation gas;
        public static Animation boss2_attack1;
        public static Animation boss2_attack2;
        public static Animation boss2_bullet;
        public static Animation boss2_die1;
        public static Animation boss2_die2;

        // skill lightball
        public static Texture2D lightball;
        public static Texture2D num2_cardLV1;
        public static Texture2D num2_cardLV2; 
        public static Texture2D num2_cardLV3;
        public static Texture2D num2_iconLV1;
        public static Texture2D num2_iconLV2;
        public static Texture2D num2_iconLV3;

        // texture skill gigaslash
        public static Texture2D announceNum1;
        public static Texture2D announceNum3;
        public static Texture2D skillCombination;
        public static Texture2D timeHole;
        public static Texture2D skyPair;
        public static Texture2D landPair;
        public static ParticleEffect effectGiga;
    }
}
