using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class IntroMapTwo : Intro
    {
        GameManager game;
        Texture2D story;
        Vector2 position;
        int timeAction = 0;
        float scale = 1.5f, rotate = 0.0f, opacity = 1f;
        SpriteFont font;

        Button skipButton;
        Color color;
        //Message testMessage;
        //Song typeWriter;
        List<DisplayMessage> messageIntro;
        bool[] isShowed;
        Song introSong;
        public IntroMapTwo(GameManager game)
        {
            this.game = game;
            color = Color.White;
            messageIntro = new List<DisplayMessage>();
            Data loadData = new Data();
            isShowed = new bool[4];
            for (int i = 0; i < 4; i++)
                isShowed[i] = false;
            isShowed[0] = true;

            string number1 = "After Series of tense battles, Sam emerge victorious. \nGreen Heaven is saved. Everybody is happy.";
            messageIntro.Add(new DisplayMessage(game,number1, TimeSpan.FromSeconds(4.0), 0, new Vector2(100, 100), Color.White));
            string number2 = "But one day,the monsters return.";
            messageIntro.Add(new DisplayMessage(game, number2, TimeSpan.FromSeconds(4.0), 0, new Vector2(100, 100), Color.White));
            string number3 = "This time, with greater number.";
            messageIntro.Add(new DisplayMessage(game, number3, TimeSpan.FromSeconds(4.0), 0, new Vector2(100, 240), Color.White));
            string number4 = "Cannot sit there waiting... \nto be devoured by the swarm, \nhe decide it's time to put an end to this blight.";
            messageIntro.Add(new DisplayMessage(game, number4, TimeSpan.FromSeconds(4.0), 0, new Vector2(510, 333), Color.White));
            
            
            position = new Vector2(0,0);
            Vector2 buttonPos = new Vector2(GlobalClass.ScreenWidth + Asset.skipButton.Width * 0.625f / 6, GlobalClass.ScreenHeight - Asset.skipButton.Height * 0.625f / 2 - 20);
            skipButton = new Button(Asset.skipButton, Asset.skipButton, Asset.skipButton, 0.625f, 0f, buttonPos);

            skipButton.Clicked += new EventHandler(skipIntro_Clicked);
            skipButton.Hover += new EventHandler(skipIntro_Hover);
            skipButton.NotHover += new EventHandler(skipIntro_NotHover);

            //testMessage = new Message(this.game, new Vector2(200, 300), "build_n1");
            introSong = game.Content.Load<Song>("sounds\\intro");
            MediaPlayer.Volume = 1f;
            MediaPlayer.Play(introSong);
        }
        public void skipIntro_Hover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X > GlobalClass.ScreenWidth + Asset.skipButton.Width * 0.625f / 6 - 2 * Asset.skipButton.Width * 0.625f / 3 + 8)
            {
                t.position.X -= 4;
            }
        }
        public void skipIntro_NotHover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X < GlobalClass.ScreenWidth + Asset.skipButton.Width * 0.625f / 6)
            {
                t.position.X += 4;
            }
        }
        public void skipIntro_Clicked(object O, EventArgs e)
        {
            game.StartMap();
        }
        public override void Play(GameTime gameTime)
        {

            skipButton.Update(gameTime);
            timeAction += gameTime.ElapsedGameTime.Milliseconds/16;
            if (timeAction <= 200)
            {
                position.X -= 2;
            }
            if (timeAction > 200 && timeAction < 400)
            {
                position.Y -= 2;
                position.X += 2;
                scale -= 0.00005f;
            }
            if (timeAction == 400)
            {
                isShowed[0] = false;
            }
            if (timeAction == 500)
            {
                isShowed[1] = true;
            }
            if (timeAction > 400 && timeAction < 600)
            {
                position.Y -= 2;
                position.X -= 2;
                scale -= 0.00005f;
            }
            if (timeAction > 600 && timeAction < 700)
            {
                position.X -= 4;
                scale += 0.003f;
            }
            if (timeAction == 800)
                isShowed[1] = false;

            if (timeAction == 820)
                isShowed[2] = true;

            if (timeAction == 1200)
                isShowed[2] = false;

            if (timeAction == 1200)
                isShowed[3] = true;
            if (timeAction == 1650)
                isShowed[3] = false;

            if (timeAction > 700 && timeAction < 800)
            {
                position.X += 4;
                position.Y -= 1;
                scale -= 0.003f;
            }
            if (timeAction > 800 && timeAction < 1000)
            {
                position.X += 2;
                position.Y -= 4;
                scale += 0.0003f;
            }
            if (timeAction > 1000 && timeAction < 1100)
            {
                position.X -= 1;
                position.Y += 2;
            }
            if (timeAction > 1100 && timeAction < 1300)
            {
                position.X -= 3;
                position.Y += 1;
                scale += 0.0003f;
            }

            if (timeAction > 1300 && timeAction < 1500)
            {
                position.Y -= 1;
                scale -= 0.00008f;
            }
            if (timeAction > 1700)
            {
                game.StartMap();
            }
            for (int i = 0; i < isShowed.Length; i++)
            {
                if (isShowed[i])
                {
                    messageIntro[i].Update(gameTime);
                }
            }

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < isShowed.Length; i++)
            {
                if (isShowed[i])
                {
                    messageIntro[i].Draw(spriteBatch, Asset.font);
                }
            }
            skipButton.Draw(spriteBatch);

            spriteBatch.Draw(Asset.introTwo, position, null, color*opacity, rotate, Vector2.Zero, scale, SpriteEffects.None, 1f);

        }
    }
}
