using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes
{

    public class IntroductionScene
    {
        Intro introduction;

        public IntroductionScene(GameManager game)
        {

            if (GlobalClass.curMapPlaying == 1)
            {
                introduction = new IntroMapOne(game);
            }
            else if (GlobalClass.curMapPlaying == 2)
            {
                introduction = new IntroMapTwo(game);
            }
        }
        public void Update(GameTime gameTime)
        {
            introduction.Play(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            introduction.Draw(spriteBatch);
        }
    }
}
