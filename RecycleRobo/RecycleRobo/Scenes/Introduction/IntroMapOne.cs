using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class IntroMapOne : Intro
    {
        GameManager game;
        Texture2D story;
        Vector2 position;
        int timeAction = 0;
        float scale = 1.8f, rotate = 0.0f, opacity = 1f;
        SpriteFont font;
        Button skipButton;
        Color color;
        List<DisplayMessage> messageIntro;
        bool[] isShowed;
        Song introSong;
        public IntroMapOne(GameManager game)
        {
            this.game = game;
            color = Color.White;
            messageIntro = new List<DisplayMessage>();
            Data loadData = new Data();

            isShowed = new bool[5];
            for (int i = 0; i < 5; i++)
                isShowed[i] = false;
            isShowed[0] = true;

            string number1 = "\"Heaven on Earth\", \"jewel of the world\" are just a few \nof beautiful names people gave to Green Heaven village.";
            messageIntro.Add(new DisplayMessage(game,number1, TimeSpan.FromSeconds(4.0), 0, new Vector2(100, 100), Color.White));
            string number2 = "from here, a boy named Sam, leaving his village \nto study University of Engineering and Technology.";
            messageIntro.Add(new DisplayMessage(game, number2, TimeSpan.FromSeconds(4.0), 0, new Vector2(500, 400), Color.White));
            string number3 = "A few years later. By the \ntime he returns home, \nGreen Heaven is no more. \nIt is dominated by \nhuge mountains of trash and \nunknown monsters.";
            messageIntro.Add(new DisplayMessage(game, number3, TimeSpan.FromSeconds(4.0), 0, new Vector2(620, 240), Color.White));
            string number4 = "It's time to use \nhis knowledge \nto bring \"Green Heaven\" back.";
            messageIntro.Add(new DisplayMessage(game, number4, TimeSpan.FromSeconds(4.0), 0, new Vector2(800, 333), Color.White));
            string number5 = "...and His adventure \nstarts from here!";
            messageIntro.Add(new DisplayMessage(game, number5, TimeSpan.FromSeconds(4.0), 0, new Vector2(700, 333), Color.White));

            position = new Vector2(-100,0);

            Vector2 buttonPos = new Vector2(GlobalClass.ScreenWidth + Asset.skipButton.Width*0.625f/6, GlobalClass.ScreenHeight - Asset.skipButton.Height*0.625f/2 - 20);
            skipButton = new Button(Asset.skipButton, Asset.skipButton, Asset.skipButton, 0.625f, 0f, buttonPos);
            
            skipButton.Clicked += new EventHandler(skipIntro_Clicked);
            skipButton.Hover += new EventHandler(skipIntro_Hover);
            skipButton.NotHover += new EventHandler(skipIntro_NotHover);


            introSong = game.Content.Load<Song>("sounds\\intro");
            MediaPlayer.Volume = 1f;
            MediaPlayer.Play(introSong);
        }
        public void skipIntro_Hover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X > GlobalClass.ScreenWidth + Asset.skipButton.Width * 0.625f / 6 - 2 * Asset.skipButton.Width * 0.625f / 3 + 8)
            {
                t.position.X -= 4;
            }
        }
        public void skipIntro_NotHover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X < GlobalClass.ScreenWidth + Asset.skipButton.Width * 0.625f / 6)
            {
                t.position.X += 4;
            }
        }
        public void skipIntro_Clicked(object O, EventArgs e)
        {
            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            MediaPlayer.Play(game.Content.Load<Song>("sounds\\main_sound"));
            game.StartMap();
        }
        public override void Play(GameTime gameTime)
        {

            skipButton.Update(gameTime);
            timeAction += gameTime.ElapsedGameTime.Milliseconds/16;
            if (timeAction <= 200)
            {
                position.Y -= 2;
                scale += 0.001f;

            }
            if (timeAction >= 200 && timeAction < 600)
            {
                position.Y -= 2;
                scale -= 0.0006f;
            }
            if (timeAction == 400)
            {
                isShowed[0] = false;
                isShowed[1] = true;
            }


            if (timeAction >= 720 && timeAction < 820)
            {
                position.Y += 12;
                position.X -= 14;
                scale += 0.0008f;
            }
            
            if (timeAction == 720)
                isShowed[1] = false;
            if (timeAction > 820 && timeAction < 850)
            {
                color = Color.LightGray;
                opacity += (float)((new Random().NextDouble() * 2) - 1) * (opacity);
            }
            if (timeAction > 820 && timeAction < 920)
            {

                isShowed[2] = true;
                position.Y -= 1;
            }
            if (timeAction == 850)
            {
                color = Color.White;
                opacity = 1f;
            }
            if (timeAction > 1300 && timeAction < 1450)
            {
                isShowed[2] = false;
                position.Y -= 6;
                position.X += 6;
                scale -= 0.0008f;
            }
            if (timeAction > 1450 && timeAction < 1550)
            {
                isShowed[3] = true;
                messageIntro[3].position.X += 1;
                messageIntro[3].position.Y -= 1;
                position.Y += 2;
                position.X += 2;
                scale -= 0.001f;
            }
            if (timeAction > 1550 && timeAction < 1600)
            {
                messageIntro[3].position.X -= 1;
            }
            if (timeAction > 1550 && timeAction < 1700)
            {
                position.Y -= 3;
                position.X -= 3;
            }
            if (timeAction > 1700 && timeAction < 1900)
            {
                isShowed[3] = false;

                position.X -= 3;
                scale += 0.001f;

            }
            if (timeAction > 1900 && timeAction < 2010)
            {
                isShowed[4] = true;
                position.X += 1;
                scale -= 0.001f;
                position.Y += 1;

            }

            if (timeAction > 2010 && timeAction < 2150)
            {
                position.Y += 2;
                scale += 0.001f;
                position.X -= 1;
                messageIntro[4].position.Y += 2;


            }
            if (timeAction > 2150 && timeAction < 2200)
            {
                messageIntro[4].position.X += 2;
            }
            if (timeAction > 2250)
            {
                game.StartMap();
            }
            for (int i = 0; i < isShowed.Length; i++)
            {
                if (isShowed[i])
                {
                    messageIntro[i].Update(gameTime);
                }
            }

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < isShowed.Length; i++)
            {
                if (isShowed[i])
                {
                    messageIntro[i].Draw(spriteBatch, Asset.font);
                }
            }
            skipButton.Draw(spriteBatch);

            spriteBatch.Draw(Asset.introOne, position, null, color*opacity, rotate, Vector2.Zero, scale, SpriteEffects.None, 1f);

        }
    }
}
