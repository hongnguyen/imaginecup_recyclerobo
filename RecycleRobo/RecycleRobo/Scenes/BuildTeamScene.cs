﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;

using RecycleRobo.Scenes;
using LoadData;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class BuildTeamScene
    {
        public enum BuildTeamState
        {
            BUILD,
            DONE
        }

        public BuildTeamState buildTeamState = BuildTeamState.BUILD;

        private GameManager Game;
        protected List<RoboToSelect> RobotToSelect;
        protected List<RoboToSelect> SelectedRobots;
        protected RoboToSelect curSelectedRobo;
        protected List<Slot> slots;
        protected List<AssistantToSelect> AssistantsToSelect;
        protected AssistantToSelect curAssistantSelected;
        protected Background bg;
        bool isDragging;
        protected MouseState preMouseState;
        private float opacity = 1f;
        public const float scaleOffical = 0.625f;
        public TextDetail textLeft, textRight;
        public UpDownScroller roboUpDown;
        public UpDownScroller assistantUpDown;

        public float effectLeft = 0, effectRight = 0;
        public float effectTop = 0, effectBottom = 0;
        public float delayEffectTime = 0, countDownToStart = 0;
        public static int[] Robo_Evolution;

        public List<Button2> buttons = new List<Button2>();

        public BuildTeamScene(GameManager game)
        {
            GlobalClass.isInMap = false;
            Game = game;
            RobotToSelect = new List<RoboToSelect>();
            SelectedRobots = new List<RoboToSelect>();
            slots = new List<Slot>();

            LevelData loadBackground = game.Content.Load<LevelData>("Data/Map/Map" + GlobalClass.curMapPlaying
                                                                + "/Level" + GlobalClass.curLevelPlaying);
            bg = new Background(game.Content, "Maps/Backgrounds/" + loadBackground.background, 0.99f);

            /** moi lam**/
            textLeft = new TextDetail(Asset.textLeft, new Vector2(185, 0), "up", 1);
            textRight = new TextDetail(Asset.textRight, new Vector2(1027, 0), "down", 1);
            roboUpDown = new UpDownScroller(Asset.upSymbolLeft, Asset.downSymbolLeft, Vector2.Zero);
            assistantUpDown = new UpDownScroller(Asset.upSymbolRight, Asset.downSymbolRight, new Vector2(GlobalClass.ScreenWidth - Asset.upSymbolRight.Width * scaleOffical, 0));

            initCycleBots();
            InitListSlot();
            InitMenu();
            initAsistants();
            readConfigFile();

            isDragging = false;
        }
        public void initCycleBots()
        {
            List<List<Texture2D>> roboFullIcon = new List<List<Texture2D>>();
            List<Texture2D> roboIcon;
            Robo_Evolution = new int[5];

            roboIcon = new List<Texture2D>();
            roboIcon.Add(Asset.card1_lv1);
            roboIcon.Add(Asset.card2_lv1);
            roboIcon.Add(Asset.card3_lv1);
            roboIcon.Add(Asset.card4_lv1);
            roboIcon.Add(Asset.card5_lv1);
            roboFullIcon.Add(roboIcon);

            roboIcon = new List<Texture2D>();
            roboIcon.Add(Asset.card1_lv2);
            roboIcon.Add(Asset.card2_lv2);
            roboIcon.Add(Asset.card3_lv2);
            roboIcon.Add(Asset.card4_lv2);
            roboIcon.Add(Asset.card5_lv2);
            roboFullIcon.Add(roboIcon);

            roboIcon = new List<Texture2D>();
            roboIcon.Add(Asset.card1_lv3);
            roboIcon.Add(Asset.card2_lv3);
            roboIcon.Add(Asset.card3_lv3);
            roboIcon.Add(Asset.card4_lv3);
            roboIcon.Add(Asset.card5_lv3);
            roboFullIcon.Add(roboIcon);

            Data loadData = new Data();
            NumberOneUpgrade att1 = loadData.Load<NumberOneUpgrade>("Player/NumberOne.xml");
            NumberTwoUpgrade att2 = loadData.Load<NumberTwoUpgrade>("Player/NumberTwo.xml");
            NumberThreeUpgrade att3 = loadData.Load<NumberThreeUpgrade>("Player/NumberThree.xml");
            NumberFourUpgrade att4 = loadData.Load<NumberFourUpgrade>("Player/NumberFour.xml");
            NumberFiveUpgrade att5 = loadData.Load<NumberFiveUpgrade>("Player/NumberFive.xml");

            int[] robotLevel = new int[5];
            robotLevel[0] = att1.level;
            robotLevel[1] = att2.level;
            robotLevel[2] = att3.level;
            robotLevel[3] = att4.level;
            robotLevel[4] = att5.level;

            roboIcon = new List<Texture2D>();
            for (int i = 0; i < robotLevel.Length; i++)
            {
                for (int j = CyclebotView.transitionLevel.GetLength(0) - 1; j >= 0; j--)
                {
                    if (robotLevel[i] >= CyclebotView.transitionLevel[j, i])
                    {
                        Robo_Evolution[i] = j+1;
                        roboIcon.Add(roboFullIcon[j][i]);
                        break;
                    }
                }
            }

            //for (int i = 0; i < 5; i++ )
            //{
            //    Console.WriteLine(i + " lv " + Robo_Evolution[i]);
            //}
            

            Data data = new Data();
            RoboAvailable RobosAvailable = data.Load<RoboAvailable>("Player/RoboAvailable.xml");

            Texture2D TextureImage; float vector_y = 140;
            for (int i = 0; i < 5; i++)
            {
                if (checkRoboAvailable(i + 1, RobosAvailable.available))
                {
                    TextureImage = roboIcon.ElementAt(i);
                    Vector2 position = new Vector2(85, vector_y);
                    Animation idleAnimation = null;
                    RoboToSelect initRobo = null;
                    switch (i + 1)
                    {
                        case 1:
                            idleAnimation = GlobalClass.getAnimationByName("NUMBER1_IDLE_" + Robo_Evolution[i]);
                            initRobo = new RoboToSelect(TextureImage, "NumberOne", position, idleAnimation, robotLevel[0]);
                            break;
                        case 2:
                            idleAnimation = GlobalClass.getAnimationByName("NUMBER2_IDLE_" + Robo_Evolution[i]);
                            initRobo = new RoboToSelect(TextureImage, "NumberTwo", position, idleAnimation, robotLevel[1]);
                            break;
                        case 3:
                            idleAnimation = GlobalClass.getAnimationByName("NUMBER3_IDLE_" + Robo_Evolution[i]);
                            initRobo = new RoboToSelect(TextureImage, "NumberThree", position, idleAnimation, robotLevel[2]);
                            break;
                        case 4:
                            idleAnimation = GlobalClass.getAnimationByName("NUMBER4_IDLE_" + Robo_Evolution[i]);
                            initRobo = new RoboToSelect(TextureImage, "NumberFour", position, idleAnimation, robotLevel[3]);
                            break;
                        case 5:
                            idleAnimation = GlobalClass.getAnimationByName("NUMBER5_IDLE_" + Robo_Evolution[i]);
                            initRobo = new RoboToSelect(TextureImage, "NumberFive", position, idleAnimation, robotLevel[4]);
                            break;
                    }
                    RobotToSelect.Add(initRobo);
                    vector_y += TextureImage.Height * 0.625f + 40;
                }
            }

            // khoi tao phan tu cuoi cung empty
            RoboToSelect robo = new RoboToSelect(Asset.blankCardRobo, "", new Vector2(85, vector_y), null, 0);
            robo.empty = Asset.blankCardRobo;
            robo.IsChoose = true;
            RobotToSelect.Add(robo);
        }
        public void InitListSlot()
        {
            Slot slot;
            slot = new Slot(Game.Content, new Vector2(455, 35), 1);
            slots.Add(slot);

            slot = new Slot(Game.Content, new Vector2(305, 300), 2);
            slots.Add(slot);

            slot = new Slot(Game.Content, new Vector2(655, 300), 3);
            slots.Add(slot);
        }
        public void InitMenu()
        {
            Button2 backButton = new Button2 ("back", Asset.backButtonAtBuildTeam, Asset.backTextAtBuildTeam, new Vector2(450, 585));
            backButton.bottomPos = new Vector2(backButton.originPos.X, 625);
            buttons.Add(backButton);
            Button2 startButton = new Button2 ("start", Asset.startButtonAtBuildTeam, Asset.startTextAtBuildTeam, new Vector2(GlobalClass.ScreenWidth - 430, 585));
            startButton.bottomPos = new Vector2(startButton.originPos.X, 625);
            buttons.Add(startButton);
        }

        public void initAsistants()
        {
            AssistantsToSelect = new List<AssistantToSelect>();
            float vecto_y = 110;

            AssistantData assData = Game.Content.Load<AssistantData>("Data/Assistant/AssistantData");

            List<AssistantInfo> assInfo = assData.Assistants;
            List<AssistantStatus> assistantStatus = new Data().Load<List<AssistantStatus>>("Assistant\\AssistantStatus.xml");

            foreach (AssistantStatus assStt in assistantStatus)
            {
                if (assStt.isBought)
                {
                    foreach (AssistantInfo e in assInfo)
                    {
                        if (e.type == assStt.type)
                        {
                            Assistant assistantTemp = new Assistant(e.type, e.defence, e.speed, e.damage, e.heal);
                            Vector2 position = new Vector2(1130, vecto_y);
                            vecto_y += assistantTemp.texture.Height + 70;
                            AssistantsToSelect.Add(new AssistantToSelect(assistantTemp, position));
                        }
                    }
                }
            }

            // khoi tao phan tu cuoi cung empty
            Assistant tmp = new Assistant("", 0, 0, 0, 0);
            tmp.textureAtBuildTeam = Asset.blankCardAss;
            AssistantToSelect blankCard = new AssistantToSelect(tmp, new Vector2(1130, vecto_y));
            blankCard.empty = Asset.blankCardAss;
            blankCard.isChoose = true;
            AssistantsToSelect.Add(blankCard);
        }
        public bool checkRoboAvailable(int RoboIndex, int[] RobosAvailable)
        {
            for (int i = 0; i < RobosAvailable.Length; i++)
                if (RoboIndex == RobosAvailable[i])
                    return true;

            return false;
        }
        public RoboToSelect getRoboIsHover(Point mouse)
        {
            foreach (RoboToSelect robo in RobotToSelect)
            {
                if (robo.isHover(mouse))
                {
                    return robo;
                }
            }
            return null;
        }
        public void UpdateScroller(GameTime gameTime)
        {
            roboUpDown.Update(gameTime);
            assistantUpDown.Update(gameTime);

            foreach (RoboToSelect robo in RobotToSelect)
            {
                RoboToSelect first = RobotToSelect.ElementAt(0);
                RoboToSelect last = RobotToSelect.ElementAt(RobotToSelect.Count - 1);

                if (last.CurrentPos.Y + last.Texture.Height * last.scale / 2 <= GlobalClass.ScreenHeight - 50 && roboUpDown.state == UpDownScroller.UpDownState.DOWN)
                {
                    return;
                }

                if (first.CurrentPos.Y - last.Texture.Height * last.scale / 2 >= 50 && roboUpDown.state == UpDownScroller.UpDownState.UP)
                {
                    return;
                }

                if (roboUpDown.isMove)
                {
                    robo.CurrentPos.Y += roboUpDown.value * 3;
                    robo.OriginalPos.Y += roboUpDown.value * 3;
                }
            }

            foreach (AssistantToSelect ass in AssistantsToSelect)
            {
                AssistantToSelect first = AssistantsToSelect.ElementAt(0);
                AssistantToSelect last = AssistantsToSelect.ElementAt(AssistantsToSelect.Count - 1);

                if (last.curPosition.Y + last.assistant.textureAtBuildTeam.Height * last.scale / 2 <= GlobalClass.ScreenHeight - 50 && assistantUpDown.state == UpDownScroller.UpDownState.DOWN)
                {
                    return;
                }

                if (first.curPosition.Y - last.assistant.textureAtBuildTeam.Height * last.scale / 2 >= 50 && assistantUpDown.state == UpDownScroller.UpDownState.UP)
                {
                    return;
                }

                if (assistantUpDown.isMove)
                {
                    ass.curPosition.Y += assistantUpDown.value * 3;
                    ass.originPosition.Y += assistantUpDown.value * 3;
                }
            }

        }
        public void Update(GameTime gameTime)
        {
            MouseState curMouseState = Mouse.GetState();
#region  State Build Team
            if (buildTeamState == BuildTeamState.BUILD)
            {
                textLeft.Update(gameTime); textRight.Update(gameTime);

                if (curSelectedRobo == null)
                    UpdateScroller(gameTime);

                Point mousePoint = new Point(MouseHelper.MousePosition(curMouseState).X, MouseHelper.MousePosition(curMouseState).Y);
                //if (curMouseState.LeftButton == ButtonState.Released && !roboUpDown.isMove)
                //{
                //    isDragging = false;
                //    RoboToSelect roboIsHover = getRoboIsHover(mousePoint);
                //    if (roboIsHover != null)
                //    {
                //        roboIsHover.scale = roboIsHover.scaleHover;
                //    }
                //    else
                //    {
                //        foreach (RoboToSelect robo in RobotToSelect)
                //            robo.scale = robo.staticScale;
                //    }
                //    //Console.WriteLine(mousePoint);
                //}

                if (curMouseState.LeftButton == ButtonState.Pressed && preMouseState.LeftButton == ButtonState.Released)
                {
                    //chon robot tham chien;
                    foreach (RoboToSelect robo in RobotToSelect)
                    {
                        if (robo.isHover(mousePoint) && !roboUpDown.isMove)
                        {
                            curSelectedRobo = new RoboToSelect(robo);
                            robo.IsChoose = true;
                            isDragging = true;
                        }
                    }
                    // Chon robot muon huy bo
                    foreach (RoboToSelect robo in SelectedRobots.ToArray())
                    {
                        if (robo.isHover(mousePoint))
                        {
                            //Console.WriteLine("chon robot huy bo");
                            curSelectedRobo = robo;
                            curSelectedRobo.slot.isPlaced = false;
                            curSelectedRobo.slot.slotState = Slot.SlotState.NONE;
                            SelectedRobots.Remove(curSelectedRobo);
                            isDragging = true;
                        }

                        //chon assistant da duoc lap
                        if (robo.assistantEquiped != null)
                        {
                            if (getSlotBoundOf(robo).Contains(mousePoint))
                            {
                                curAssistantSelected = new AssistantToSelect(robo.assistantEquiped);
                                robo.slot.slotState = Slot.SlotState.ROBO;
                                robo.assistantEquiped = null;
                                isDragging = true;
                                break;
                            }
                        }
                    }

                    // chon assistant de lap vao robot;
                    takeAssistant(mousePoint);
                }

                if (isDragging)
                {
                    Vector2 mouseMove = new Vector2(MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(curMouseState).X, MouseHelper.MousePosition(preMouseState).Y - MouseHelper.MousePosition(curMouseState).Y);
                    if (curSelectedRobo != null)
                        curSelectedRobo.CurrentPos -= mouseMove;

                    if (curAssistantSelected != null)
                        curAssistantSelected.curPosition -= mouseMove;

                }

                if (curMouseState.LeftButton == ButtonState.Released && preMouseState.LeftButton == ButtonState.Pressed)
                {
                    if (curSelectedRobo != null)
                    {
                        int checkFlag = 0;
                        for (int i = 0; i < slots.Count; i++)
                        {
                            Slot slot = slots.ElementAt(i);
                            if (curSelectedRobo.getRectangle().Intersects(slot.BoundingBox()))
                            {
                                if (!slot.isPlaced)
                                {
                                    slot.isPlaced = true;
                                    curSelectedRobo.slot = slot;
                                    if (curSelectedRobo.assistantEquiped != null) curSelectedRobo.slot.slotState = Slot.SlotState.FULL;
                                    else curSelectedRobo.slot.slotState = Slot.SlotState.ROBO;
                                    curSelectedRobo.CurrentPos = slot.pos + new Vector2(86.5f, 113);
                                    SelectedRobots.Add(curSelectedRobo);
                                }
                                else
                                {
                                    if (curSelectedRobo.assistantEquiped != null)
                                        updateListAssistant(curSelectedRobo.assistantEquiped);
                                    updateRoboToSelect(curSelectedRobo);
                                }
                                curSelectedRobo = null;
                                break;
                            }
                            else if (!curSelectedRobo.getRectangle().Intersects(slot.BoundingBox()))
                            {
                                checkFlag++;
                                if (checkFlag == slots.Count)
                                {
                                    if (curSelectedRobo.assistantEquiped != null)
                                        updateListAssistant(curSelectedRobo.assistantEquiped);
                                    updateRoboToSelect(curSelectedRobo);
                                    curSelectedRobo = null;
                                }
                            }
                        }
                    }

                    if (curAssistantSelected != null)
                    {
                        updateAssistantState();
                    }

                    isDragging = false;
                }

                updateMenu(gameTime);
            }
#endregion            
#region State Effect
            else
            {
                delayEffectTime += gameTime.ElapsedGameTime.Milliseconds;
                if (delayEffectTime > 10)
                {
                    delayEffectTime = 0;
                    effectLeft -= 5f;
                    effectRight += 5f;
                    effectTop -= 8;
                    effectBottom += 8;
                }
                opacity -= 0.03f;

                countDownToStart += gameTime.ElapsedGameTime.Milliseconds;
                if (countDownToStart >= 1300)
                {
                    GlobalClass.RobotSelected = SelectedRobots;
                    Game.PlayGame();
                }
            }
#endregion
            preMouseState = curMouseState;
        }

        private void updateMenu(GameTime gameTime)
        {
            foreach (Button2 button in buttons)
            {
                button.Update(gameTime);
                if (button.clicked)
                    pressButton(button);
            }
        }

        private void pressButton(Button2 button)
        {
            if (button.name == "back") Game.StartMap();
            else if (button.name == "start")
            {
                if (SelectedRobots.Count == 0) return;
                buildTeamState = BuildTeamState.DONE;
                saveConfigFile();
            }
        }
        public void updateRoboToSelect(RoboToSelect robo)
        {
            for (int i = 0; i < RobotToSelect.Count; i++)
            {
                if (RobotToSelect.ElementAt(i).Name == robo.Name)
                {
                    RobotToSelect.ElementAt(i).IsChoose = false;
                }
            }
        }
        public void takeAssistant(Point mousePoint)
        {
            foreach (AssistantToSelect e in AssistantsToSelect)
            {
                if (e.isHover(mousePoint) && !assistantUpDown.isMove)
                {
                    curAssistantSelected = new AssistantToSelect(e);
                    e.isChoose = true;
                    isDragging = true;
                }
            }
        }
        public void updateListAssistant(AssistantToSelect ass)
        {
            foreach (AssistantToSelect e in AssistantsToSelect)
            {
                if (e.assistant.type == ass.assistant.type)
                {
                    e.isChoose = false;
                }
            }
        }
        public void updateAssistantState()
        {
            if (SelectedRobots.Count == 0)
            {
                updateListAssistant(curAssistantSelected);
                curAssistantSelected = null;
            }
            else
            {
                int checkFlag = 0;
                for (int i = 0; i < SelectedRobots.Count; i++)
                {
                    RoboToSelect robotInSlot = SelectedRobots.ElementAt(i);
                    if (getSlotBoundOf(robotInSlot).Intersects(curAssistantSelected.getRectangle()) && robotInSlot.assistantEquiped == null)
                    {
                        robotInSlot.assistantEquiped = new AssistantToSelect(curAssistantSelected);
                        robotInSlot.slot.slotState = Slot.SlotState.FULL;
                        curAssistantSelected = null;
                        break;
                    }
                    else checkFlag++;
                    if (checkFlag == SelectedRobots.Count)
                    {
                        updateListAssistant(curAssistantSelected);
                        curAssistantSelected = null;
                    }
                }
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            if (buildTeamState == BuildTeamState.BUILD)
            {
                roboUpDown.Draw(spriteBatch); 
                assistantUpDown.Draw(spriteBatch);
                spriteBatch.Draw(Asset.middlePart, new Vector2(273, 20), null, Color.White, 0f, 
                    Vector2.Zero, 0.625f, SpriteEffects.None, 0.97f);
                
                foreach (Button2 button in buttons)
                {
                    button.Draw(spriteBatch);
                }
            }

            textLeft.Draw(spriteBatch,effectLeft); textRight.Draw(spriteBatch, effectRight);
            spriteBatch.Draw(Asset.bottomHalf, new Vector2(273, 20) + new Vector2 (0, effectBottom), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);
            spriteBatch.Draw(Asset.topHalf, new Vector2(273, 20) + new Vector2 (0, effectTop), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);
            spriteBatch.Draw(Asset.leftBg, Vector2.Zero + new Vector2(effectLeft, 0), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);
            spriteBatch.Draw(Asset.rightBg, new Vector2(GlobalClass.ScreenWidth - Asset.rightBg.Width * scaleOffical, 0) + new Vector2(effectRight, 0),
                null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);

            //draw list robot to select.
            foreach (RoboToSelect robot in RobotToSelect)
            {
                if (!robot.IsChoose)
                {
                    //robot.OriginalPos += new Vector2(effectLeft, 0);
                    spriteBatch.Draw(robot.Texture, robot.OriginalPos + new Vector2(effectLeft, 0), new Rectangle (0,0,219,304), Color.White * opacity,
                        0f, robot.center, robot.scale, SpriteEffects.None, 0.4f);
                    spriteBatch.DrawString(Asset.smallFont, robot.level.ToString(), robot.OriginalPos + new Vector2(-3, 75) + new Vector2(effectLeft, 0), Color.Black, 0f, new Vector2(3, 3), robot.scale + 0.3f, SpriteEffects.None, 0.38f);
                }
                else
                    spriteBatch.Draw(robot.empty, robot.OriginalPos, new Rectangle(0, 0, 219, 304), Color.White * opacity,
                        0f, robot.emptyCenter, robot.scale, SpriteEffects.None, 0.4f);
            }

            //draw Assistants
            drawAssistants(spriteBatch, effectRight);

            //draw list robot is selected;
            foreach (RoboToSelect robot in SelectedRobots)
            {
                if (!robot.IsChoose)
                {
                    spriteBatch.Draw(robot.Texture, robot.CurrentPos, new Rectangle(0, 0, 219, 304),
                        Color.White*opacity, 0f, robot.center, robot.scale, SpriteEffects.None, 0.51f);
                    spriteBatch.DrawString(Asset.smallFont, robot.level.ToString(), robot.CurrentPos + new Vector2(-3, 75), Color.Black*opacity, 0f, new Vector2(3, 3), robot.scale + 0.3f, SpriteEffects.None, 0.38f);

                    if (robot.assistantEquiped != null)
                    {
                        robot.assistantEquiped.curPosition = new Vector2(robot.CurrentPos.X + robot.Texture.Width * robot.scale + 5, robot.CurrentPos.Y - 11);
                        spriteBatch.Draw(robot.assistantEquiped.assistant.textureAtBuildTeam, robot.assistantEquiped.curPosition,
                          null, Color.White * opacity, 0f, robot.assistantEquiped.center, robot.assistantEquiped.scale, SpriteEffects.None, 0.4f);
                    }
                }

                if (buildTeamState == BuildTeamState.DONE)
                {
                    robot.drawIdleAnimation(gameTime, spriteBatch, robot.CurrentPos);
                }
            }

            //draw robot when dragging;
            if (curSelectedRobo != null)
            {
                spriteBatch.Draw(curSelectedRobo.Texture, curSelectedRobo.CurrentPos, null, Color.White,
                                    0f, curSelectedRobo.center, curSelectedRobo.scale, SpriteEffects.None, 0.38f);
                spriteBatch.DrawString(Asset.smallFont, curSelectedRobo.level.ToString(), curSelectedRobo.CurrentPos + new Vector2(-3, 75), Color.Black, 0f, new Vector2(3, 3), curSelectedRobo.scale + 0.3f, SpriteEffects.None, 0.36f);

            }

            //draw slots
            foreach (Slot slot in slots)
            {
                slot.Draw(gameTime, spriteBatch, opacity);
            }

            bg.Draw(spriteBatch);
        }
        public void drawAssistants(SpriteBatch spriteBatch, float effect)
        {
            foreach (AssistantToSelect e in AssistantsToSelect)
            {
                e.Draw(spriteBatch, effect, 0.38f);
            }

            if (curAssistantSelected != null)
            {
                curAssistantSelected.Draw(spriteBatch, 0f, 0.36f);
            }
        }
        public Rectangle getSlotBoundOf(RoboToSelect robot)
        {
            Vector2 posRect = new Vector2(robot.CurrentPos.X + robot.Texture.Width * robot.scale + 5, robot.CurrentPos.Y - 11);
            return new Rectangle((int)(posRect.X - 50), (int)(posRect.Y - 60.65f), 100, 121);
        }
        public Rectangle BoundingForScrollKits()
        {
            return new Rectangle((int)(GlobalClass.ScreenWidth - 110 * 2), 0, 110, 690);
        }
        public void saveConfigFile()
        {
            List<BuildTeamConfig> config = new List<BuildTeamConfig>();
            foreach (RoboToSelect robo in SelectedRobots)
            {
                BuildTeamConfig tmp = null;
                if (robo.assistantEquiped != null)
                {
                    tmp = new BuildTeamConfig(robo.Name, robo.assistantEquiped.assistant.type, robo.CurrentPos, robo.assistantEquiped.curPosition, robo.slot.index);
                }
                else
                {
                    tmp = new BuildTeamConfig(robo.Name, "none", robo.CurrentPos, Vector2.Zero, robo.slot.index);
                }
                config.Add(tmp);
            }
            new Data().Save(config, "BuildTeamConfig/config.xml");
        }
        public void readConfigFile()
        {
            List<BuildTeamConfig> config = new Data().Load<List<BuildTeamConfig>>("BuildTeamConfig/config.xml");
            int i = 0;

            foreach (BuildTeamConfig iterator in config)
            {
                RoboToSelect robo = null;
                AssistantToSelect ass = null;

                foreach (RoboToSelect robot in RobotToSelect)
                {
                    if (robot.Name == iterator.nameRobo)
                    {
                        robo = new RoboToSelect(robot);
                        robot.IsChoose = true;
                        robo.CurrentPos = iterator.positionRobo;
                    }
                }

                foreach (AssistantToSelect assis in AssistantsToSelect)
                {
                    if (assis.assistant.type == iterator.nameAss)
                    {
                        ass = new AssistantToSelect(assis);
                        assis.isChoose = true;
                        ass.curPosition = iterator.positionAss;
                    }
                }

                if (robo != null)
                {
                    robo.slot = slots.ElementAt(iterator.index_slot - 1);
                    robo.slot.isPlaced = true;
                    robo.slot.slotState = Slot.SlotState.ROBO;

                    if (ass != null)
                    {
                        robo.assistantEquiped = ass;
                        robo.slot.slotState = Slot.SlotState.FULL;
                    }

                    SelectedRobots.Add(robo);
                    i++;
                }
            }
        }
    }
}
