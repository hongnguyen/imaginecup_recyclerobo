﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using RecycleRobo.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class VictoryScene : EndGameScene
    {

        Data data;
        Level curLevelPlaying;
        String PathFileLevelConfig;
        SoundEffect soundButton;
        int ratingAfterPlay;
        int ratingInFile;

        Texture2D star;
        Vector2 originStarPos;
        Song victorySong;

        //Variables for delay;
        private float delayToStar = 0.0f;
        private float delayToScale = 0.0f;
        public Player[] players;
        //Varialble to count;
        int starCount = 0;
        int scaleCount = 0;

        Background infoRobo;

        //Button
        Button gotIt;

        float[] arrayScale = {0.01f, 0.02f, 0.03f, 0.04f, 0.05f, 0.06f, 0.07f, 0.08f, 0.09f, 0.1f,
                              0.09f, 0.08f, 0.07f, 0.06f, 0.05f, 0.04f, 0.03f, 0.02f, 0.01f, 0.0f,
                              -0.01f, -0.02f, -0.03f, -0.04f, -0.05f, -0.06f, -0.07f, -0.08f, -0.09f, -0.1f,
                              -0.09f, -0.08f, -0.07f, -0.06f, -0.05f, -0.04f, -0.03f, -0.02f, -0.01f, 0.0f};

        SoundEffect startSound;

        public VictoryScene(GameManager game, List<Item> sewagesInRecycleBin)
            : base(game, sewagesInRecycleBin)
        {
            newspaper = Game.Content.Load<Texture2D>("Icon/newspaper-victory");
            oriPosNews = new Vector2(newspaper.Width / 2, newspaper.Height / 2);
            posNews = new Vector2(GlobalClass.ScreenWidth / 2, GlobalClass.ScreenHeight / 2 - 60);

            PathFileLevelConfig = "Map/Map_" + GlobalClass.curMapPlaying + "/Level" + GlobalClass.curLevelPlaying + ".xml";
            data = new Data();
            curLevelPlaying = data.Load<Level>(PathFileLevelConfig);
            ratingInFile = curLevelPlaying.rating;
            soundButton = Game.Content.Load<SoundEffect>("sounds/_upgrade");

            InitFunFact();
            recyclingSewages();
            InitButton();

            calculateRating();
            star = Game.Content.Load<Texture2D>("Icon/star");
            originStarPos = new Vector2(star.Width / 2, star.Height / 2);


            startSound = Game.Content.Load<SoundEffect>("sounds/starSoundEffect");
            victorySong = Game.Content.Load<Song>("sounds\\victory");

            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            MediaPlayer.Play(victorySong);
            
            infoRobo = null;
        }

        public void InitButton()
        {
            Texture2D buttonNormal = Game.Content.Load<Texture2D>("Icon/nextButton-win");
            Texture2D buttonhover = Game.Content.Load<Texture2D>("Icon/nextButton-win-hover");
            Texture2D buttonPress = Game.Content.Load<Texture2D>("Icon/nextButton-win");
            Vector2 buttonPos = new Vector2(900, 630);
            nextLevelButton = new Button(buttonNormal, buttonhover, buttonPress, 0.35f, 0f, buttonPos);
            nextLevelButton.soundButton = soundButton;
            nextLevelButton.Clicked += new EventHandler(nextLevelButton_Clicked);
        }

        public void InitFunFact()
        {
            createRandomFuncFact();

            if (!curLevelPlaying.isUnlock)
            {
                LevelData levelInfo = GlobalClass.LevelInformation;
                if (levelInfo.roboUnlock != 0)
                {
                    funfact = Game.Content.Load<Texture2D>("Icon/funFact_unlock" + levelInfo.roboUnlock);
                    RoboAvailable roboAvailable = data.Load<RoboAvailable>("Player/RoboAvailable.xml");
                    int[] newRoboAvailable = new int[roboAvailable.available.Length + 1];
                    for (int i = 0; i < roboAvailable.available.Length; i++)
                        newRoboAvailable[i] = roboAvailable.available[i];
                    newRoboAvailable[roboAvailable.available.Length] = levelInfo.roboUnlock;
                    data.Save(new RoboAvailable(newRoboAvailable), "Player/RoboAvailable.xml");
                }

                if (levelInfo.assistantUnlock != "none")
                {
                    List<AssistantStatus> assistantStatus = new Data().Load<List<AssistantStatus>>("Assistant\\AssistantStatus.xml");
                    foreach (AssistantStatus ass in assistantStatus)
                    {
                        if (ass.type==levelInfo.assistantUnlock)
                        {
                            ass.isUnlocked = true;
                        }
                    }
                    data.Save(assistantStatus, "Assistant\\AssistantStatus.xml");
                }

                isBonus = true;
            }
            else
            {
                isBonus = false;
            }

            oriPosFunFact = new Vector2(funfact.Width / 2, funfact.Height / 2);
            posFunfact = new Vector2(878, 289);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            if (isShowFunFact)
            {
                for (int i = 0; i < ratingAfterPlay; i++)
                {
                    if (i < starCount)
                    {
                        if (i == 1)
                            spriteBatch.Draw(star, new Vector2(830 + 75 * i, 550), null,
                                Color.White, 0f, originStarPos, 0.7f + arrayScale[scaleCount], SpriteEffects.None, 0.09f);
                        else
                            spriteBatch.Draw(star, new Vector2(830 + 75 * i, 570), null,
                                Color.White, 0f, originStarPos, 0.525f + arrayScale[scaleCount], SpriteEffects.None, 0.09f);
                    }
                }
            }

            if (infoRobo != null)
            {
                infoRobo.Draw(spriteBatch);
                gotIt.Draw(spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (isShowFunFact)
            {
                if (starCount < ratingAfterPlay)
                {
                    delayToStar += gameTime.ElapsedGameTime.Milliseconds;
                    if (delayToStar >= 900)
                    {
                        startSound.Play();
                        delayToStar = 0.0f;
                        starCount++;
                    }
                }

                delayToScale += gameTime.ElapsedGameTime.Milliseconds;
                if (delayToScale >= 50)
                {
                    delayToScale = 0.0f;
                    scaleCount++;
                }
                if (scaleCount >= arrayScale.Length)
                    scaleCount = 0;

            }

            if (infoRobo!=null)
            {
                gotIt.Update(gameTime);
            }
        }

        public void nextLevelButton_Clicked(object sender, EventArgs e)
        {
            if (!curLevelPlaying.isUnlock)
            {
                LevelData levelInfo = GlobalClass.LevelInformation;
                if (levelInfo.roboUnlock!=0)
                {
                    Console.WriteLine("info"+levelInfo.roboUnlock);
                    infoRobo = new Background(Game.Content, "Maps/info" + levelInfo.roboUnlock, 0.00004f);
                    gotIt = new Button(Game.Content.Load<Texture2D>("Maps/gotItButton"),
                        Game.Content.Load<Texture2D>("Maps/gotItButton_Hover"),
                        Game.Content.Load<Texture2D>("Maps/gotItButton"), 0.6f, 0.0f,
                        new Vector2(GlobalClass.ScreenWidth / 2 - 80, 645));
                    gotIt.layer = 0.00001f;
                    gotIt.Clicked += new EventHandler(gotIt_Clicked);
                    nextLevelButton.Disable();
                    return;
                }
            }

            MediaPlayer.Stop();
            MediaPlayer.Play(Game.Content.Load<Song>("sounds\\main_sound"));
            if (GlobalClass.curLevelPlaying == 5 && GlobalClass.curMapPlaying == 1)
            {
                GlobalClass.curLevelPlaying = 1;
                GlobalClass.curMapPlaying = 2;
            }
            Game.StartMap();
        }

        public void gotIt_Clicked(object o, EventArgs e)
        {
            MediaPlayer.Stop();
            MediaPlayer.Play(Game.Content.Load<Song>("sounds\\main_sound"));
            if (GlobalClass.curLevelPlaying == 5 && GlobalClass.curMapPlaying == 1)
            {
                GlobalClass.curLevelPlaying = 1;
                GlobalClass.curMapPlaying = 2;
            }
            Game.StartMap();
        }

        public void calculateRating()
        {
            if (GlobalClass.timePlaying <= GlobalClass.LevelInformation.threeStar)
            {
                ratingAfterPlay = 3;
            }
            else if (GlobalClass.timePlaying <= GlobalClass.LevelInformation.twoStar)
            {
                ratingAfterPlay = 2;
            }
            else ratingAfterPlay = 1;

            Level status = new Level();

            if (ratingAfterPlay > ratingInFile)
                status = new Level(true, ratingAfterPlay);
            else 
                status = new Level(true, ratingInFile);

            data.Save(status, PathFileLevelConfig);
        }
    }
}
