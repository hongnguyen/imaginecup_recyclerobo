﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using Microsoft.Xna.Framework.Media;

namespace RecycleRobo.Scenes
{
    public class MapScene
    {
        GameManager game;
        int numOfLevel;
        int numOfUnlocked;
        List<Texture2D> levelTexture;
        List<Rectangle> levelBox;
        Texture2D[] rattingTexture;
        List<Vector2> BoxStartingPoint, BoxComingSoon, RobotMovePoint;
        List<int> stars;

        NumberOne robot;
        List<int> path;
        int[,] connected;

        MouseState curMouseState;
        MouseState preMouseState;

        int robotOriginalTarget;
        int robotPosition;
        int count;

        private Texture2D[] background;

        static float scale = 0.7f;

        private Texture2D levelComingSoon;
        //Texture2D fadeTexture, levelComingSoon, miniGameWarning;
        //Texture2D quizTexture;

        Data loadData;
        Button mapButton, introButton, factoryButton, challengeButton;
        Vector2[] buttonPos = new Vector2[4];
        public MapScene(GameManager game, int numOfLevel)
        {
            GlobalClass.isInMap = true;
            //isBackClicked = false;
            loadData = new Data();

            numOfUnlocked = 0;
            rattingTexture = new Texture2D[3];
            path = new List<int>();
            this.game = game;

            levelTexture = new List<Texture2D>();
            levelBox = new List<Rectangle>();
            BoxStartingPoint = new List<Vector2>();
            RobotMovePoint = new List<Vector2>();
            BoxComingSoon = new List<Vector2>();

            this.numOfLevel = numOfLevel;
            stars = new List<int>();
            for (int i = 0; i < numOfLevel; i++)
            {
                Level nlevel = loadData.Load<Level>("Map\\Map_" + GlobalClass.curMapPlaying + "\\Level" + Convert.ToString(i + 1) + ".xml");
                if (nlevel.isUnlock)
                {
                    stars.Add(nlevel.rating);
                    this.numOfUnlocked += 1;
                }
                else 
                {
                    stars.Add(0);
                }
            }

            MapChoose(GlobalClass.curMapPlaying);

            robotOriginalTarget = 100;
            count = -1;

            buttonPos[0] = new Vector2(Asset.backButton.Width*0.625f/2 - (2*Asset.backButton.Width*0.625f)/3, Asset.backButton.Height * 0.625f / 2 + 20);
            buttonPos[1] = new Vector2(buttonPos[0].X, Asset.backButton.Height * 0.625f / 2 + 45 + buttonPos[0].Y);
            buttonPos[2] = new Vector2(buttonPos[0].X, Asset.backButton.Height * 0.625f / 2 + 45 + buttonPos[1].Y);
            buttonPos[3] = new Vector2(buttonPos[0].X, Asset.backButton.Height * 0.625f / 2 + 45 + buttonPos[2].Y);

            mapButton = new Button(Asset.backButton, Asset.backButton,Asset.backButton, 0.625f, 0.0f, 
                                    buttonPos[0]);
            if (GlobalClass.curMapPlaying == 1)
            {
                introButton = new Button(Asset.introButton1, Asset.introButton1, Asset.introButton1, 0.625f, 0f,
                buttonPos[1]);

            }
            else if (GlobalClass.curMapPlaying == 2)
            {
                introButton = new Button(Asset.introButton2, Asset.introButton2, Asset.introButton1, 0.625f, 0f,
                                buttonPos[1]);
            }
            factoryButton = new Button(Asset.shopMapButton, Asset.shopMapButton, Asset.shopMapButton, 0.625f, 0f,
                       buttonPos[2]);


            challengeButton = new Button(Asset.achieveButton,Asset.achieveButton,Asset.achieveButton, 0.625f, 0f,
                               buttonPos[3]);

            

            introButton.Clicked += new EventHandler(introButton_Clicked);
            factoryButton.Clicked += new EventHandler(factoryButton_Clicked);
            mapButton.Clicked += new EventHandler(mapButton_Clicked);
            challengeButton.Clicked += new EventHandler(challengeButton_Clicked);

            introButton.Hover += new EventHandler(introButton_Hover);
            factoryButton.Hover += new EventHandler(factoryButton_Hover);
            mapButton.Hover += new EventHandler(mapButton_Hover);
            challengeButton.Hover += new EventHandler(challengeButton_Hover);

            introButton.NotHover += new EventHandler(introButton_NotHover);
            factoryButton.NotHover += new EventHandler(factoryButton_NotHover);
            mapButton.NotHover += new EventHandler(mapButton_NotHover);
            challengeButton.NotHover += new EventHandler(challengeButton_NotHover);

            MediaPlayer.Stop();
            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;


            if (GlobalClass.curMapPlaying == 1)
            {
                MediaPlayer.Play(Asset.bgSound2);
            }
            else
            {
                MediaPlayer.Play(Asset.bgSound3);
            }
        }

        private void factoryButton_Hover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X < Asset.backButton.Width*0.625f/2 - 8)
            {
                t.position.X += 4;
            }
        }

        private void challengeButton_Hover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X < Asset.backButton.Width * 0.625f / 2 - 8)
            {
                t.position.X += 4;
            }
        }
        private void introButton_Hover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X < Asset.backButton.Width * 0.625f / 2 - 8)
            {
                t.position.X += 4;
            }
        }

        public void mapButton_Hover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X < Asset.backButton.Width * 0.625f / 2 - 8)
            {
                t.position.X += 4;
            }
        }
        private void factoryButton_NotHover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X > buttonPos[0].X)
            {
                t.position.X -= 4;
            }
        }

        private void challengeButton_NotHover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X > buttonPos[0].X)
            {
                t.position.X -= 4;
            }
        }
        private void introButton_NotHover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X > buttonPos[0].X)
            {
                t.position.X -= 4;
            }
        }

        public void mapButton_NotHover(object o, EventArgs e)
        {
            Button t = (Button)o;
            if (t.position.X > buttonPos[0].X)
            {
                t.position.X -= 4;
            }

        }





        private void factoryButton_Clicked(object o, EventArgs e)
        {
            game.RecycleStart();
        }

        private void challengeButton_Clicked(object o, EventArgs e)
        {
            game.StartChallenge();
        }
        private void introButton_Clicked(object o, EventArgs e)
        {
            game.StartIntroScene();
        }

        public void mapButton_Clicked(object o, EventArgs e)
        {
            GlobalClass.curLevelPlaying = 1;
            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            MediaPlayer.Play(Asset.bgSound1);
            game.WorldMap();

        }



        public void drawStar(int number, Vector2 pos, SpriteBatch spritebatch) 
        {
            if (number == 0 || number == -1) return;
            else if(number == 1)
                spritebatch.Draw(Asset.star, pos - new Vector2(Asset.star.Width * scale * 1 / 2, 0),
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
            else if (number == 2)
            {
                spritebatch.Draw(Asset.star, pos - new Vector2(Asset.star.Width * scale, 0),
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
                spritebatch.Draw(Asset.star, pos,
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
            }
            else 
            {
                spritebatch.Draw(Asset.star, pos - new Vector2(Asset.star.Width * scale * 1 / 2, 0),
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
                spritebatch.Draw(Asset.star, pos - new Vector2(Asset.star.Width * scale * 3/2, 0),
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
                spritebatch.Draw(Asset.star, pos + new Vector2(Asset.star.Width * scale * 1/2, 0),
                    null, Color.White, 0.0f, Vector2.Zero, 0.925f, SpriteEffects.None, 0.9f);
            }

        }

        public void MapChoose(int mapID)
        {
            if (mapID == 1)
            {
                numOfLevel = 7;

                connected = new int[numOfLevel, numOfLevel];
                for (int i = 0; i < numOfLevel; i++)
                {
                    for (int j = 0; j < numOfLevel; j++)
                    {
                        connected[i, j] = 0;
                    }
                }
                connected[0, 1] = 1;
                connected[1, 2] = 1;
                connected[2, 3] = 1;
                connected[3, 4] = 1;
                connected[4, 5] = 1;
                connected[5, 6] = 1;

                connected[1, 0] = 1;
                connected[2, 1] = 1;
                connected[3, 2] = 1;
                connected[4, 3] = 1;
                connected[5, 4] = 1;
                connected[6, 5] = 1;

                background = new Texture2D[numOfLevel+1];
                background[0] = Asset.backgroundMap2_0;
                background[1] = Asset.backgroundMap2_1;
                background[2] = Asset.backgroundMap2_2;
                background[3] = Asset.backgroundMap2_3;
                background[4] = Asset.backgroundMap2_4;
                background[5] = Asset.backgroundMap2_5;
                background[6] = Asset.backgroundMap2_6;
                background[7] = Asset.backgroundMap2_7;

                RobotMovePoint.Add(new Vector2(443, 234));
                RobotMovePoint.Add(new Vector2(675, 262));
                RobotMovePoint.Add(new Vector2(891, 341));
                RobotMovePoint.Add(new Vector2(812, 568));
                RobotMovePoint.Add(new Vector2(639, 471));
                RobotMovePoint.Add(new Vector2(412, 496));
                RobotMovePoint.Add(new Vector2(269, 573));

                levelBox.Add(new Rectangle(338, 130, 200, 100));
                levelBox.Add(new Rectangle(600, 159, 200, 100));
                levelBox.Add(new Rectangle(885, 210, 200, 110));
                levelBox.Add(new Rectangle(876, 440, 200, 130));
                levelBox.Add(new Rectangle(573, 322, 200, 130));
                levelBox.Add(new Rectangle(278, 374, 200, 130));
                levelBox.Add(new Rectangle(48, 482, 200, 140));

                robot = new NumberOne(game.Content, new Vector2(-100,-100), null, game);

                for (int i = 0; i < numOfLevel; i++) 
                {
                    RobotMovePoint[i] = RobotMovePoint[i] - 
                        new Vector2(robot.CurAnimation.FrameWidth / 2 * robot.sprite.Scale - 20, robot.CurAnimation.FrameHeight * robot.sprite.Scale - 25);
                }
                robot = new NumberOne(game.Content, RobotMovePoint[GlobalClass.curLevelPlaying - 1], null, game);
                if (GlobalClass.curMapPlaying == 1)
                {
                    robotPosition = GlobalClass.curLevelPlaying - 1;
                }
                else 
                {
                    robotPosition = 0;
                }
            }
            if (mapID == 2)
            {
                numOfLevel = 8;

                connected = new int[numOfLevel, numOfLevel];
                for (int i = 0; i < numOfLevel; i++)
                {
                    for (int j = 0; j < numOfLevel; j++)
                    {
                        connected[i, j] = 0;
                    }
                }
                connected[0, 1] = 1;
                connected[1, 2] = 1;
                connected[2, 3] = 1;
                connected[3, 4] = 1;
                connected[4, 5] = 1;
                connected[5, 6] = 1;
                connected[6, 7] = 1;

                connected[1, 0] = 1;
                connected[2, 1] = 1;
                connected[3, 2] = 1;
                connected[4, 3] = 1;
                connected[5, 4] = 1;
                connected[6, 5] = 1;
                connected[7, 6] = 1;

                background = new Texture2D[numOfLevel + 1];
                background[0] = Asset.backgroundMap1_0;
                background[1] = Asset.backgroundMap1_1;
                background[2] = Asset.backgroundMap1_2;
                background[3] = Asset.backgroundMap1_3;
                background[4] = Asset.backgroundMap1_4;
                background[5] = Asset.backgroundMap1_5;
                background[6] = Asset.backgroundMap1_6;
                background[7] = Asset.backgroundMap1_7;
                background[8] = Asset.backgroundMap1_8;

                RobotMovePoint.Add(new Vector2(270, 290));
                RobotMovePoint.Add(new Vector2(164, 426));
                RobotMovePoint.Add(new Vector2(169, 577));
                RobotMovePoint.Add(new Vector2(525, 496));
                RobotMovePoint.Add(new Vector2(526, 387));
                RobotMovePoint.Add(new Vector2(607, 203));
                RobotMovePoint.Add(new Vector2(799, 179));
                RobotMovePoint.Add(new Vector2(988, 153));

                levelBox.Add(new Rectangle(195, 180, 180, 120));
                levelBox.Add(new Rectangle(0, 344, 160, 110));
                levelBox.Add(new Rectangle(198, 540, 180, 120));
                levelBox.Add(new Rectangle(515, 480, 190, 120));
                levelBox.Add(new Rectangle(410, 326, 170, 100));
                levelBox.Add(new Rectangle(485, 141, 180, 70));
                levelBox.Add(new Rectangle(745, 88, 180, 90));
                levelBox.Add(new Rectangle(967, 40, 180, 105));

                robot = new NumberOne(game.Content, new Vector2(-100, -100), null, game);
                for (int i = 0; i < numOfLevel; i++)
                {
                    RobotMovePoint[i] = RobotMovePoint[i] -
                        new Vector2(robot.CurAnimation.FrameWidth / 2 * robot.sprite.Scale - 20, robot.CurAnimation.FrameHeight * robot.sprite.Scale - 25);
                }
                robot = new NumberOne(game.Content, RobotMovePoint[GlobalClass.curLevelPlaying - 1], null, game);
                if (GlobalClass.curMapPlaying == 2)
                {
                    robotPosition = GlobalClass.curLevelPlaying - 1;
                }
                else
                {
                    robotPosition = 0;
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            curMouseState = Mouse.GetState();
            Point mousePos = new Point(MouseHelper.MousePosition(curMouseState).X, MouseHelper.MousePosition(curMouseState).Y);
            //Console.WriteLine(Mouse.GetState().X + " " + Mouse.GetState().Y);
            mapButton.Update(gameTime);
            if(robot.isWalking)
            for (int i = 0; i < numOfLevel; i++)
            {
                if ((robot.Position - RobotMovePoint[i]).Length() < 3)
                {
                    robotPosition = i;
                    robot.Position = RobotMovePoint[i];
                    if (i == robotOriginalTarget) 
                    {
                        GlobalClass.curLevelPlaying = i + 1;
                        game.StartBuildTeam();
                    }
                }
            }

            factoryButton.Update(gameTime);
            challengeButton.Update(gameTime);
            introButton.Update(gameTime);
            if (curMouseState.LeftButton == ButtonState.Released && preMouseState.LeftButton == ButtonState.Pressed
                && !robot.isWalking && mapButton.position.X <= 60)
            {

                for (int i = 0; i < numOfLevel; i++)
                {
                    
                    if (i >= numOfUnlocked) break;
                    if (levelBox[i].Contains(mousePos))
                    {
                        GlobalClass.curLevelPlaying = i + 1;
                        robotOriginalTarget = i;
                        path.Clear();
                        findPath(robotPosition, robotOriginalTarget);
                        count = path.Count() - 1;
                        if (robotPosition == robotOriginalTarget)
                            game.StartBuildTeam();
                    }
                }
            }
            move();
            if (robot.TargetPos.X != -1 && (robot.TargetPos - robot.Position).Length() > 3)
            {
                Vector2 direction = robot.TargetPos - robot.Position;
                direction.Normalize();
                if (robot.isWalking && Math.Sign(robot.initialDirection.X) == Math.Sign(direction.X) && Math.Sign(robot.initialDirection.Y) ==
                Math.Sign(direction.Y) && robot.isControl)
                {

                    if (robot.TargetPos.X < robot.Position.X)
                    {
                        robot.sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                    }
                    else
                    {
                        robot.sprite.SpriteEffect = SpriteEffects.None;
                    }

                    robot.Position.X += (float)(direction.X * robot.speed * gameTime.ElapsedGameTime.TotalSeconds);
                    robot.Position.Y += (float)(direction.Y * robot.speed * gameTime.ElapsedGameTime.TotalSeconds);
                }
                else
                {
                    robot.TargetPos = robot.Position;
                }
            }
            else if (robot.isWalking)
            {
                robot.setIdleAnimation();
                robot.isWalking = false;
            }
            preMouseState = curMouseState;
        }

        public void move()
        {
            if (path == null || path.Count == 0) return;
            if (count < 0)
            {
                count = 0;
                return;
            }
            if (count >= path.Count)
            {
                count = path.Count - 1;
                return;
            }
            if ((robot.Position - RobotMovePoint[robotOriginalTarget]).Length() < 5)
            {
                robot.TargetPos = RobotMovePoint[path[count]];
                robot.initialDirection = RobotMovePoint[path[count]] - robot.Position;
                robot.isWalking = true;
                robot.setWalkingAnimation();
                GlobalClass.curLevelPlaying = path[count] + 1;
                path.Clear();
                return;
            }
            robot.TargetPos = RobotMovePoint[path[count]];
            robot.initialDirection = RobotMovePoint[path[count]] - robot.Position;
            robot.isWalking = true;
            robot.setWalkingAnimation();
            if (robot.isWalking && (RobotMovePoint[path[count]] - robot.Position).Length() < 5)
            {
                count--;
            }
        }

        public void findPath(int pos, int target)
        {
            int[] visited = new int[numOfLevel];
            int[] prev = new int[numOfLevel];
            for (int i = 0; i < numOfLevel; i++)
            {
                visited[i] = 0;
                prev[i] = 0;
            }
            Queue<int> test = new Queue<int>();
            test.Enqueue(pos);
            while (test.Count() != 0)
            {
                int tmp = test.Dequeue();
                if (tmp == target)
                {
                    int tmp2 = target;
                    while (tmp2 != pos)
                    {
                        path.Add(tmp2);
                        tmp2 = prev[tmp2];
                    }
                    //path.Add(pos);
                    return;
                }
                for (int i = 0; i < numOfLevel; i++)
                {
                    if (connected[i, tmp] == 1 && visited[i] == 0)
                    {
                        test.Enqueue(i);
                        visited[i] = 1;
                        prev[i] = tmp;
                    }
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spritebatch)
        {
            mapButton.Draw(spritebatch);
            introButton.Draw(spritebatch);
            factoryButton.Draw(spritebatch);
            challengeButton.Draw(spritebatch);
            //spritebatch.Draw(background[numOfUnlocked], Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.98f);
            spritebatch.Draw(background[numOfUnlocked], Vector2.Zero, new Rectangle(0, 0, 1920, 1080), 
                Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.98f);
            robot.Draw(gameTime, spritebatch);
            for (int i = 0; i < levelBox.Count; i++) 
            {
                Vector2 pos = new Vector2(levelBox[i].X + levelBox[i].Width/2, levelBox[i].Y + levelBox[i].Height - 20);
                drawStar(stars[i], pos, spritebatch);
            }
        }
    }

}