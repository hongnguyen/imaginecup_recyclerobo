﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
namespace RecycleRobo.Scenes
{
    public class AssistantView 
    {
        private MouseState cur, prev;
        private RecycleScene recycle;
        private static float scale = 0.575f;

        private static Vector2 offset = new Vector2(60, 350);
        private static int width = 172;
        private static int height = 58;

        public List<Texture2D> allAssistant;
        public List<Texture2D> locked;
        public List<Texture2D> bought;
        public List<Vector2> pos;

        private Texture2D resourceBar;
        private CircleObject energyBall = new CircleObject(Asset.energyBall, new Vector2(910, 635), Vector2.Zero, false);

        private SoundEffect effectCorrect;
        private SoundEffect effectFailure;

        public AssistantView(RecycleScene re) 
        {
            this.recycle = re;
            allAssistant = new List<Texture2D>();
            locked = new List<Texture2D>();
            bought = new List<Texture2D>();
            allAssistant.Add(Asset.mini);
            allAssistant.Add(Asset.megadroidBuy);
            allAssistant.Add(Asset.gigadroidBuy);
            allAssistant.Add(Asset.defentantBuy);
            allAssistant.Add(Asset.knightBuy);
            allAssistant.Add(Asset.thunderBuy);
            allAssistant.Add(Asset.thunderbolt2Buy);
            allAssistant.Add(Asset.engine);
            allAssistant.Add(Asset.healer1);
            allAssistant.Add(Asset.healer2);

            locked.Add(Asset.mini);
            locked.Add(Asset.megadroidLock);
            locked.Add(Asset.gigadroidLock);
            locked.Add(Asset.defentantLock);
            locked.Add(Asset.knightLock);
            locked.Add(Asset.thunderLock);
            locked.Add(Asset.thunderbolt2Lock);
            locked.Add(Asset.engine);
            locked.Add(Asset.healer1);
            locked.Add(Asset.healer2);

            bought.Add(Asset.mini);
            bought.Add(Asset.megadroidBought);
            bought.Add(Asset.gigadroidBought);
            bought.Add(Asset.defentantBought);
            bought.Add(Asset.knightBought);
            bought.Add(Asset.thunderBought);
            bought.Add(Asset.thunderbolt2Bought);
            bought.Add(Asset.engine);
            bought.Add(Asset.healer1);
            bought.Add(Asset.healer2);

            resourceBar = Asset.resourceBar;

            effectCorrect = Asset.correctSound;
            effectFailure = Asset.wrongSound;
            energyBall.layer = 0.48f;
            energyBall.scale = 0.46f;

            for (int i = 1; i < recycle.assistantStatus.Length; i++) 
            {
                if (recycle.assistantStatus[i-1].isBought) 
                {
                    allAssistant[i] = bought[i];
                }
                else if (!recycle.assistantStatus[i-1].isUnlocked) 
                {
                    allAssistant[i] = locked[i];
                }
            }
            reset();
        }

        public void reset() 
        {
            Vector2 begin = new Vector2(60, 100);
            if (pos == null)
                pos = new List<Vector2>();
            else
                pos.Clear();
            pos.Add(begin);
            for (int i = 1; i < allAssistant.Count; i++)
            {
                // 505 la width cua anh goc khi chua bi DxT compression
                pos.Add(new Vector2(pos[i - 1].X + 505 * scale + 40, pos[0].Y));
                //pos.Add(new Vector2(pos[i - 1].X + allAssistant[i - 1].Width * scale + 40, pos[0].Y));
            }
        }

        public void Update(GameTime gameTime)
        {
            cur = Mouse.GetState();
            Point mousePoint = MouseHelper.MousePosition(cur);
            Point mousePoint2 = MouseHelper.MousePosition(prev);
            if (cur.LeftButton == ButtonState.Pressed && prev.LeftButton == ButtonState.Released)
            {
                for (int i = 1; i < allAssistant.Count; i++)
                {
                    Rectangle rec = new Rectangle((int)pos[i].X + (int)offset.X, (int)pos[i].Y + (int)offset.Y, width, height);
                    //if (rec.Contains(mousePoint))
                    //{
                    //    Console.WriteLine(recycle.assistantStatus[i - 1].isUnlocked);
                    //    Console.WriteLine(recycle.assistantStatus[i - 1].isBought);
                    //}
                    if (rec.Contains(mousePoint) && recycle.assistantStatus[i - 1].isBought == false
                        && recycle.assistantStatus[i - 1].isUnlocked == true && rec.Contains(mousePoint2))
                    {
                        if (recycle.resoure >= recycle.assistantStatus[i-1].cost)
                        {
                            effectCorrect.Play();
                            recycle.resoure -= recycle.assistantStatus[i - 1].cost;
                            allAssistant[i] = bought[i];
                            recycle.assistantStatus[i-1].isBought = true;
                            this.recycle.save();
                        }
                        else
                        {
                            effectFailure.Play();
                        }
                        break;
                    }
                }
            }
            if (prev.LeftButton == ButtonState.Pressed && cur.LeftButton == ButtonState.Pressed) 
            {
                Vector2 Target = new Vector2((float)MouseHelper.MousePosition(cur).X, (float)MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2((float)MouseHelper.MousePosition(prev).X, (float)MouseHelper.MousePosition(prev).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 40)
                {
                    direction.Normalize();
                    direction = direction * 40;
                }
                for (int i = 0; i < pos.Count(); i++)
                {
                    (pos[i]) = new Vector2(pos[i].X + direction.X, pos[i].Y);
                }
                if (pos[0].X > 20 && direction.X > 0)
                {
                    for (int i = 0; i < pos.Count(); i++)
                    {
                        (pos[i]) = new Vector2(pos[i].X - direction.X, pos[i].Y);
                    }
                }
                //else if (pos[allAssistant.Count - 1].X + allAssistant[allAssistant.Count - 1].Width * scale < GlobalClass.ScreenWidth - 20 && direction.X < 0)
                else if (pos[allAssistant.Count - 1].X + 505 * scale < GlobalClass.ScreenWidth - 20 && direction.X < 0)
                {
                    for (int i = 0; i < pos.Count(); i++)
                    {
                        (pos[i]) = new Vector2(pos[i].X - direction.X, pos[i].Y);
                    }
                }
            }
            
            energyBall.update(gameTime);

            prev = cur;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < allAssistant.Count; i++) 
            {
                spriteBatch.Draw(allAssistant[i],pos[i],new Rectangle (0,0,505,756),Color.White,0,Vector2.Zero,scale,SpriteEffects.None,0.5f);
            }
            Vector2 resourcePlace = new Vector2(GlobalClass.ScreenWidth - resourceBar.Width * scale,
                GlobalClass.ScreenHeight - resourceBar.Height * scale);
            spriteBatch.Draw(resourceBar, resourcePlace, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.5f);
            energyBall.draw(spriteBatch);
            spriteBatch.DrawString(Asset.font, recycle.resoure.ToString(), resourcePlace + new Vector2(200,30),Color.Green);
        }

    }
}