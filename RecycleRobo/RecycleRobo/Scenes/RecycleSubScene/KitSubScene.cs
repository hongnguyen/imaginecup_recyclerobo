﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo.Scenes
{
    class KitSubScene
    {
        public GameManager game;

        private Vector2[] Pos;
        private Texture2D[] IconBackground;
        private Rectangle[] BoundingBox;
        private Button uUpgrade;

        public static List<Texture2D> allKit;
        public List<int> available;
        public SpriteFont font;

        public static float scale = 1.0f;
        
        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };
        private Rectangle clippingRec;

        private MouseState cur;
        private MouseState pre;

        private SoundEffect effectCorrect;
        private SoundEffect effectFailure;
        private string[] kitName;

        private int TextBoxID;
        private bool inside;
        private bool disable;
        private Rectangle borderRectangle;
        private RecycleScene re;

        public KitSubScene(List<int> available, GameManager game, Rectangle clippingRec, RecycleScene re){
            kitName = new string[] { "Plastic recycler", "Paper recycler", "Glass recycler", "Compost recycler", "Metal recycler", "Metal plastic recycler", "Multi recycler" };
            this.re = re;
            this.available = available;
            this.game = game;
            this.clippingRec = clippingRec;
            borderRectangle = new Rectangle((int)clippingRec.X, (int)GlobalClass.ScreenHeight - 300,
                (int)((GlobalClass.ScreenWidth - clippingRec.X) - 10), 250 );
            loadKit();
            
            font = game.Content.Load<SpriteFont>("newFont");
            TextBoxID = -1;
            createBuyButton(borderRectangle);
            inside = false;
            disable = true;
        }

        public void loadKit() 
        {
            allKit = new List<Texture2D>();

            allKit.Add(Asset.plastic);
            allKit.Add(Asset.paper);
            allKit.Add(Asset.glass);
            allKit.Add(Asset.compost);
            allKit.Add(Asset.metal);
            allKit.Add(Asset.metalplastic);
            allKit.Add(Asset.mix);

            effectCorrect = Asset.correctSound;
            effectFailure = Asset.wrongSound;

            Pos = new Vector2[allKit.Count()];
            Pos[0] = new Vector2(clippingRec.X, clippingRec.Y);
            IconBackground = new Texture2D[allKit.Count()];
            BoundingBox = new Rectangle[allKit.Count];
            for (int i = 0; i < allKit.Count; i++)
            {
                IconBackground[i] = Asset.ownedBackground;
                if (i >= 1) Pos[i] = new Vector2(Pos[i - 1].X + IconBackground[i].Width * scale + 20, clippingRec.Y);
                BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clippingRec.Y,
                    (int)(IconBackground[i].Width * scale),
                    (int)(IconBackground[i].Height * scale));
            }
        }

        /*public static string WrapText(SpriteFont spriteFont, string text, float maxLineWidth)
        {
            string[] words = text.Split(' ');

            StringBuilder sb = new StringBuilder();

            float lineWidth = 0f;

            float spaceWidth = spriteFont.MeasureString(" ").X;

            foreach (string word in words)
            {
                Vector2 size = spriteFont.MeasureString(word);

                if (lineWidth + size.X < maxLineWidth)
                {
                    sb.Append(word + " ");
                    lineWidth += size.X + spaceWidth;
                }
                else
                {
                    sb.Append("\n" + word + " ");
                    lineWidth = size.X + spaceWidth;
                }
            }

            return sb.ToString();
        }*/

        public String getKitinfo(int i) 
        {
            KitData kitData = game.Content.Load<KitData>("Data/Kit/KitInfo");
            String result = "";
            List<KitInfo> kitInfo = kitData.Kits;
            
            result += (kitInfo[i].cans_percent * 100 + "% metal, ");
            result += (kitInfo[i].food_percent * 100 + "% compost, ");
            result += (kitInfo[i].glass_percent * 100 + "% glass, ");
            result += (kitInfo[i].paper_percent * 100 + "% paper, ");
            result += (kitInfo[i].plastics_percent * 100 + "% plastic, ");
            result += "are recycled.";

            return result;
        }

        public String getKitReq(int ID) 
        {
            String[] typesOfRes = { "plastic", "paper", "glass", "compost", "metal" };
            String result = "";
            result += "Requirement:";
            for (int i = 0; i < typesOfRes.Length ; i++) 
            {
                if(i < typesOfRes.Length-1) result += (int)(RecycleScene.kitPriceBase)[ID][i] + " " + typesOfRes[i] + ",";
                else result += (int)(RecycleScene.kitPriceBase)[ID][i] + " " + typesOfRes[i];
            }
            return result;
        }

        public List<String> getText(int ID) 
        {
            List<String> result = new List<string>();
            result.Add(kitName[ID]);
            result.Add(getKitinfo(ID));
            if (ID != 0)
                result.Add(getKitReq(ID));
            else
                result.Add("Requirement: NONE");
            if (!available.Contains(ID)) 
            {
                result.Add("\nYou have bought this Item.");
            }
            return result;
        }

        public void Update(GameTime gametime) 
        {
            cur = Mouse.GetState();
            if (available.Contains(TextBoxID)) disable = false;
            else disable = true;
            if (!clippingRec.Contains(new Point((int)MouseHelper.MousePosition(cur).X, (int)MouseHelper.MousePosition(cur).Y)))
            {
                inside = false;
                return;
            }
            else 
            {
                inside = true;
            }

            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Released)
            {
                for (int i = 0; i < allKit.Count; i++)
                {
                    if ((BoundingBox[i]).Contains(new Point((int)MouseHelper.MousePosition(cur).X, (int)MouseHelper.MousePosition(cur).Y)))
                    {
                        TextBoxID = i;
                        IconBackground[i] = Asset.selectedState;
                    }
                    else
                    {
                        IconBackground[i] = Asset.ownedBackground;
                    }
                }
            }

            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Pressed)
            {
                Vector2 Target = new Vector2((float)MouseHelper.MousePosition(cur).X, (float)MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2((float)MouseHelper.MousePosition(pre).X, (float)MouseHelper.MousePosition(pre).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 40)
                {
                    direction.Normalize();
                    direction = direction * 40;
                }
                if (Pos[0].X > clippingRec.X + 10 && direction.X > 0) return;
                if (Pos[Pos.Count() - 1].X + IconBackground[0].Width < GlobalClass.ScreenWidth
                    && direction.X < 0) return;

                for (int i = 0; i < allKit.Count; i++)
                {
                    Pos[i] = new Vector2((Pos[i]).X + (direction.X), (Pos[i]).Y);
                    BoundingBox[i] = (new Rectangle((int)((Pos[i]).X), (int)((Pos[i]).Y),
                    (IconBackground[i]).Width, (IconBackground[i]).Height));
                }
            }
            pre = cur;
            uUpgrade.Update(gametime);
        }

        public bool CheckResRequirement(int ID)
        {
            for (int i = 0; i < (re.resource).Count(); i++)
            {
                if ((re.resource[i] < (RecycleScene.kitPriceBase)[ID][i]))
                {
                    return false;
                }
            }
            return true;
        }

        public void DrawTextBox(SpriteBatch spriteBatch, int ID)
        {
            if (!inside) return;
            if (TextBoxID == -1) return;
            Texture2D boxTexture = Asset.infoBackground;
            spriteBatch.Draw(boxTexture,borderRectangle, null,Color.White,0.0f,new Vector2(0,0),SpriteEffects.None,0.98f);
            List<String> textToWrite = getText(ID);
            Color[] colorList = {Color.White,Color.Purple,Color.Green,Color.Blue};
            if (!CheckResRequirement(ID)) colorList[2] = Color.Red;
            for (int i = 0; i < textToWrite.Count; i++)
            {
                String text = textToWrite[i];
                Vector2 size = font.MeasureString(text);
                spriteBatch.DrawString(font, textToWrite[i],
                    new Vector2(borderRectangle.X + borderRectangle.Width / 2 - size.X / 2, borderRectangle.Y + 10 + i * 50)
                    , colorList[i]);
            }
            if (!disable) uUpgrade.Draw(spriteBatch);
        }

        public void createBuyButton(Rectangle textRec)
        {
            Vector2 center = new Vector2(textRec.X + textRec.Width / 2, textRec.Y + textRec.Height);
            uUpgrade = new Button(Asset.buildButton, Asset.buildButtonHover, Asset.buildButton, 0.5f, 0.0f, center - new Vector2(0, Asset.buildButton.Height / 4 + 10));
            uUpgrade.Clicked += new EventHandler(uUpgrade_Clicked);
        }

        private void uUpgrade_Clicked(object sender, EventArgs e)
        {
            if (disable) 
            {
                return; 
            }
            if (!CheckResRequirement(TextBoxID))
            {
                effectFailure.Play();
                return;
            }
            //Console.WriteLine(TextBoxID);
            available.Remove(TextBoxID);
            for (int i = 0; i < re.resource.Count(); i++)
                re.resource[i] -= (RecycleScene.kitPriceBase)[TextBoxID][i];
            re.save();
            disable = true;
            effectCorrect.Play();
        }

        public void DrawKit(GameTime gametime, SpriteBatch spriteBatch) 
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                      null, null, _rasterizerState,null, Resolution.getTransformationMatrix());
            spriteBatch.GraphicsDevice.ScissorRectangle = clippingRec;
            for (int i = 0; i < allKit.Count; i++) 
            {
                int pos1 = (int)(Pos[i]).X + (IconBackground[i]).Width / 2 - (allKit[i]).Width / 2;

                int pos2 = (int)(Pos[i]).Y + (IconBackground[i]).Height / 2 - (allKit[i]).Height / 2;

                int width = (allKit[i]).Width;

                int height = (allKit[i]).Height;
                spriteBatch.Draw((IconBackground[i])
                    , (Rectangle)BoundingBox[i]
                    , null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                spriteBatch.Draw((allKit[i]), new Rectangle(pos1, pos2, width, height),
                    null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
            }
            DrawTextBox(spriteBatch,TextBoxID);
            spriteBatch.End();
        }
    }
}
