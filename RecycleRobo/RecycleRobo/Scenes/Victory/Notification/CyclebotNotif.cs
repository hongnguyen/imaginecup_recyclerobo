﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.GamerServices;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;


    namespace RecycleRobo.Scenes.Victory
    {
        /// <summary>
        /// This is a game component that implements IUpdateable.
        /// </summary>
        public class CyclebotNotif : Notification
        {
            Texture2D newCyclebot;
            Vector2 position;
            Vector2 target = new Vector2(0, 0);
            float speed = 1f;
            float rotate = 0.5f;
            float opacity = 0f;

            Vector2 infoPositon;
            Vector2 targetInfoPosition;
            float scale = 0.625f;
            public CyclebotNotif(Texture2D newCyclebot)
            {
                this.newCyclebot = newCyclebot;
                position = new Vector2(-300, GlobalClass.ScreenHeight);
                infoPositon = new Vector2(GlobalClass.ScreenWidth, 0);
                targetInfoPosition = new Vector2(GlobalClass.ScreenWidth - Asset.inforCycle.Width * scale, 0);
            }
            public override void Update(GameTime gameTime)
            {
                MouseState mouse = Mouse.GetState();
              //  Console.WriteLine(mouse);
                Vector2 direction = target - position;
                Vector2 directionInfo = targetInfoPosition - infoPositon;

                if (rotate > 0f)
                {
                    rotate -= 0.05f;
                }
                if (rotate < 0)
                {
                    rotate = 0;
                }

                direction.Normalize();
                directionInfo.Normalize();

                if (position.Y > target.Y)
                {
                    position += direction * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
                }
                if (infoPositon.X > targetInfoPosition.X)
                {
                    infoPositon += directionInfo * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
                }
                opacity += 0.08f;
                base.Update(gameTime);
            }
            public override void Draw(GameTime gameTime,SpriteBatch spriteBatch)
            {
                spriteBatch.Draw(newCyclebot, position, null, Color.White, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
                spriteBatch.Draw(Asset.inforCycle, infoPositon, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
                spriteBatch.Draw(Asset.typeCycle, new Vector2(0, 0), null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
                base.Draw(gameTime, spriteBatch);
            }
        }
    }

}
