using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{

    public class Notification
    {
        CircleObject circle;
        public Notification()
        {
            circle = new CircleObject(Asset.circle, new Vector2(GlobalClass.ScreenWidth - 100, GlobalClass.ScreenHeight - 78), new Vector2(Asset.circle.Width / 2, Asset.circle.Height / 2), true);
            circle.layer = 0.4f;
        }

        public void Initialize()
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            //circle.update(gameTime);
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            //circle.draw(spriteBatch);
            spriteBatch.Draw(Asset.notifBackground, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.0033f);
        }
    }
}
