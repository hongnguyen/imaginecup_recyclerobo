﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class FunFactNotif
    {
        Texture2D texture;
        public float scale = 0.625f;
        public float layer = 0.004f;
        public float speed = 0.5f;
        Vector2 pos = new Vector2(100,-10);
        Vector2 target = new Vector2(100, 15);
        Random rd = new Random();
        public FunFactNotif(GameManager game)
        {
            int fun = rd.Next(1, 13);
            texture = game.Content.Load<Texture2D>("Victory/FunFact/" + fun);
            pos.Y = -774 * scale;
            pos.X = GlobalClass.ScreenWidth - 379 * scale - 20;
            target.X = GlobalClass.ScreenWidth - 379 * scale - 20;
        }
        public void Update(GameTime gameTime)
        {
            Vector2 direction = target - pos;
            direction.Normalize();
            if (Math.Abs(pos.Y - target.Y) > 5)
            {
                pos += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * speed;

            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, pos, new Rectangle(0,0,379,774), Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
        }
    }
}
