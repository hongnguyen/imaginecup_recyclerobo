﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class AssistantToSelect
    {
        public Assistant assistant;
        public Vector2 originPosition;
        public Vector2 curPosition;
        public float scale = 0.625f;
        public bool isChoose;
        public Texture2D empty;
        public static SpriteFont fontInfo;
        public Vector2 center;

        public AssistantToSelect(Assistant assistant, Vector2 originPos)
        {
            this.assistant = assistant;
            curPosition = originPosition = originPos;
            isChoose = false;
            center = new Vector2(assistant.textureAtBuildTeam.Width / 2, assistant.textureAtBuildTeam.Height / 2);
            empty = Asset.emptyPosAss;
        }

        public AssistantToSelect(AssistantToSelect clone)
        {
            assistant = clone.assistant;
            originPosition = clone.originPosition;
            curPosition = clone.curPosition;
            isChoose = false;
            scale = clone.scale;
            empty = clone.empty;
            center = clone.center;
        }

        public bool isHover(Point mousePoint)
        {
            if (getRectangle().Contains(mousePoint) && !isChoose) return true;
            return false;
        }

        public Rectangle getRectangle()
        {
            return new Rectangle((int)(curPosition.X - assistant.textureAtBuildTeam.Width * scale / 2), 
                                 (int)(curPosition.Y - assistant.textureAtBuildTeam.Height * scale / 2),
                                 (int)(assistant.textureAtBuildTeam.Width * scale), 
                                 (int)(assistant.textureAtBuildTeam.Height * scale)
                                 );
        }

        public void Draw(SpriteBatch spriteBatch, float effect, float layer)
        {
            if (!isChoose)
                spriteBatch.Draw(assistant.textureAtBuildTeam, curPosition + new Vector2(effect,0), null, Color.White,
                                 0f, center, scale, SpriteEffects.None, layer);
            else
                spriteBatch.Draw(empty, curPosition + new Vector2(effect, 0), null, Color.White,
                                 0f, center, scale, SpriteEffects.None, layer);
        }
    }

}
