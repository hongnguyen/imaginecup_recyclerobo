﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RecycleRobo
{
    public class TextDetail
    {
        public const float scale = 0.625f;
        public float layer = 0.9f;
        public Vector2 originPos;
        public Vector2 curPos1, curPos2;
        public Texture2D texture;
        public float speed;
        public string typeMove;
        public float delayMove = 0;
        public TextDetail(Texture2D texture, Vector2 position, string typeMove, float speedMove)
        {
            this.texture = texture;
            this.speed = speedMove;
            curPos1 = position;
            this.typeMove = typeMove;
            if (typeMove == "up")
            {
                originPos = curPos2 = new Vector2(position.X, GlobalClass.ScreenHeight);
            }
            else
            {
                originPos = curPos2 = new Vector2(position.X, -GlobalClass.ScreenHeight);
            }
        }
        public void Draw(SpriteBatch spriteBatch, float effect)
        {
            spriteBatch.Draw(texture, curPos1 + new Vector2(effect, 0), null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
            spriteBatch.Draw(texture, curPos2 + new Vector2(effect, 0), null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
        }
        public void Update(GameTime gametime)
        {
            delayMove += gametime.ElapsedGameTime.Milliseconds;
            if (delayMove > 20)
            {
                delayMove = 0;
                if (typeMove == "up")
                {
                    curPos1.Y -= speed;
                    curPos2.Y -= speed;
                    if (curPos1.Y <= -GlobalClass.ScreenHeight) curPos1 = originPos;
                    if (curPos2.Y <= -GlobalClass.ScreenHeight) curPos2 = originPos;
                }
                else
                {
                    curPos1.Y += speed;
                    curPos2.Y += speed;
                    if (curPos1.Y >= GlobalClass.ScreenHeight) curPos1 = originPos;
                    if (curPos2.Y >= GlobalClass.ScreenHeight) curPos2 = originPos;
                }
            }
        }
    }
}
