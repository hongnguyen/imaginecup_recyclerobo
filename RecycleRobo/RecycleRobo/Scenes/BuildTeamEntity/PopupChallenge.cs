﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RecycleRobo.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class PopupChallenge
    {
        public Texture2D popup;
        public Vector2 positionP;
        public bool isDoneEffect = false;
        int timeAction = 0;
        GameManager game;
        public PopupChallenge(GameManager game)
        {
            this.game = game;
            Challenge challengeCompleted = new Data().Load<Challenge>("Challenge\\ChallengeCompleted.xml");
            if (challengeCompleted.completed != null)
            {
                if (challengeCompleted.completed.Length < 4)
                    GlobalClass.challengeAccept = challengeCompleted.completed[challengeCompleted.completed.Length - 1] + 1;

                else
                    GlobalClass.challengeAccept = 0;
            }
            else if (challengeCompleted.completed == null)
            {
                GlobalClass.challengeAccept = 1;
            }
            //Console.WriteLine(GlobalClass.challengeAccept);
            if (GlobalClass.challengeAccept != 0)
            {
                popup = game.Content.Load<Texture2D>("PopupChallenge\\" + GlobalClass.challengeAccept);
                positionP = new Vector2(GlobalClass.ScreenWidth / 2 - popup.Width / 2, -popup.Height);
            }

        }
        public void Update(GameTime gameTime)
        {
            timeAction += gameTime.ElapsedGameTime.Milliseconds / 16;
            if (positionP.Y != GlobalClass.ScreenHeight / 2 - popup.Height / 2)
            {
                isDoneEffect = false;
                positionP.Y += GlobalClass.ScreenHeight / 40 - popup.Height / 40 + popup.Height / 20;
            }
            else
                isDoneEffect = true;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(popup, positionP, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.1f);
        }
    }
}
