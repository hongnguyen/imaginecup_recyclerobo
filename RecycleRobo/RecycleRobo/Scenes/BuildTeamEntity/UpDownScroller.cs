﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RecycleRobo
{
    public class UpDownScroller
    {
        public enum UpDownState
        {
            NONE,
            UP,
            DOWN
        }

        public UpDownState state = UpDownState.NONE;

        public Texture2D upTexture;
        public Texture2D downTexture;
        public Vector2 positionUp;
        public Vector2 positionDown;
        public float value = 0;
        public float scale = 0.625f;
        public float layer = 0.1f;
        public MouseState curState, prevState;
        public bool isMove = false;

        public UpDownScroller(Texture2D upTexture, Texture2D downTexture, Vector2 position)
        {
            this.upTexture = upTexture;
            this.downTexture = downTexture;
            this.positionUp = position;
            this.positionDown = position + new Vector2(0, GlobalClass.ScreenHeight - downTexture.Height * scale);
        }

        public void Update(GameTime gametime)
        {
            curState = Mouse.GetState();
            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);

            if (isHoverUp(mousePoint))
            {
                isMove = true;
            }
            else if (isHoverDown(mousePoint))
            {
                isMove = true;
            }
            else isMove = false;

            if (curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Released)
            {
                value = 0;
                state = UpDownState.NONE;
            }

            if (curState.LeftButton == ButtonState.Pressed)
            {
                if (isHoverUp(mousePoint))
                {
                    state = UpDownState.UP;
                    value += 0.03f;
                    isMove = true;
                }
                else if (isHoverDown(mousePoint))
                {
                    state = UpDownState.DOWN;
                    value -= 0.03f;
                    isMove = true;
                }
                else
                {
                    state = UpDownState.NONE;
                    isMove = false;
                    value = 0;
                }
            }
            else
            {
                isMove = false;
                state = UpDownState.NONE;
            }

            prevState = curState;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(upTexture, positionUp, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
            spriteBatch.Draw(downTexture, positionDown, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
        }

        public Rectangle getUpRectangle()
        {
            return new Rectangle((int)(positionUp.X), (int)(positionUp.Y), (int)(upTexture.Width * scale), (int)(upTexture.Height * scale) - 40);
        }

        public Rectangle getDownRectangle()
        {
            return new Rectangle((int)(positionDown.X), (int)(positionDown.Y + 40), (int)(downTexture.Width * scale), (int)(downTexture.Height * scale));
        }

        public bool isHoverUp(Point mouse)
        {
            return getUpRectangle().Contains(mouse);
        }

        public bool isHoverDown(Point mouse)
        {
            return getDownRectangle().Contains(mouse);
        }

    }
}
