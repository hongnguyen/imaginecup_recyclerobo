﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class Slot
    {
        public enum SlotState
        {
            NONE,
            ROBO,
            FULL
        }

        public SlotState slotState = SlotState.NONE;

        public Vector2 pos;
        public bool isPlaced;
        public int index;
        public Texture2D texture;
        public Texture2D texureHaveRobo;
        public Texture2D textureFull;
        public float scale = 0.625f;

        public Slot(ContentManager Content, Vector2 initPos, int index)
        {
            pos = initPos;
            isPlaced = false;
            this.index = index;
            texureHaveRobo = Asset.positionHalfFull;
            textureFull = Asset.positionFull;
            if (index == 1)texture = Asset.positionI;
            else if (index == 2) texture = Asset.positionII;
            else texture = Asset.positionIII;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, float opacity)
        {
            if (slotState == SlotState.NONE)
                spriteBatch.Draw(texture, pos, null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            else if (slotState == SlotState.ROBO)
                spriteBatch.Draw(texureHaveRobo, pos, null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            else
                spriteBatch.Draw(textureFull, pos, null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
        }

        public Rectangle BoundingBox()
        {
            return new Rectangle((int)(pos.X + 30), (int)(pos.Y + 55), (int)(texture.Width * scale / 4), (int)(texture.Width * scale / 4));
        }

    }
}
