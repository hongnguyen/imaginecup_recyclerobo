﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using ProjectMercury;
using System.Threading;
using LoadData;

namespace RecycleRobo
{
    public struct Node
    {
        String animationName;
        public String AnimationName
        {
            get { return animationName; }
            set { animationName = value; }
        }

        Animation animation;
        public Animation Animation
        {
            get { return animation; }
            set { animation = value; }
        }

        public Node(String nameofAnimation, Animation Anim)
        {
            animationName = nameofAnimation;
            animation = Anim;
        }
    }

    public class LoadingAssets
    {
        String path = "ElementBoost/area/";
        public LoadingAssets(ContentManager Content)
        {
            LoadingAnimation(Content);
            LoadingTexture(Content);
        }

        public void LoadingAnimation(ContentManager Content)
        {
            List<Node> TotalAnimation = new List<Node>();
            Animation animation; Node node;

            while (GlobalClass.percentage < 50)
            {
                if (GlobalClass.percentage == 0)
                {
                    /* MiniGame */
                    animation = new Animation(Content.Load<Texture2D>("Minigame/energyball_3x6"), 810, 407, 3, 6, 0.09f, true);
                    node = new Node("ENERGY_BALL", animation);
                    TotalAnimation.Add(node);
                    /*Load animation for Robo*/

                    #region DEMO
                    /*----------------------- NUM 1 ------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/number1_idle_3x4"), 728, 594, 3, 4, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/number1_attack_4x5"), 890, 776, 4, 5, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/number1_walk_4x6"), 864, 736, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/number1_die_2x7"), 1260, 393, 2, 7, 0.09f, false);
                    node = new Node("NUMBER1_DIE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl1_form2_idle_3x4"), 728, 595, 3, 4, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_1_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl1_form2_attack_5x4"), 1038, 970, 5, 4, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_1_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl1_form2_walk_6x4"), 804, 1104, 6, 4, 0.02f, true);
                    node = new Node("NUMBER1_WALK_1_THORN", animation);
                    TotalAnimation.Add(node);




                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_idle_3x6"), 1500, 694, 3, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_attack_3x6"), 1500, 694, 3, 6, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_walk_4x6"), 1500, 925, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_die_2x6"), 1500, 462, 2, 6, 0.09f, false);
                    node = new Node("NUMBER1_DIE_2", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form2_idle_3x6"), 1500, 694, 3, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form2_attack_6x3"), 963, 1388, 6, 3, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form2_walk_4x6"), 1500, 925, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form3_idle_3x6"),1500, 694, 3, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_2_SEAL", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form3_attack_3x6"), 1500, 694, 3, 6, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_2_SEAL", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl2_form3_walk_4x6"), 1500, 925, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_2_SEAL", animation);
                    TotalAnimation.Add(node);



                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_idle_2x6"), 1620, 499, 2, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_attack_4x6"), 1620, 999, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_walk_2x6"), 1620, 499, 2, 6, 0.1f, true);
                    node = new Node("NUMBER1_WALK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_die_2x6"), 1620, 499, 2, 6, 0.09f, false);
                    node = new Node("NUMBER1_DIE_3", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form2_idle_2x6"), 1620, 499, 2, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form2_attack_6x4"), 1165, 1497, 6, 4, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form2_walk_2x6"), 1620, 499, 2, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form3_idle_2x6"), 1620, 499, 2, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_3_SEAL", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form3_attack_4x6"), 1620, 999, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_3_SEAL", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form3_walk_2x6"), 1620, 499, 2, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_3_SEAL", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form4_idle_2x6"), 1620, 499, 2, 6, 0.09f, true);
                    node = new Node("NUMBER1_IDLE_3_FIRE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form4_attack_4x6"), 1620, 999, 4, 6, 0.02f, true);
                    node = new Node("NUMBER1_ATTACK_3_FIRE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num1/num1_lvl3_form4_walk_2x6"), 1620, 499, 2, 6, 0.02f, true);
                    node = new Node("NUMBER1_WALK_3_FIRE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>(path + "num1/num1_area1"), 683, 780, 6, 3, 0.07f, true);
                    node = new Node("NUM1_AREA_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num1/num1_area2"), 1278, 1368, 6, 3, 0.07f, true);
                    node = new Node("NUM1_AREA_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num1/num1_area3"), 1568, 1932, 6, 3, 0.07f, true);
                    node = new Node("NUM1_AREA_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num1/num1_area4"), 1603, 1620, 6, 3, 0.07f, true);
                    node = new Node("NUM1_AREA_4", animation);
                    TotalAnimation.Add(node);
                    #endregion

                    #region Number Two
                    /*----------------------- NUM 2 ------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/number2_idle_3x4"), 617, 594, 3, 4, 0.09f, true);
                    node = new Node("NUMBER2_IDLE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/number2_attack_2x11"), 1991, 396, 2, 11, 0.04f, true);
                    node = new Node("NUMBER2_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/number2_walk_1x6"), 900, 198, 1, 6, 0.07f, true);
                    node = new Node("NUMBER2_WALK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/number2_die_3x5"), 750, 438, 3, 5, 0.09f, false);
                    node = new Node("NUMBER2_DIE_1", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl2_idle_2x6"), 1320, 438, 2, 6, 0.09f, true);
                    node = new Node("NUMBER2_IDLE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl2_attack_4x6"), 1320, 876, 4, 6, 0.0367f, true);
                    node = new Node("NUMBER2_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl2_walk_1x1"), 220, 219, 1, 1, 0.03f, true);
                    node = new Node("NUMBER2_WALK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl2_die_4x3"), 660, 876, 4, 3, 0.09f, false);
                    node = new Node("NUMBER2_DIE_2", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_idle_2x6"), 1560, 540, 2, 6, 0.09f, true);
                    node = new Node("NUMBER2_IDLE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_attack_3x6"), 1560, 809, 3, 6, 0.0489f, true);
                    node = new Node("NUMBER2_ATTACK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_walk_1x1"), 260, 270, 1, 1, 0.03f, true);
                    node = new Node("NUMBER2_WALK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_die_2x6"), 1560, 540, 2, 6, 0.09f, false);
                    node = new Node("NUMBER2_DIE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_e4_idle_2x6"), 1560, 540, 2, 6, 0.09f, true);
                    node = new Node("NUMBER2_IDLE_3_PHOENIX", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_e4_attack_3x6"), 1560, 810, 3, 6, 0.0489f, true);
                    node = new Node("NUMBER2_ATTACK_3_PHOENIX", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num2/num2_lvl3_e4_walk_1x1"), 260, 276, 1, 1, 0.03f, true);
                    node = new Node("NUMBER2_WALK_3_PHOENIX", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("ElementBoost/area/num2/num2_area1"), 1620, 411, 3, 6, 0.07f, true);
                    node = new Node("NUM2_AREA_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("ElementBoost/area/num2/num2_area4"), 1620, 431, 3, 6, 0.07f, true);
                    node = new Node("NUM2_AREA_4", animation);
                    TotalAnimation.Add(node);
                    #endregion

                    GlobalClass.percentage = 10;
                }
                else if (GlobalClass.percentage == 10)
                {
                    #region Number Three
                    /*----------------------- NUM 3 ------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl1_idle_4x6"), 1200, 941, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl1_attack_4x6"), 1200, 942, 4, 6, 0.035f, true);
                    node = new Node("NUMBER3_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl1_die_2x6"), 1200, 471, 2, 6, 0.08f, false);
                    node = new Node("NUMBER3_DIE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl1_walk_4x6"), 1200, 941, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_WALK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_idle_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_attack_5x6"), 1380, 1174, 5, 6, 0.028f, true);
                    node = new Node("NUMBER3_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_die_2x6"), 1380, 469, 2, 6, 0.08f, false);
                    node = new Node("NUMBER3_DIE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_walk_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_WALK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form2_idle_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_2_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form2_attack_5x6"), 1380, 1174, 5, 6, 0.028f, true);
                    node = new Node("NUMBER3_ATTACK_2_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form2_walk_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_WALK_2_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form3_idle_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_2_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form3_attack_5x6"), 1380, 1174, 5, 6, 0.028f, true);
                    node = new Node("NUMBER3_ATTACK_2_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl2_form3_walk_4x6"), 1380, 939, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_WALK_2_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/number3_idle_4x6"), 1338, 988, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/number3_attack_4x6"), 1500, 1112, 4, 6, 0.035f, true);
                    node = new Node("NUMBER3_ATTACK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/number3_walk_4x6"), 1392, 1028, 4, 6, 0.03f, true);
                    node = new Node("NUMBER3_WALK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/number3_die_3x4"), 792, 675, 3, 4, 0.07f, false);
                    node = new Node("NUMBER3_DIE_3", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form2_idle_4x6"), 1338, 988, 4, 6, 0.035f, true);
                    node = new Node("NUMBER3_IDLE_3_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form2_attack_4x6"), 1500, 1112, 4, 6, 0.035f, true);
                    node = new Node("NUMBER3_ATTACK_3_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form2_walk_4x6"), 1392, 1028, 4, 6, 0.03f, true);
                    node = new Node("NUMBER3_WALK_3_CUTTER", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form3_idle_4x6"), 1338, 988, 4, 6, 0.07f, true);
                    node = new Node("NUMBER3_IDLE_3_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form3_attack_4x6"), 1500, 1112, 4, 6, 0.035f, true);
                    node = new Node("NUMBER3_ATTACK_3_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num3/num3_lvl3_form3_walk_4x6"), 1392, 1028, 4, 6, 0.03f, true);
                    node = new Node("NUMBER3_WALK_3_ICEFANG", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num3/num3_area1"), 844, 900, 6, 3, 0.07f, true);
                    node = new Node("NUM3_AREA_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num3/num3_area2"), 1405, 1500, 6, 3, 0.07f, true);
                    node = new Node("NUM3_AREA_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num3/num3_area3"), 1236, 1320, 6, 3, 0.07f, true);
                    node = new Node("NUM3_AREA_3", animation);
                    TotalAnimation.Add(node);
                    #endregion   
                    #region Number Four
                    /*----------------------- NUM 4 ------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl1_idle_2x6"), 1020, 409, 2, 6, 0.09f, true);
                    node = new Node("NUMBER4_IDLE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl1_heal_5x6"), 1020, 1022,5, 6, 0.05f, true);
                    node = new Node("NUMBER4_HEALING_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl1_walk_4x6"), 1020, 818, 4, 6, 0.04f, true);
                    node = new Node("NUMBER4_WALK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl1_die_2x6"), 1020, 409, 2, 6, 0.08f, false);
                    node = new Node("NUMBER4_DIE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/number4_idle_4x6"), 1200, 962, 4, 6, 0.07f, true);
                    node = new Node("NUMBER4_IDLE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/number4_healing_4x6"), 1182, 1028, 4, 6, 0.05f, true);
                    node = new Node("NUMBER4_HEALING_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/number4_walk_4x4"), 808, 972, 4, 4, 0.04f, true);
                    node = new Node("NUMBER4_WALK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/number4_die_3x5"), 985, 711, 3, 5, 0.09f, false);
                    node = new Node("NUMBER4_DIE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl2_form3_idle_4x6"), 1200, 962, 4, 6, 0.07f, true);
                    node = new Node("NUMBER4_IDLE_2_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl2_form3_walk_4x4"), 808, 971, 4, 4, 0.04f, true);
                    node = new Node("NUMBER4_WALK_2_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl2_form3_heal_4x6"), 1182, 1027, 4, 6, 0.05f, true);
                    node = new Node("NUMBER4_HEALING_2_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_idle_4x6"), 1350, 1164, 4, 6, 0.06f, true);
                    node = new Node("NUMBER4_IDLE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_heal_4x6"), 1350, 1164, 4, 6, 0.07f, true);
                    node = new Node("NUMBER4_HEALING_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_walk_4x6"), 1350, 1164, 4, 6, 0.04f, true);
                    node = new Node("NUMBER4_WALK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_die_2x6"), 1350, 582, 2, 6, 0.08f, false);
                    node = new Node("NUMBER4_DIE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_form3_idle_4x6"), 1350, 1164, 4, 6, 0.06f, true);
                    node = new Node("NUMBER4_IDLE_3_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_form3_heal_4x6"), 1350, 1164, 4, 6, 0.07f, true);
                    node = new Node("NUMBER4_HEALING_3_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num4/num4_lvl3_form3_walk_4x6"), 1350, 1164, 4, 6, 0.04f, true);
                    node = new Node("NUMBER4_WALK_3_HERB", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num4/num4_area1"), 902, 900, 6, 3, 0.07f, true);
                    node = new Node("NUM4_AREA_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num4/num4_area3"), 1317, 1314, 6, 3, 0.07f, true);
                    node = new Node("NUM4_AREA_3", animation);
                    TotalAnimation.Add(node);
                    #endregion

                    GlobalClass.percentage = 20;
                }
                else if (GlobalClass.percentage == 20)
                {
                    #region Number Five
                    /*----------------------- NUM 5 ------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_walk_6x4"), 852, 880, 6, 4, 0.03f, true);
                    node = new Node("NUMBER5_WALK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_attack_6x4"), 851, 880, 6, 4, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_idle_6x2"), 426, 880, 6, 2, 0.09f, true);
                    node = new Node("NUMBER5_IDLE_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_die_6x2"), 426, 880, 6, 2, 0.09f, false);
                    node = new Node("NUMBER5_DIE_1", animation);
                    TotalAnimation.Add(node);

                    //-------------------------- lv 1 form 2 ----------------------------------//
                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_form2_walk_6x4"), 852, 880, 6, 4, 0.03f, true);
                    node = new Node("NUMBER5_WALK_1_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_form2_attack_6x4"), 852, 880, 6, 4, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_1_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl1_form2_idle_6x2"), 426, 880, 6, 2, 0.09f, true);
                    node = new Node("NUMBER5_IDLE_1_THORN", animation);
                    TotalAnimation.Add(node);
                    //--------------------------------------------------------------------------

                    /*-------------------------------Lv 2 --------------------------------------*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_walk_4x6"), 1350, 683, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_attack_4x6"), 1350, 683, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_idle_2x6"), 1350, 341, 2, 6, 0.09f, true);
                    node = new Node("NUMBER5_IDLE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_die_2x6"), 1350, 341, 2, 6, 0.09f, false);
                    node = new Node("NUMBER5_DIE_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form2_walk_4x6"), 1350, 683, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form2_attack_4x6"), 1350, 683, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form2_idle_2x6"), 1350, 341, 2, 6, 0.09f, true);
                    node = new Node("NUMBER5_IDLE_2_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form3_walk_4x6"), 1350, 683, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_2_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form3_attack_4x6"), 1350, 683, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_2_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl2_form3_idle_2x6"), 1350, 341, 2, 6, 0.09f, true);
                    node = new Node("NUMBER5_IDLE_2_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_walk_4x6"), 1350, 924, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_attack_4x6"), 1350, 924, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_idle_4x6"), 1350, 923, 4, 6, 0.07f, true);
                    node = new Node("NUMBER5_IDLE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_die_2x6"), 1350, 462, 2, 6, 0.09f, false);
                    node = new Node("NUMBER5_DIE_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form2_walk_4x6"), 1350, 924, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form2_attack_4x6"), 1350, 924, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form2_idle_4x6"), 1350, 924, 4, 6, 0.07f, true);
                    node = new Node("NUMBER5_IDLE_3_THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form3_walk_4x6"), 1350, 924, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_3_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form3_attack_4x6"), 1350, 924, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_3_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form3_idle_4x6"), 1350, 924, 4, 6, 0.07f, true);
                    node = new Node("NUMBER5_IDLE_3_WAVE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form4_walk_4x6"), 1350, 924, 4, 6, 0.03f, true);
                    node = new Node("NUMBER5_WALK_3_STEAM", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form4_attack_4x6"), 1350, 924, 4, 6, 0.05f, true);
                    node = new Node("NUMBER5_ATTACK_3_STEAM", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/num5/num5_lvl3_form4_idle_4x6"), 1350, 924, 4, 6, 0.07f, true);
                    node = new Node("NUMBER5_IDLE_3_STEAM", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num5/num5_area1"),804, 900,  6, 3, 0.07f, true);
                    node = new Node("NUM5_AREA_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num5/num5_area2"), 1371, 1536,  6, 3, 0.07f, true);
                    node = new Node("NUM5_AREA_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num5/num5_area3"), 1607, 1800, 6, 3, 0.07f, true);
                    node = new Node("NUM5_AREA_3", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>(path + "num5/num5_area4"), 1446, 1620, 6, 3, 0.07f, true);
                    node = new Node("NUM5_AREA_4", animation);
                    TotalAnimation.Add(node);
                    #endregion

                    GlobalClass.percentage = 30;
                }
                else if (GlobalClass.percentage == 30)
                {
                    #region TRASH 1234

                    /*Load animation for Monster*/
                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash1_walk_4x6"), 996, 758, 4, 6, 0.03f, true);
                    node = new Node("TRASH1_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash1_attack_4x6"), 1236, 884, 4, 6, 0.03f, true);
                    node = new Node("TRASH1_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash1_die_3x4"), 680, 573, 3, 4, 0.1f, false);
                    node = new Node("TRASH1_DIE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash2_attack_3x7"), 1193, 615, 3, 7, 0.06f, true);
                    node = new Node("TRASH2_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash2_walk_4x6"), 1000, 791, 4, 6, 0.045f, true);
                    node = new Node("TRASH2_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash2_die_2x6"), 982, 372, 2, 6, 0.1f, false);
                    node = new Node("TRASH2_DIE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash3_attack_2x6"), 1740, 350, 2, 6, 0.15f, true);
                    node = new Node("TRASH3_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash3_walk_3x6"), 1152, 551, 3, 6, 0.15f, true);
                    node = new Node("TRASH3_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash3_die_2x6"), 1080, 384, 2, 6, 0.1f, false);
                    node = new Node("TRASH3_DIE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash4_attack_2x8"), 1392, 304, 2, 8, 0.06f, true);
                    node = new Node("TRASH4_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash4_walk_3x8"), 1176, 468, 3, 8, 0.03f, true);
                    node = new Node("TRASH4_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash4_die_2x6"), 846, 275, 2, 6, 0.1f, false);
                    node = new Node("TRASH4_DIE", animation);
                    TotalAnimation.Add(node);

                    #endregion
                    #region TRASH 5 6..11
                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash5_attack_3x6"), 1440, 683, 3, 6, 0.06f, true);
                    node = new Node("TRASH5_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash5_walk_2x6"), 1440, 455, 2, 6, 0.04f, true);
                    node = new Node("TRASH5_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash5_die_2x6"), 1440, 455, 2, 6, 0.1f, false);
                    node = new Node("TRASH5_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash6_attack_4x6"), 1620, 1025, 4, 6, 0.06f, true);
                    node = new Node("TRASH6_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash6_walk_2x6"), 1620, 512, 2, 6, 0.03f, true);
                    node = new Node("TRASH6_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash6_die_2x6"), 1620, 512, 2, 6, 0.1f, false);
                    node = new Node("TRASH6_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash7_attack_2x6"),1200, 470, 2, 6, 0.05f, true);
                    node = new Node("TRASH7_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash7_walk_2x6"), 1200, 471, 2, 6, 0.07f, true);
                    node = new Node("TRASH7_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash7_die_2x6"), 1200, 471, 2, 6, 0.1f, false);
                    node = new Node("TRASH7_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash8_attack_2x6"), 1200, 433, 2, 6, 0.05f, true);
                    node = new Node("TRASH8_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash8_walk_2x6"), 1200, 434, 2, 6, 0.03f, true);
                    node = new Node("TRASH8_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash8_die_2x6"), 1200, 434, 2, 6, 0.1f, false);
                    node = new Node("TRASH8_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash9_attack_5x3"), 630, 702, 5, 3, 0.05f, true);
                    node = new Node("TRASH9_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash9_walk_4x3"), 630, 562, 4, 3, 0.09f, true);
                    node = new Node("TRASH9_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash9_die_3x3"), 630, 421, 3, 3, 0.08f, false);
                    node = new Node("TRASH9_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash10_attack_4x3"), 900, 1022, 4, 3, 0.06f, true);
                    node = new Node("TRASH10_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash10_walk_4x3"), 900, 1022, 4, 3, 0.05f, true);
                    node = new Node("TRASH10_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash10_die_4x3"), 900, 1022, 4, 3, 0.08f, false);
                    node = new Node("TRASH10_DIE", animation);
                    TotalAnimation.Add(node);


                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash11_attack_3x5"), 1438, 750, 3, 5, 0.05f, true);
                    node = new Node("TRASH11_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash11_walk_3x6"), 1726, 750, 3, 6, 0.06f, true);
                    node = new Node("TRASH11_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/trash11_die_3x4"), 1151, 750, 3, 4, 0.08f, false);
                    node = new Node("TRASH11_DIE", animation);
                    TotalAnimation.Add(node);
                    #endregion

                    GlobalClass.percentage = 40;
                }
                else if (GlobalClass.percentage == 40)
                {
                    #region OtherAnimation
                    //****************************** BOSS ONE ANIMATION *******************************//

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/boss1/attack1"), 1416, 1395, 5, 4, 0.06f, true);
                    node = new Node("BOSS1_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/boss1/attack2"), 1399, 2000, 7, 3, 0.08f, true);
                    node = new Node("BOSS1_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/boss1/idle"), 780, 792, 3, 3, 0.07f, true);
                    node = new Node("BOSS1_IDLE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/boss1/walk"), 810, 728, 3, 3, 0.07f, true);
                    node = new Node("BOSS1_WALK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/trash/boss1/die_3x3"), 720, 740, 3, 3, 0.07f, false);
                    node = new Node("BOSS1_DIE", animation);
                    TotalAnimation.Add(node);
                    //******************************************************************/

                    /******************Load animation for skills**********************/

                    //skill 1

                    animation = new Animation(Content.Load<Texture2D>("skills/skill1/skill1_shine_2x3"), 588, 426, 2, 3, 0.06f, true);
                    node = new Node("SKILL1_SHINE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill1/skill1_attack_part1_3x5"), 1005, 656, 3, 5, 0.06f, false);
                    node = new Node("SKILL_ATTACK_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill1/skill1_attack_part2_1x6"), 1180, 213, 1, 6, 0.03f, false);
                    node = new Node("SKILL_ATTACK_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill1/sword_forming_6x1"), 1208, 1758, 6, 1, 0.3f, false);
                    node = new Node("CREATE_SWORD", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill1/sword_2x1"), 1208, 586, 2, 1, 2f, true);
                    node = new Node("SWORD", animation);
                    TotalAnimation.Add(node);

                    //skill2
                    animation = new Animation(Content.Load<Texture2D>("skills/skill2/skill2_shine_2x4"), 1524, 782, 2, 4, 0.08f, true);
                    node = new Node("SKILL2_SHINE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill2/skill2_attack_4x5"), 1765, 1880, 4, 5, 0.065f, true);
                    node = new Node("SKILL2_ATTACK", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill2/skill2_combine_4x5"), 1895, 1564, 4, 5, 0.13f, false);
                    node = new Node("SKILL2_COMBINE", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill2/projectile_effect_1x9"), 666, 90, 1, 9, 0.07f, true);
                    node = new Node("SKILL2_PROJECTILE", animation);
                    TotalAnimation.Add(node);

                    // texture skill gigaslash
                    Asset.announceNum1 = Content.Load<Texture2D>("skills/skill2/skill2_num1");
                    Asset.announceNum3 = Content.Load<Texture2D>("skills/skill2/skill2_num3");
                    Asset.skillCombination = Content.Load<Texture2D>("skills/skill2/combination");
                    Asset.timeHole = Content.Load<Texture2D>("skills/skill2/timeHole");
                    Asset.skyPair = Content.Load<Texture2D>("skills/skill2/skyPair");
                    Asset.landPair = Content.Load<Texture2D>("skills/skill2/landPair");
                    Asset.effectGiga = Content.Load<ParticleEffect>("skills/skill2/hitBySkill2");

                    //skill3
                    animation = new Animation(Content.Load<Texture2D>("skills/skill3/holyHeal_lvl1"), 1020, 1022, 5, 6, 0.07f, true);
                    node = new Node("CREATE_HOLY_1", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill3/skill3_heal_4x6"), 1182, 1028, 4, 6, 0.07f, true);
                    node = new Node("CREATE_HOLY_2", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("skills/skill3/holyHeal_lvl3"), 1380, 1235, 4, 6, 0.07f, true);
                    node = new Node("CREATE_HOLY_3", animation);
                    TotalAnimation.Add(node);


                    /******************************************Effect animation***************************************/
                    animation = new Animation(Content.Load<Texture2D>("Animations/Effect/torrentacle"), 4, 4, 0.125f, true);
                    node = new Node("OCEAN_PRISON", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/Effect/thorn"), 2, 5, 0.1f, true);
                    node = new Node("THORN", animation);
                    TotalAnimation.Add(node);

                    animation = new Animation(Content.Load<Texture2D>("Animations/Effect/leafcut"), 3, 3, 0.12f, true);
                    node = new Node("LEAFCUT", animation);
                    TotalAnimation.Add(node);

                    GlobalClass.ToTalAnimation = TotalAnimation;

                    /**********************************Gesture Animation**************************************/
                    Asset.gestureVerticle = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureVer_1x8"), 300, 96, 1, 8, 0.12f, true);
                    Asset.gestureHorizontal = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureHor_8x1"), 160, 500, 8, 1, 0.12f, true);
                    Asset.gestureU = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureU_3x7"), 700, 305, 3, 7, 0.08f, true);
                    Asset.gestureV = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureV_2x9"), 810, 181, 2, 9, 0.08f, true);
                    Asset.gestureZ = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureZ_2x7"), 740, 193, 2, 7, 0.1f, true);
                    Asset.gestureN = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureN_2x7"), 630, 197, 2, 7, 0.1f, true);
                    Asset.gestureO = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureO_3x7"), 900, 386, 3, 7, 0.08f, true);
                    Asset.gestureTap = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureTap_1x4"), 400, 109, 1, 4, 0.15f, true);
                    Asset.gestureNULL = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureTap_1x4"), 400, 109, 1, 4, 0.15f, true);
                    Asset.gestureNone = new Animation(Content.Load<Texture2D>("ElementBoost/gesture/gestureNone_1x4"), 120, 33, 1, 4, 0.15f, true);

                    /*Animation boss 2*/
                    Asset.appear = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_appear_4x3"), 1647, 1900, 4, 3, 0.09f, false);
                    Asset.disappear = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_disappear_4x3"), 1647, 1900, 4, 3, 0.09f, false);
                    Asset.gas = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_attack1_gas_4x3"), 1800, 1000, 4, 3, 0.07f, true);
                    Asset.boss2_attack1 = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_attack1_4x4"), 1436, 1528, 4, 4, 0.08f, true);
                    Asset.boss2_attack2 = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_attack2_3x3"), 1092, 1035,3, 3, 0.08f, true);
                    Asset.boss2_bullet = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_attack2_bullet_4x2"), 464, 320, 4, 2, 0.08f, true);
                    Asset.boss2_die1 = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_die1_3x3"), 1077, 1146, 3, 3, 0.08f, false);
                    Asset.boss2_die2 = new Animation(Content.Load<Texture2D>("Animations/trash/boss2/boss2_die2_3x3"), 1092, 1035, 3, 3, 0.08f, false);
                    #endregion
                   
                    GlobalClass.percentage = 50;
                }
            }

        }
        public void LoadingTexture(ContentManager Content)

        {
            while (GlobalClass.percentage < 100 && GlobalClass.percentage >= 49)
            {
                if (GlobalClass.percentage == 50)
                {
                    Asset.bgMiniGame = Content.Load<Texture2D>("MiniGame/background");
                    Asset.bgMiniGameForce = Content.Load<Texture2D>("MiniGame/background_force");
                    Asset.bgMiniGameForce3 = Content.Load<Texture2D>("MiniGame/minigame3");
                    Asset.victoryBg = Content.Load<Texture2D>("Victory\\victory\\background");
                    Asset.loseBg = Content.Load<Texture2D>("Victory/backgroundLose");

                    Asset.world1 = Content.Load<Texture2D>("WorldMap/world1");
                    Asset.world2 = Content.Load<Texture2D>("WorldMap/world2");
                    Asset.world3 = Content.Load<Texture2D>("WorldMap/world3");

                    Asset.barDemo_1 = Content.Load<Texture2D>("Victory/victory/barDemo_1");
                    Asset.barDemo_2 = Content.Load<Texture2D>("Victory/victory/barDemo_2");
                    Asset.barDemo_3 = Content.Load<Texture2D>("Victory/victory/barDemo_3");

                    Asset.barGeorge_1 = Content.Load<Texture2D>("Victory/victory/barGeorge_1");
                    Asset.barGeorge_2 = Content.Load<Texture2D>("Victory/victory/barGeorge_2");
                    Asset.barGeorge_3 = Content.Load<Texture2D>("Victory/victory/barGeorge_3");

                    Asset.barRay_1 = Content.Load<Texture2D>("Victory/victory/barRay_1");
                    Asset.barRay_2 = Content.Load<Texture2D>("Victory/victory/barRay_2");
                    Asset.barRay_3 = Content.Load<Texture2D>("Victory/victory/barRay_3");

                    Asset.barSoul_1 = Content.Load<Texture2D>("Victory/victory/barSoul_1");
                    Asset.barSoul_2 = Content.Load<Texture2D>("Victory/victory/barSoul_2");
                    Asset.barSoul_3 = Content.Load<Texture2D>("Victory/victory/barSoul_3");

                    Asset.barCrusher_1 = Content.Load<Texture2D>("Victory/victory/barCrusher_1");
                    Asset.barCrusher_2 = Content.Load<Texture2D>("Victory/victory/barCrusher_2");
                    Asset.barCrusher_3 = Content.Load<Texture2D>("Victory/victory/barCrusher_3");

                    Asset.notifBackground = Content.Load<Texture2D>("Victory/background");
                    Asset.quangsang = Content.Load<Texture2D>("Victory/victory/quangSang");

                    Asset.nextButton = Content.Load<Texture2D>("Victory/nextButton");
                    Asset.nextButtonHover = Content.Load<Texture2D>("Victory/nextButtonHover");

                    Asset.twitterButton = Content.Load<Texture2D>("Victory/twitterButton");
                    Asset.facebookButton = Content.Load<Texture2D>("Victory/fbButton");
                    Asset.twitterButton_Hover = Content.Load<Texture2D>("Victory/twitterButton_Hover");
                    Asset.facebookButton_Hover = Content.Load<Texture2D>("Victory/fbButton_Hover");
                    Asset.typeAchie = Content.Load<Texture2D>("Victory/newAchievement/type");
                    Asset.inforAchie = Content.Load<Texture2D>("Victory/newAchievement/info");
                    Asset.circle = Content.Load<Texture2D>("Victory/circle");

                    Asset.typeAss = Content.Load<Texture2D>("Victory/newAss/type");
                    Asset.inforAss = Content.Load<Texture2D>("Victory/newAss/info");

                    Asset.typeCycle = Content.Load<Texture2D>("Victory/newCyclebot/type");
                    Asset.inforCycle = Content.Load<Texture2D>("Victory/newCyclebot/info");

                    Asset.typeEle = Content.Load<Texture2D>("Victory/newElement/type");
                    Asset.inforEle = Content.Load<Texture2D>("Victory/newElement/info");

                    Asset.crusher1 = Content.Load<Texture2D>("Victory/newCyclebot/crusher1");
                    Asset.crusher2 = Content.Load<Texture2D>("Victory/newCyclebot/crusher2");
                    Asset.crusher3 = Content.Load<Texture2D>("Victory/newCyclebot/crusher3");

                    Asset.ray1 = Content.Load<Texture2D>("Victory/newCyclebot/ray1");
                    Asset.ray2 = Content.Load<Texture2D>("Victory/newCyclebot/ray2");
                    Asset.ray3 = Content.Load<Texture2D>("Victory/newCyclebot/ray3");

                    Asset.soul1 = Content.Load<Texture2D>("Victory/newCyclebot/soul1");
                    Asset.soul2 = Content.Load<Texture2D>("Victory/newCyclebot/soul2");
                    Asset.soul3 = Content.Load<Texture2D>("Victory/newCyclebot/soul3");

                    Asset.george1 = Content.Load<Texture2D>("Victory/newCyclebot/george1");
                    Asset.george2 = Content.Load<Texture2D>("Victory/newCyclebot/george2");
                    Asset.george3 = Content.Load<Texture2D>("Victory/newCyclebot/george3");

                    Asset.demo1 = Content.Load<Texture2D>("Victory/newCyclebot/demo1");
                    Asset.demo2 = Content.Load<Texture2D>("Victory/newCyclebot/demo2");
                    Asset.demo3 = Content.Load<Texture2D>("Victory/newCyclebot/demo3");

                    GlobalClass.percentage = 60;
                }
                else if (GlobalClass.percentage == 60)
                {
                    Asset.backButton = Content.Load<Texture2D>("Maps/backButton");
                    Asset.introButton1 = Content.Load<Texture2D>("Maps/buttonStory1");
                    Asset.introButton2 = Content.Load<Texture2D>("Maps/buttonStory2");
                    Asset.shopMapButton = Content.Load<Texture2D>("Maps/buttonShop");
                    Asset.achieveButton = Content.Load<Texture2D>("Maps/buttonAchievement");
                    Asset.skipButton = Content.Load<Texture2D>("Introduction/buttonSkip");
                    Asset.backButtonRight = Content.Load<Texture2D>("Maps/backButtonRight");

                    Asset.introOne = Content.Load<Texture2D>("Introduction/officialIntro");
                    Asset.introTwo = Content.Load<Texture2D>("Introduction/officialIntro2");

                    Asset.Ass_normal = Content.Load<Texture2D>("Assistant/minitor");
                    Asset.Ass_megadroid = Content.Load<Texture2D>("Assistant/megadroid");
                    Asset.Ass_gigadroid = Content.Load<Texture2D>("Assistant/gigadroid");
                    Asset.Ass_defentant = Content.Load<Texture2D>("Assistant/defentant");
                    Asset.Ass_steelKnight = Content.Load<Texture2D>("Assistant/steelKnight");
                    Asset.Ass_thunderbolt1 = Content.Load<Texture2D>("Assistant/thunderbolt1");
                    Asset.Ass_thunderbolt2 = Content.Load<Texture2D>("Assistant/thunderbolt2");

                    Asset.fontMinigame = Content.Load<SpriteFont>("FontMinigame");
                    Asset.mediumFont = Content.Load<SpriteFont>("newFont");
                    Asset.largeFont = Content.Load<SpriteFont>("largeFont");
                    Asset.smallFont = Content.Load<SpriteFont>("smallFont");

                    Asset.land1 = Content.Load<Texture2D>("MapSelect/land1");
                    Asset.land2 = Content.Load<Texture2D>("MapSelect/land2");
                    Asset.cloudTexture = Content.Load<Texture2D>("MapSelect/cloud");
                    Asset.ocean = Content.Load<Texture2D>("MapSelect/ocean");
                    Asset.mapLock = Content.Load<Texture2D>("MapSelect/lock");

                    /* Texture at Menu */
                    Asset.fade = Content.Load<Texture2D>("HowToPlay/fade");
                    Asset.bgSound1 = Content.Load<Song>("sounds/bg/bg_0");
                    Asset.bgSound2 = Content.Load<Song>("sounds/bg/bg_1");
                    Asset.bgSound3 = Content.Load<Song>("sounds/bg/bg_2");


                    Asset.guidePlayScene = Content.Load<Texture2D>("Maps//guildAtPlayScene");
                    Asset.titleGame = Content.Load<Texture2D>("Menu/title");
                    Asset.menuBackground = Content.Load<Texture2D>("Menu/background");
                    Asset.menuCircle = Content.Load<Texture2D>("Menu/circle");
                    Asset.blackMist = Content.Load<Texture2D>("Menu/blackMist");
                    Asset.demo = Content.Load<Texture2D>("Menu/demo");
                    Asset.ray = Content.Load<Texture2D>("Menu/ray");
                    Asset.crusher = Content.Load<Texture2D>("Menu/crusher");
                    Asset.soul = Content.Load<Texture2D>("Menu/soul");
                    Asset.george = Content.Load<Texture2D>("Menu/george");

                    Asset.startButton = Content.Load<Texture2D>("Menu/playButton");
                    Asset.startText = Content.Load<Texture2D>("Menu/playText_hover");
                    Asset.shopButton = Content.Load<Texture2D>("Menu/shopButton");
                    Asset.shopText = Content.Load<Texture2D>("Menu/shopText_hover");
                    Asset.achievementButton = Content.Load<Texture2D>("Menu/achievementButton");
                    Asset.achievementText = Content.Load<Texture2D>("Menu/achievementText_hover");
                    Asset.optionButtonMenu = Content.Load<Texture2D>("Menu/optionButton");
                    Asset.optionButtonHoverMenu = Content.Load<Texture2D>("Menu/optionButton_hover");

                    Asset.resoTextureFull = Content.Load<Texture2D>("Setting/official/fullOnTile");
                    Asset.resoTextureNormal = Content.Load<Texture2D>("Setting/official/fullOffTile");
                    Asset.exitTexture = Content.Load<Texture2D>("Setting/official/exitTile");
                    Asset.exitTextureHover = Content.Load<Texture2D>("Setting/official/exitHover");
                    Asset.resumeTexture = Content.Load<Texture2D>("Setting/official/resumeTile");
                    Asset.resumeTextureHover = Content.Load<Texture2D>("Setting/official/resumeHover");
                    Asset.retreatTexture = Content.Load<Texture2D>("Setting/official/retreatTile");
                    Asset.retreatTextureHover = Content.Load<Texture2D>("Setting/official/retreatHover");
                    Asset.volumeTexture = Content.Load<Texture2D>("Setting/official/volumeTile");
                    Asset.aboutTexture = Content.Load<Texture2D>("Setting/official/aboutTile");

                    Asset.checkTexture = Content.Load<Texture2D>("Setting/official/checkButton");
                    Asset.checkTextureHover = Content.Load<Texture2D>("Setting/official/checkButtonHover");
                    Asset.crossTexture = Content.Load<Texture2D>("Setting/official/crossButton");
                    Asset.crossTextureHover = Content.Load<Texture2D>("Setting/official/crossButtonHover");

                    /* Texture at Recycle Factory*/
                    Asset.font = Content.Load<SpriteFont>("newfont");

                    Asset.megadroidBought = Content.Load<Texture2D>("Recycle/megadroid_bought");
                    Asset.megadroidBuy = Content.Load<Texture2D>("Recycle/megadroid_buy");
                    Asset.megadroidLock = Content.Load<Texture2D>("Recycle/megadroid_locked");

                    Asset.gigadroidBought = Content.Load<Texture2D>("Recycle/gigadroid_bought");
                    Asset.gigadroidBuy = Content.Load<Texture2D>("Recycle/gigadroid_buy");
                    Asset.gigadroidLock = Content.Load<Texture2D>("Recycle/gigadroid_locked");

                    Asset.knightBought = Content.Load<Texture2D>("Recycle/knight_bought");
                    Asset.knightBuy = Content.Load<Texture2D>("Recycle/knight_buy");
                    Asset.knightLock = Content.Load<Texture2D>("Recycle/knight_locked");

                    Asset.thunderBought = Content.Load<Texture2D>("Recycle/thunderbolt_bought");
                    Asset.thunderBuy = Content.Load<Texture2D>("Recycle/thunderbolt_buy");
                    Asset.thunderLock = Content.Load<Texture2D>("Recycle/thunderbolt_locked");

                    Asset.thunderbolt2Bought = Content.Load<Texture2D>("Recycle/THUNDERBOLT2_bought");
                    Asset.thunderbolt2Buy = Content.Load<Texture2D>("Recycle/THUNDERBOLT2_buy");
                    Asset.thunderbolt2Lock = Content.Load<Texture2D>("Recycle/THUNDERBOLT2_locked");

                    Asset.engine = Content.Load<Texture2D>("Recycle/engine1_locked");
                    Asset.healer1 = Content.Load<Texture2D>("Recycle/healer1_locked");
                    Asset.healer2 = Content.Load<Texture2D>("Recycle/healer2_locked");

                    Asset.defentantBought = Content.Load<Texture2D>("Recycle/defentant_bought");
                    Asset.defentantBuy = Content.Load<Texture2D>("Recycle/defentant_buy");
                    Asset.defentantLock = Content.Load<Texture2D>("Recycle/defentant_lock");
                    Asset.mini = Content.Load<Texture2D>("Recycle/minitor_bought");

                    GlobalClass.percentage = 70;
                }
                else if (GlobalClass.percentage == 70)
                {

                    Asset.resourceBar = Content.Load<Texture2D>("Recycle/energybar");
                    Asset.energyBall = Content.Load<Texture2D>("Recycle/energy ball");
                    Asset.taskBarAssistant = Content.Load<Texture2D>("Recycle/taskbar_assistantSelected");
                    Asset.taskBarCyclebot = Content.Load<Texture2D>("Recycle/taskbar_CyclebotSelected");

                    Asset.levelDisplay = Content.Load<Texture2D>("Recycle/cyclebot/levelDisplay");
                    Asset.level1_1 = Content.Load<Texture2D>("Recycle/cyclebot/num1_1elementUnlocked");
                    Asset.level2_1 = Content.Load<Texture2D>("Recycle/cyclebot/num1_2elementUnlocked");
                    Asset.level3_1 = Content.Load<Texture2D>("Recycle/cyclebot/num1_3elementUnlocked");
                    Asset.level4_1 = Content.Load<Texture2D>("Recycle/cyclebot/num1_4elementUnlocked");

                    Asset.selected_1 = Content.Load<Texture2D>("Recycle/selectedItems/1selected");
                    Asset.selected_2 = Content.Load<Texture2D>("Recycle/selectedItems/2selected");
                    Asset.selected_3 = Content.Load<Texture2D>("Recycle/selectedItems/3selected");
                    Asset.selected_4 = Content.Load<Texture2D>("Recycle/selectedItems/4selected");
                    Asset.selected_5 = Content.Load<Texture2D>("Recycle/selectedItems/5selected");
                    Asset.cyclebotList1 = Content.Load<Texture2D>("Recycle/selectedItems/cyclebot list 1");
                    Asset.cyclebotList2 = Content.Load<Texture2D>("Recycle/selectedItems/cyclebot list 2");
                    Asset.cyclebotList3 = Content.Load<Texture2D>("Recycle/selectedItems/cyclebot list 3");
                    Asset.cyclebotList4 = Content.Load<Texture2D>("Recycle/selectedItems/cyclebot list 4");
                    Asset.cyclebotList5 = Content.Load<Texture2D>("Recycle/selectedItems/cyclebot list 5");
                    Asset.unknown = Content.Load<Texture2D>("Recycle/selectedItems/initial list 0-unknown");

                    Asset.num1_lv1 = Content.Load<Texture2D>("Recycle/RobotIcons/num1_lvl1_complete");
                    Asset.num1_lv2 = Content.Load<Texture2D>("Recycle/RobotIcons/num1_lvl2_complete");
                    Asset.num1_lv3 = Content.Load<Texture2D>("Recycle/RobotIcons/num1_lvl3_complete");

                    Asset.num2_lv1 = Content.Load<Texture2D>("Recycle/RobotIcons/num2_lvl1_complete");
                    Asset.num2_lv2 = Content.Load<Texture2D>("Recycle/RobotIcons/num2_lvl2_complete");
                    Asset.num2_lv3 = Content.Load<Texture2D>("Recycle/RobotIcons/num2_lvl3_complete");

                    Asset.num4_lv1 = Content.Load<Texture2D>("Recycle/RobotIcons/num4_lvl1_complete");
                    Asset.num4_lv2 = Content.Load<Texture2D>("Recycle/RobotIcons/num4_lvl2_complete");
                    Asset.num4_lv3 = Content.Load<Texture2D>("Recycle/RobotIcons/num4_lvl3_complete");

                    Asset.num3_lv1 = Content.Load<Texture2D>("Recycle/RobotIcons/num3_lvl1_complete");
                    Asset.num3_lv2 = Content.Load<Texture2D>("Recycle/RobotIcons/num3_lvl2_complete");
                    Asset.num3_lv3 = Content.Load<Texture2D>("Recycle/RobotIcons/num3_lvl3_complete");

                    Asset.num5_lv1 = Content.Load<Texture2D>("Recycle/RobotIcons/num5_lvl1_complete");
                    Asset.num5_lv2 = Content.Load<Texture2D>("Recycle/RobotIcons/num5_lvl2_complete");
                    Asset.num5_lv3 = Content.Load<Texture2D>("Recycle/RobotIcons/num5_lvl3_complete");

                    Asset.descriptionBox = Content.Load<Texture2D>("Recycle/description");

                    Asset.desc1_1 = Content.Load<Texture2D>("Recycle/desc1/1");
                    Asset.desc1_2 = Content.Load<Texture2D>("Recycle/desc1/2");
                    Asset.desc1_3 = Content.Load<Texture2D>("Recycle/desc1/3");
                    Asset.desc1_4 = Content.Load<Texture2D>("Recycle/desc1/4");

                    Asset.desc2_1 = Content.Load<Texture2D>("Recycle/desc2/1");
                    Asset.desc2_2 = Content.Load<Texture2D>("Recycle/desc2/2");
                    Asset.desc2_3 = Content.Load<Texture2D>("Recycle/desc2/3");
                    Asset.desc2_4 = Content.Load<Texture2D>("Recycle/desc2/4");

                    Asset.desc2_1 = Content.Load<Texture2D>("Recycle/desc2/1");
                    Asset.desc2_2 = Content.Load<Texture2D>("Recycle/desc2/2");
                    Asset.desc2_3 = Content.Load<Texture2D>("Recycle/desc2/3");
                    Asset.desc2_4 = Content.Load<Texture2D>("Recycle/desc2/4");

                    Asset.desc3_1 = Content.Load<Texture2D>("Recycle/desc3/1");
                    Asset.desc3_2 = Content.Load<Texture2D>("Recycle/desc3/2");
                    Asset.desc3_3 = Content.Load<Texture2D>("Recycle/desc3/3");
                    Asset.desc3_4 = Content.Load<Texture2D>("Recycle/desc3/4");

                    Asset.desc4_1 = Content.Load<Texture2D>("Recycle/desc4/1");
                    Asset.desc4_2 = Content.Load<Texture2D>("Recycle/desc4/2");
                    Asset.desc4_3 = Content.Load<Texture2D>("Recycle/desc4/3");
                    Asset.desc4_4 = Content.Load<Texture2D>("Recycle/desc4/4");

                    Asset.desc5_1 = Content.Load<Texture2D>("Recycle/desc5/1");
                    Asset.desc5_2 = Content.Load<Texture2D>("Recycle/desc5/2");
                    Asset.desc5_3 = Content.Load<Texture2D>("Recycle/desc5/3");
                    Asset.desc5_4 = Content.Load<Texture2D>("Recycle/desc5/4");

                    Asset.correctSound = Content.Load<SoundEffect>("Minigame/correct");
                    Asset.wrongSound = Content.Load<SoundEffect>("Minigame/wrong");
                    Asset.soundButton = Content.Load<SoundEffect>("sounds/clicked");

                    GlobalClass.percentage = 80;
                }
                else if (GlobalClass.percentage == 80)
                {

                    /* Texture at Challenge */
                    Asset.challengeBackground = Content.Load<Texture2D>("Challenge/achievementBackground");
                    Asset.challengeTaskBar = Content.Load<Texture2D>("Challenge/taskbar");

                    Asset.challenge1 = Content.Load<Texture2D>("Challenge/1");
                    Asset.challenge2 = Content.Load<Texture2D>("Challenge/2");
                    Asset.challenge3 = Content.Load<Texture2D>("Challenge/3");
                    Asset.challenge4 = Content.Load<Texture2D>("Challenge/4");
                    Asset.challenge5 = Content.Load<Texture2D>("Challenge/5");
                    Asset.challenge6 = Content.Load<Texture2D>("Challenge/6");
                    Asset.challenge7 = Content.Load<Texture2D>("Challenge/7");
                    Asset.challenge8 = Content.Load<Texture2D>("Challenge/8");
                    Asset.challenge9 = Content.Load<Texture2D>("Challenge/9");
                    Asset.challenge10 = Content.Load<Texture2D>("Challenge/10");
                    Asset.challenge11 = Content.Load<Texture2D>("Challenge/11");
                    Asset.challenge12 = Content.Load<Texture2D>("Challenge/12");
                    Asset.challenge13 = Content.Load<Texture2D>("Challenge/13");

                    Asset.challenge1completed = Content.Load<Texture2D>("Challenge/1done");
                    Asset.challenge2completed = Content.Load<Texture2D>("Challenge/2done");
                    Asset.challenge3completed = Content.Load<Texture2D>("Challenge/3done");
                    Asset.challenge4completed = Content.Load<Texture2D>("Challenge/4done");
                    Asset.challenge5completed = Content.Load<Texture2D>("Challenge/5done");
                    Asset.challenge6completed = Content.Load<Texture2D>("Challenge/6done");
                    Asset.challenge7completed = Content.Load<Texture2D>("Challenge/7done");
                    Asset.challenge8completed = Content.Load<Texture2D>("Challenge/8done");
                    Asset.challenge9completed = Content.Load<Texture2D>("Challenge/9done");
                    Asset.challenge10completed = Content.Load<Texture2D>("Challenge/10done");
                    Asset.challenge11completed = Content.Load<Texture2D>("Challenge/11done");
                    Asset.challenge12completed = Content.Load<Texture2D>("Challenge/12done");
                    Asset.challenge13completed = Content.Load<Texture2D>("Challenge/13done");

                    Asset.ChallengeBacknormal = Content.Load<Texture2D>("Challenge/backButton _ customized");
                    Asset.ChallengeBackHover = Content.Load<Texture2D>("Challenge/backButton _ customized_hover");

                    //Map Scene
                    Asset.backgroundMap1_0 = Content.Load<Texture2D>("Maps/Map2Background/blue1");
                    Asset.backgroundMap1_1 = Content.Load<Texture2D>("Maps/Map2Background/blue2");
                    Asset.backgroundMap1_2 = Content.Load<Texture2D>("Maps/Map2Background/blue3");
                    Asset.backgroundMap1_3 = Content.Load<Texture2D>("Maps/Map2Background/blue4");
                    Asset.backgroundMap1_4 = Content.Load<Texture2D>("Maps/Map2Background/blue5");
                    Asset.backgroundMap1_5 = Content.Load<Texture2D>("Maps/Map2Background/blue6");
                    Asset.backgroundMap1_6 = Content.Load<Texture2D>("Maps/Map2Background/blue7");
                    Asset.backgroundMap1_7 = Content.Load<Texture2D>("Maps/Map2Background/blue8");
                    Asset.backgroundMap1_8 = Content.Load<Texture2D>("Maps/Map2Background/blueComplete");

                    Asset.backgroundMap2_0 = Content.Load<Texture2D>("Maps/Map1Background/green1");
                    Asset.backgroundMap2_1 = Content.Load<Texture2D>("Maps/Map1Background/green2");
                    Asset.backgroundMap2_2 = Content.Load<Texture2D>("Maps/Map1Background/green3");
                    Asset.backgroundMap2_3 = Content.Load<Texture2D>("Maps/Map1Background/green4");
                    Asset.backgroundMap2_4 = Content.Load<Texture2D>("Maps/Map1Background/green5");
                    Asset.backgroundMap2_5 = Content.Load<Texture2D>("Maps/Map1Background/green6");
                    Asset.backgroundMap2_6 = Content.Load<Texture2D>("Maps/Map1Background/green7");
                    Asset.backgroundMap2_7 = Content.Load<Texture2D>("Maps/Map1Background/greenComplete");
                    
                    Asset.star = Content.Load<Texture2D>("Maps/star");

                    // Live Tile
                    Asset.selectedTile = Content.Load<Texture2D>("ElementBoost/selectedTile");

                    Asset.balanceTile = Content.Load<Texture2D>("ElementBoost/LiveTile/balance");
                    Asset.thornTile = Content.Load<Texture2D>("ElementBoost/LiveTile/thorn");
                    Asset.sealTile = Content.Load<Texture2D>("ElementBoost/LiveTile/seal");
                    Asset.fireTile = Content.Load<Texture2D>("ElementBoost/LiveTile/fire");

                    Asset.speedTile = Content.Load<Texture2D>("ElementBoost/LiveTile/speed");
                    Asset.bombTile = Content.Load<Texture2D>("ElementBoost/LiveTile/bomb");
                    Asset.swordTile = Content.Load<Texture2D>("ElementBoost/LiveTile/sword");
                    Asset.phoenixTile = Content.Load<Texture2D>("ElementBoost/LiveTile/phoenix");

                    Asset.angryTile = Content.Load<Texture2D>("ElementBoost/LiveTile/angry");
                    Asset.cutterTile = Content.Load<Texture2D>("ElementBoost/LiveTile/cutter");
                    Asset.iceFangTile = Content.Load<Texture2D>("ElementBoost/LiveTile/iceFang");
                    Asset.slashTile = Content.Load<Texture2D>("ElementBoost/LiveTile/slash");

                    Asset.healTile = Content.Load<Texture2D>("ElementBoost/LiveTile/heal");
                    Asset.hornTile = Content.Load<Texture2D>("ElementBoost/LiveTile/horn");
                    Asset.herbTile = Content.Load<Texture2D>("ElementBoost/LiveTile/herb");

                    Asset.shieldTile = Content.Load<Texture2D>("ElementBoost/LiveTile/shield");
                    Asset.waveTile = Content.Load<Texture2D>("ElementBoost/LiveTile/wave");
                    Asset.steamTile = Content.Load<Texture2D>("ElementBoost/LiveTile/steam");

                    //BuildTeamScene.
                    Asset.bottomHalf = Content.Load<Texture2D>("BuildTeam/vung giua/bottomHalf");
                    Asset.topHalf = Content.Load<Texture2D>("BuildTeam/vung giua/topHalf");
                    Asset.middlePart = Content.Load<Texture2D>("BuildTeam/vung giua/middlePart");
                    Asset.positionI = Content.Load<Texture2D>("BuildTeam/vung giua/position1empty");
                    Asset.positionII = Content.Load<Texture2D>("BuildTeam/vung giua/position2empty");
                    Asset.positionIII = Content.Load<Texture2D>("BuildTeam/vung giua/position3empty");
                    Asset.positionFull = Content.Load<Texture2D>("BuildTeam/vung giua/positionFull");
                    Asset.positionHalfFull = Content.Load<Texture2D>("BuildTeam/vung giua/positionHalfFull");

                    Asset.card1_lv1 = Content.Load<Texture2D>("BuildTeam/canh trai/demo1");
                    Asset.card1_lv2 = Content.Load<Texture2D>("BuildTeam/canh trai/demo2");
                    Asset.card1_lv3 = Content.Load<Texture2D>("BuildTeam/canh trai/demo3");

                    Asset.card2_lv1 = Content.Load<Texture2D>("BuildTeam/canh trai/ray1");
                    Asset.card2_lv2 = Content.Load<Texture2D>("BuildTeam/canh trai/ray2");
                    Asset.card2_lv3 = Content.Load<Texture2D>("BuildTeam/canh trai/ray3");

                    Asset.card3_lv1 = Content.Load<Texture2D>("BuildTeam/canh trai/crusher1");
                    Asset.card3_lv2 = Content.Load<Texture2D>("BuildTeam/canh trai/crusher2");
                    Asset.card3_lv3 = Content.Load<Texture2D>("BuildTeam/canh trai/crusher3");

                    Asset.card4_lv1 = Content.Load<Texture2D>("BuildTeam/canh trai/soul1");
                    Asset.card4_lv2 = Content.Load<Texture2D>("BuildTeam/canh trai/soul2");
                    Asset.card4_lv3 = Content.Load<Texture2D>("BuildTeam/canh trai/soul3");

                    Asset.card5_lv1 = Content.Load<Texture2D>("BuildTeam/canh trai/george1");
                    Asset.card5_lv2 = Content.Load<Texture2D>("BuildTeam/canh trai/george2");
                    Asset.card5_lv3 = Content.Load<Texture2D>("BuildTeam/canh trai/george3");

                    GlobalClass.percentage = 90;

                }
                else if (GlobalClass.percentage == 90)
                {

                    Asset.blankCardRobo = Content.Load<Texture2D>("BuildTeam/canh trai/blank card");
                    Asset.emptyPosRobo = Content.Load<Texture2D>("BuildTeam/canh trai/emptyPosition");
                    Asset.leftBg = Content.Load<Texture2D>("BuildTeam/canh trai/leftBg");
                    Asset.textLeft = Content.Load<Texture2D>("BuildTeam/canh trai/textCyclebot");
                    Asset.upSymbolLeft = Content.Load<Texture2D>("BuildTeam/canh trai/upSymbol");
                    Asset.downSymbolLeft = Content.Load<Texture2D>("BuildTeam/canh trai/downSymbol");

                    Asset.megadroidCard = Content.Load<Texture2D>("BuildTeam/canh phai/megadroid");
                    Asset.gigadroidCard = Content.Load<Texture2D>("BuildTeam/canh phai/gigadroid");
                    Asset.defentantCard = Content.Load<Texture2D>("BuildTeam/canh phai/defentant");
                    Asset.steelKnightCard = Content.Load<Texture2D>("BuildTeam/canh phai/steelKnight");
                    Asset.thunderbolt1Card = Content.Load<Texture2D>("BuildTeam/canh phai/thunderbolt1");
                    Asset.thunderbolt2Card = Content.Load<Texture2D>("BuildTeam/canh phai/thunderbolt2");

                    Asset.blankCardAss = Content.Load<Texture2D>("BuildTeam/canh phai/blankCard");
                    Asset.emptyPosAss = Content.Load<Texture2D>("BuildTeam/canh phai/emptyPositionAss");
                    Asset.rightBg = Content.Load<Texture2D>("BuildTeam/canh phai/rightBackground");
                    Asset.textRight = Content.Load<Texture2D>("BuildTeam/canh phai/textAssistant");
                    Asset.upSymbolRight = Content.Load<Texture2D>("BuildTeam/canh phai/upSymbol");
                    Asset.downSymbolRight = Content.Load<Texture2D>("BuildTeam/canh phai/downSymbol");

                    Asset.backButtonAtBuildTeam = Content.Load<Texture2D>("BuildTeam/button/backButton");
                    Asset.backTextAtBuildTeam = Content.Load<Texture2D>("BuildTeam/button/backText");
                    Asset.startButtonAtBuildTeam = Content.Load<Texture2D>("BuildTeam/button/startButton");
                    Asset.startTextAtBuildTeam = Content.Load<Texture2D>("BuildTeam/button/startText");

                    Asset.bulletNum2 = Content.Load<Texture2D>("Animations/num2/num2_lvl3_ball");
                    Asset.bulletTrash6 = Content.Load<Texture2D>("Animations/trash/trash6_bullet");
                    Asset.bulletTrash10 = Content.Load<Texture2D>("Animations/trash/trash10_bullet");
                    Asset.bulletTrash11 = Content.Load<Texture2D>("Animations/trash/trash11_bullet");

                    Asset.healthTexture = Content.Load<Texture2D>("Icon/healthBar/health");

                    // skill lightball
                    Asset.lightball = Content.Load<Texture2D>("skills/lightball/bomb");
                    Asset.num2_cardLV1 = Content.Load<Texture2D>("skills/lightball/lvl1card");
                    Asset.num2_cardLV2 = Content.Load<Texture2D>("skills/lightball/lvl2card");
                    Asset.num2_cardLV3 = Content.Load<Texture2D>("skills/lightball/lvl3_card");

                    GlobalClass.percentage = 100;
                }
            }
        }
    }
}