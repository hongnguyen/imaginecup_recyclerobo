﻿using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class PhysicalBullet : Bullet
    {
        public Body body;
        public Vector2 limitPoint;
        public bool isTop = true, isDown = true;

        public PhysicalBullet(Texture2D texture, World world, Vector2 position, Vector2 target, Vector2 deltaPos, Vector2 limitPoint, int damage, SpriteEffects spriteEffect)
        :base (texture,position,target,damage)
        {
            this.spriteEffect = spriteEffect;
            if (target.Y < position.Y)
            {
                isDown = false;
                isTop = false;
            }
            this.limitPoint = limitPoint;
            body = new Body(world);
            body.BodyType = BodyType.Dynamic;
            body.Position = ConvertUnits.ToSimUnits(position);
            body.CreateFixture(new CircleShape(0.3f, 1f));
            Vector2 force = target + deltaPos - position;
            body.ApplyForce(new Vector2(force.X / 3.5f, force.Y / 3.5f));
            //Console.WriteLine(body.LinearVelocity);
        }

        public override void Update(GameTime gametime)
        {
            position = ConvertUnits.ToDisplayUnits(body.Position);
        }
    }
}
