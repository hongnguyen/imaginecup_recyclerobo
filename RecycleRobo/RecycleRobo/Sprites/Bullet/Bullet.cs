﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace RecycleRobo
{
    public class Bullet
    {
        public Texture2D texture;
        public Vector2 position;
        public Vector2 target;
        public float scale = 0.625f;
        public SpriteEffects spriteEffect = SpriteEffects.None;
        public int damage;

        public Bullet(Texture2D texture, Vector2 pos, Vector2 target, int damage)
        {
            this.texture = texture;
            this.position = pos;
            this.target = target;
            this.damage = damage;
        }

        public virtual void Update(GameTime gametime){}

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, null, Color.White, 0f, Vector2.Zero, 0.625f, spriteEffect, 0.2f);
        }

        public Rectangle getRectangle()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)(texture.Width * scale), (int)(texture.Height * scale));
        }
    }
}
