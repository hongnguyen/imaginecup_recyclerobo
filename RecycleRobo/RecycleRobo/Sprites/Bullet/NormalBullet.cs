﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class NormalBullet : Bullet
    {
        public Vector2 direction;
        public float distance;
        public int speed;
        public bool isOnScreen = true;

        public NormalBullet(Texture2D texture, Vector2 pos, Vector2 target, int damage, int speed)
        :base(texture,pos,target, damage)
        {
            this.speed = speed;
            this.direction = target - pos;
            this.direction.Normalize();
            this.distance = Vector2.Distance(target, pos);
        }

        public override void Update(GameTime gametime)
        {
            position.X += (float)(direction.X * speed * distance * gametime.ElapsedGameTime.TotalSeconds);
            position.Y += (float)(direction.Y * speed * distance * gametime.ElapsedGameTime.TotalSeconds);

            if ( position.X < 0 || position.X > GlobalClass.ScreenWidth || position.Y < 0 || position.Y > GlobalClass.ScreenHeight)
            {
                isOnScreen = false;
            }
        }
    }
}
