﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;

namespace RecycleRobo
{
    public class Background : SpriteManager
    {
        float layer;
        public Background(ContentManager Content, String filename, float orderLayer)
            : base(Content, filename)
        {
            Position = Vector2.Zero;
            Scale = GlobalClass.ScreenWidth / Texture.Width;
            layer = orderLayer;
        }
        public Background(Texture2D tex, float orderLayer)
            : base(tex)
        {
            Position = Vector2.Zero;
            Scale = GlobalClass.ScreenWidth / Texture.Width;
            layer = orderLayer;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(Texture, Position, null, Color, Rotation, 
            //    Origin, Scale, SpriteEffect, layer);
            spriteBatch.Draw(Texture, Position, new Rectangle(0,0,1920,1080), Color, Rotation,
                Origin, 0.625f, SpriteEffect, layer);
            base.Draw(spriteBatch);
        }
    }
}
