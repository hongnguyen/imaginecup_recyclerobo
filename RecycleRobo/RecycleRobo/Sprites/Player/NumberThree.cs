﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;
using LoadData;

namespace RecycleRobo
{
    public class NumberThree : Player
    {
        public NumberThree(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
        : base (Content, initialPos, assistant, game)
        {
            soundAttack = game.Content.Load<SoundEffect>("sounds/_number3Attack");
            healthBar = Content.Load<Texture2D>("Icon/healthbar/healthBar3");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByNum3");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
            changeStatsRobo();
        }

        public override void loadStats()
        {
            upgradeData = loadData.Load<NumberThreeUpgrade>("Player/NumberThree.xml");
            baseData = game.Content.Load<NumberThreeBase>("Data\\Player\\NumberThree");
            base.loadStats();

            levelOfRobot = upgradeData.level;

            NumberThreeBase tmp = (NumberThreeBase)baseData;
            damage = staticDamage = (int)(tmp.damage * (float)Math.Pow(tmp.coefficientDamage, upgradeData.level));

            if (getLevelOfEvolution() == 1) sprite.FrameToAction = 12;
            else if (getLevelOfEvolution() == 2) sprite.FrameToAction = 15;
            else sprite.FrameToAction = 16;
        }

        public override void initCurrentForm()
        {
            base.initCurrentForm();
            currentForm = new AngerBearForm(formsData.ElementAt(0).bonus);
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(curAnimation.FrameWidth * sprite.Scale)-10, (int)(sprite.Scale * curAnimation.FrameHeight-10));
        }

        public override Rectangle getRectangleWithBullet()
        {
            Rectangle tmp = base.getRectangleWithBullet();
            if (getLevelOfEvolution() == 1)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 40, tmp.Y + 30, tmp.Width - 80, tmp.Height - 35);
                else
                    return new Rectangle(tmp.X + 70, tmp.Y + 30, tmp.Width - 80, tmp.Height - 35);
            }
             else if (getLevelOfEvolution()==2)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 55, tmp.Y + 20, tmp.Width - 120, tmp.Height - 15);
                else
                    return new Rectangle(tmp.X + 70, tmp.Y + 20, tmp.Width - 120, tmp.Height - 15);
            }
            else
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 40, tmp.Y + 20, tmp.Width - 80, tmp.Height - 15);
                else
                    return new Rectangle(tmp.X + 40, tmp.Y + 20, tmp.Width - 80, tmp.Height - 15);
            }
            
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            if (sprite.SpriteEffect == SpriteEffects.None)
            {
                if (getLevelOfEvolution() == 1)
                    return new Rectangle(tmp.X + 15, tmp.Y, (int)(tmp.Width / 2), tmp.Height);
                else if (getLevelOfEvolution() == 2)
                    return new Rectangle(tmp.X + 25, tmp.Y - 20, tmp.Width - 80, tmp.Height);
                else
                    return new Rectangle(tmp.X + 20, tmp.Y, tmp.Width - 50, tmp.Height);
            }
            else
            {
                if (getLevelOfEvolution() == 1)
                    return new Rectangle(tmp.X + 45, tmp.Y, (int)(tmp.Width / 2), tmp.Height);
                else if (getLevelOfEvolution() == 2)
                    return new Rectangle(tmp.X + 45, tmp.Y - 20, tmp.Width - 80, tmp.Height);
                else
                    return new Rectangle(tmp.X + 20, tmp.Y, tmp.Width - 50, tmp.Height);
            }
        }

        public override void setIdleAnimation()
        {
            String key = "NUMBER3_IDLE_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setWalkingAnimation()
        {
            string key = "NUMBER3_WALK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setDeathAnimation()
        {
            string key = "NUMBER3_DIE_" + getLevelOfEvolution();
            curAnimation = GlobalClass.getAnimationByName(key);
        }

        public override void setAttackAnimation()
        {
            string key = "NUMBER3_ATTACK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
            sprite.PlayAnimation(curAnimation);
        }

        public override Texture2D getTexture()
        {
            return game.Content.Load<Texture2D>("Icon2/num3_icon");
        }
        public override void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            Vector2 pos = new Vector2(Center.X - currentForm.areaAnimation.FrameWidth * areaAnimationPlayer.Scale / 2, Center.Y + 20);
            areaAnimationPlayer.PlayAnimation(currentForm.areaAnimation);

            Vector2 tmpAreaPos = Vector2.Zero;

            if (sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
            {
                if (getLevelOfEvolution() == 3) tmpAreaPos = new Vector2(-10, -6);
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + new Vector2(25, 0) + tmpAreaPos);
            }
            else
            {
                if (getLevelOfEvolution() == 3) tmpAreaPos = new Vector2(5, -3);
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + tmpAreaPos);
            }
        }

        public override Vector2 getCirclePosition()
        {
            return new Vector2(Position.X - 90, Position.Y - 80);
        }
        public override string getPathFormDataFile()
        {
            return "RobotForm/NumberThree.xml";
        }

        public override int getLevelOfEvolution()
        {
            if (levelOfRobot < 12)
            {
                return 1;
            }
            else if (levelOfRobot < 18)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }

        public override int getNumberOfForm()
        {
            if (levelOfRobot < 12) return 1;
            else if (levelOfRobot < 14) return 2;
            else if (levelOfRobot < 18) return 3;
            else return 4;
        }
    }
}
