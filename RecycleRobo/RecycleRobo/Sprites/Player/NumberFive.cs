﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;
using LoadData;

namespace RecycleRobo
{
    // Tan cong tu xa
    public class NumberFive : Ranger
    {
        public NumberFive(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
            : base(Content, initialPos, assistant, game) 
        {
            sprite.FrameToAction = 18;
            soundAttack = game.Content.Load<SoundEffect>("sounds/_number5Attack");
            healthBar = Content.Load<Texture2D>("Icon/healthbar/healthBar5");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByNum5");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
            changeStatsRobo();
        }

        public override void loadStats()
        {
            upgradeData = loadData.Load<NumberFiveUpgrade>("Player/NumberFive.xml");
            baseData = game.Content.Load<NumberFiveBase>("Data\\Player\\NumberFive");
            base.loadStats();
            NumberFiveBase tmp = (NumberFiveBase)baseData;
            damage = staticDamage = (int)(tmp.damage * (float)Math.Pow(tmp.coefficientDamage, upgradeData.level));
            range = tmp.range * (float)Math.Pow(tmp.coefficientRange, upgradeData.level);
            
            levelOfRobot = upgradeData.level;
        }

        public override void initCurrentForm()
        {
            base.initCurrentForm();
            currentForm = new GreenShieldForm(formsData.ElementAt(0).bonus);
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(curAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * curAnimation.FrameHeight));
        }

        public override Rectangle getRectangleWithBullet()
        {
            Rectangle tmp =  base.getRectangleWithBullet();
            return new Rectangle(tmp.X + 50, tmp.Y+25, tmp.Width - 80, tmp.Height-10);
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp =  base.getAttackRectangle();
            return new Rectangle(tmp.X + 30, tmp.Y, (int)(tmp.Width / 2), (int)(tmp.Height / 2));
        }

        public override void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            Vector2 pos = new Vector2(Center.X - currentForm.areaAnimation.FrameWidth * areaAnimationPlayer.Scale / 2, Center.Y);
            areaAnimationPlayer.PlayAnimation(currentForm.areaAnimation);

            Vector2 tmpAreaPos = Vector2.Zero;
            if (getLevelOfEvolution() == 2) tmpAreaPos = new Vector2(0, -5);
            else if (getLevelOfEvolution() == 3) tmpAreaPos = new Vector2(2, 25);

            if (sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + new Vector2(10,0) + tmpAreaPos);
            else
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + tmpAreaPos);
        }

        public override void setIdleAnimation()
        {
            string key = "NUMBER5_IDLE_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setWalkingAnimation()
        {
            string key = "NUMBER5_WALK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setDeathAnimation()
        {
            string key = "NUMBER5_DIE_" + getLevelOfEvolution();
            curAnimation = GlobalClass.getAnimationByName(key);
        }

        public override void setAttackAnimation()
        {
            string key = "NUMBER5_ATTACK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
            sprite.PlayAnimation(curAnimation);
        }

        public override Texture2D getTexture()
        {
            return game.Content.Load<Texture2D>("Icon2/num5_icon");
        }

        public override Vector2 getCirclePosition()
        {
            return new Vector2(Position.X - 90, Position.Y - 110);
        }

        public override string getPathFormDataFile()
        {
            return "RobotForm/NumberFive.xml";
        }

        public override int getLevelOfEvolution()
        {
            if (levelOfRobot < 15) return 1;
            else if (levelOfRobot < 20) return 2;
            else return 3;
        }

        public override int getNumberOfForm()
        {
            if (levelOfRobot < 10) return 1;
            else if (levelOfRobot < 15) return 2;
            else if (levelOfRobot < 20) return 3;
            else return 4;
        }
    }
}
