﻿using LoadData;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectMercury;
using RecycleRobo.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class BulletBoss2
    {
        public AnimationPlayer bulletPlayer;
        public Vector2 position;
        public Vector2 direction;
        public float distance;
        public int speed;
        public bool isOnScreen = true;
        public int damage;
        private float rotate;
        private Animation bullet;

        public BulletBoss2(Animation bullet, Vector2 pos, Vector2 target, int damage, int speed, float rotate, float layer)
        {
            this.bullet = bullet;
            bulletPlayer.color = Color.White;
            bulletPlayer.Scale = 0.48f;
            bulletPlayer.OrderLayer = layer;
            bulletPlayer.PlayAnimation(bullet);
            this.rotate = rotate;
            this.speed = speed;
            this.damage = damage;
            this.position = pos;
            this.direction = target - pos;
            this.direction.Normalize();
            this.distance = Vector2.Distance(target, pos);
        }

        public void update(GameTime gameTime)
        {
            position.X += (float)(direction.X * speed * distance * gameTime.ElapsedGameTime.TotalSeconds);
            position.Y += (float)(direction.Y * speed * distance * gameTime.ElapsedGameTime.TotalSeconds);

            if (position.X < 0 || position.X > GlobalClass.ScreenWidth || position.Y < 0 || position.Y > GlobalClass.ScreenHeight)
            {
                isOnScreen = false;
            }
        }

        public bool isCorrect(Player[] player, ParticleEffect effect)
        {
            foreach (Player robo in player)
            {
                if (robo.curHealth <= 0 && !robo.isVisible && !robo.isControl) continue;
                if (Vector2.Distance(position, robo.Center) <= 5 || getBound().Intersects(robo.getRectangleWithBullet()))
                {
                    robo.curHealth -= damage;
                    effect.Trigger(robo.Center);
                    effect.Trigger(robo.Center);
                    effect.Trigger(robo.Center);
                    effect.Trigger(robo.Center);
                    effect.Trigger(robo.Center);
                    return true;
                }
            }
            return false;
        }

        public void draw(GameTime gametime, SpriteBatch spritebatch)
        {
            bulletPlayer.DrawRotate(gametime, spritebatch, position, rotate);
        }

        public Rectangle getBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)(bullet.FrameWidth * bulletPlayer.Scale - 30), (int)(bullet.FrameHeight * bulletPlayer.Scale));
        }
    }

    public class BossTwo : Monster
    {
        public enum SmokeState
        {
            APPEAR,
            DISAPPEAR,
            NONE
        }

        public enum Boss2State
        {
            STATE1,
            STATE2,
            STATE3,
            TMP,
        }

        public SmokeState smokeState = SmokeState.APPEAR;
        public Boss2State boss2State = Boss2State.STATE1;

        public Animation appear;
        public Animation disaapear;
        public AnimationPlayer smokeAnimationPlayer;

        public Animation gas;
        public AnimationPlayer effectAnimationPlayer;

        private int robot = -10;
        private Random rd = new Random(3000);

        private int count = 1;

        public List<BulletBoss2> bullets = new List<BulletBoss2>();
        private float timeCountDown = 0;
        public Monster[] finalWave;

        //bien danh cho can bang
        private int damageBulletATState2 = 100;
        private float state1Time = 7000;
        private float state2Time = 13000;

        private SoundEffect soundState1;

        public BossTwo(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player) 
        {
            Position = Vector2.Zero;
            initBoss();
            sprite.FrameToAction = 12;
            isVisible = false;
            appear = Asset.appear;
            disaapear = Asset.disappear;
            smokeAnimationPlayer.color = Color.White;
            smokeAnimationPlayer.SpriteEffect = SpriteEffects.None;
            smokeAnimationPlayer.Scale = 0.625f;

            CurAnimation = Asset.boss2_attack1;

            gas = Asset.gas;
            effectAnimationPlayer.color = Color.White;
            effectAnimationPlayer.SpriteEffect = SpriteEffects.None;
            effectAnimationPlayer.Scale = 0.625f;

            //sound. Boss co the phai can them sound
            soundState1 = Content.Load<SoundEffect>("Animations/trash/boss2/boss2_attack");
            soundAttack = Content.Load<SoundEffect>("Animations/trash/boss2/boss2_attack_bullet");
            soundDie = Content.Load<SoundEffect>("Animations/trash/boss2/boss2_die");

            particleEffect = Content.Load<ParticleEffect>("Animations/trash/boss2/boss2bullet");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
        }

        public void initBoss()
        {
            Player robo = getNearestRobot();
            Position = robo.Position;
            curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
        }

        public override Vector2 Center
        {
            get 
            {
                return (Position + new Vector2(180, 180));
            }
            set { }
        }


        public void loadWave()
        {
            SubWave waveData = Content.Load<SubWave>("Data/Map/Map2/Boss2Wave");
            Monster[] childMonster = new Monster[waveData.totalMonster];
            new LoadCharacter(game).Monster(childMonster, player, waveData.monsters);

            finalWave = new Monster[waveData.totalMonster + 1];

            finalWave[0] = GamePlayScene.waveManager.wave.monsters[0];

            for (int i = 0; i < childMonster.Length; i++)
            {
                finalWave[1 + i] = childMonster[i];
            }

            GamePlayScene.waveManager.wave.monsters = finalWave;
        }


        public override void updatePosition(GameTime gameTime)
        {
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);

            if (!isAttackable)
            {
                particleEffect = Content.Load<ParticleEffect>("Animations/trash/boss2/boss2_die_Effect");
                particleEffect.LoadContent(Content);
                particleEffect.Initialise();

                int x = 100;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));

                x = 80;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));

                x = 40;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));
            }

            if (isAttackable)
            {
                if (smokeState == SmokeState.APPEAR)
                {
                    if (smokeAnimationPlayer.IsDone)
                    {
                        if (count % 6 == 1)
                        {
                            sprite.FrameToAction = 12;
                            boss2State = Boss2State.STATE1;
                            CurAnimation = Asset.boss2_attack1;
                            isVisible = true;
                        }
                        else if (count % 6 == 3)
                        {
                            CurAnimation = Asset.boss2_attack2;
                            sprite.FrameToAction = 7;
                            boss2State = Boss2State.STATE2;
                            isVisible = true;
                        }
                        else if (count % 2 == 0) isVisible = false;

                        count++;
                        smokeState = SmokeState.DISAPPEAR;
                    }
                }
                else if (smokeState == SmokeState.DISAPPEAR)
                {
                    if (smokeAnimationPlayer.IsDone)
                    {
                        smokeState = SmokeState.NONE;
                    }
                }
            }

            if (boss2State == Boss2State.STATE1 && isAttackable)
            {
                effectAnimationPlayer.PlayAnimation(gas);
                if (sprite.isFrameToAction)
                {
                    foreach (Player robo in player)
                    {
                        if (robo.curHealth <= 0 || !robo.isControl || !robo.isVisible) continue;

                        if (Vector2.Distance(robo.Center, Center) <= 370)
                        {
                            robo.curHealth -= damage;
                            soundState1.Play();
                            particleEffect.Trigger(robo.Center + new Vector2(0, -20));
                            particleEffect.Trigger(robo.Center + new Vector2(0, -20));
                            particleEffect.Trigger(robo.Center + new Vector2(0, -20));
                        }
                    }

                    sprite.isFrameToAction = false;
                }

                timeCountDown += gameTime.ElapsedGameTime.Milliseconds;
                if (timeCountDown >= state1Time)
                {
                    timeCountDown = 0f;
                    smokeState = SmokeState.APPEAR;
                    boss2State = Boss2State.TMP;
                }
            }
            else if (boss2State == Boss2State.STATE2 && isAttackable)
            {
                Player r = getRobo();
                if (r == null || !r.isVisible) return;

                if (sprite.isFrameToAction)
                {
                    Vector2 posBullet = Position + new Vector2(160, 140);
                    double x = r.Center.X - posBullet.X;
                    double y = (r.Center.Y - posBullet.Y);
                    float angle = (float)Math.Atan(y / x);
                    BulletBoss2 bullet = new BulletBoss2(Asset.boss2_bullet, posBullet, r.Center, damageBulletATState2, 2, angle, 0.2f);
                    bullets.Add(bullet);
                    sprite.isFrameToAction = false;
                }

                foreach (BulletBoss2 bullet in bullets.ToArray())
                {
                    bullet.update(gameTime);
                    if (bullet.isCorrect(player, particleEffect))
                    {
                        soundAttack.Play();
                        bullets.Remove(bullet);
                    }
                        
                }

                timeCountDown += gameTime.ElapsedGameTime.Milliseconds;
                if (timeCountDown >= state2Time)
                {
                    timeCountDown = 0f;
                    smokeState = SmokeState.APPEAR;
                    boss2State = Boss2State.TMP;
                }
            }
            else if (boss2State == Boss2State.STATE3)
            {
                if (isAttackable)
                {
                    if (isChildDeadAll())
                    {
                        count++;
                        smokeState = SmokeState.APPEAR;
                        boss2State = Boss2State.TMP;
                    }
                }
                else
                {
                    setAllChildDead();
                }
            }

            if (smokeState == SmokeState.NONE && boss2State == Boss2State.TMP)
            {
                timeCountDown += gameTime.ElapsedGameTime.Milliseconds;
                if (timeCountDown >= 1500)
                {

                    if (count % 6 == 1)
                    {
                        Player robo = getNearestRobot();
                        if (robo == null) return;
                        Position = robo.Position;
                        curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
                    }
                    else if (count % 6 == 3)
                    {
                        int x = rd.Next(0, 100);
                        int y = rd.Next(-10, (int)(GlobalClass.ScreenHeight - CurAnimation.FrameHeight * sprite.Scale));
                        Position = new Vector2(x, y);
                    }
                    else if (count % 6 == 5)
                    {
                        int x = rd.Next(0, (int)(GlobalClass.ScreenWidth - CurAnimation.FrameWidth * sprite.Scale));
                        int y = rd.Next(-10, (int)(GlobalClass.ScreenHeight - CurAnimation.FrameHeight * sprite.Scale));
                        Position = new Vector2(x, y);
                        count++;
                        CurAnimation = Asset.boss2_attack1;
                        loadWave();
                        boss2State = Boss2State.STATE3;
                        isVisible = false;
                        return;
                    }

                    timeCountDown = 0;
                    smokeState = SmokeState.APPEAR;
                }
            }
        }

        public bool isChildDeadAll()
        {
            for (int i = 1; i < finalWave.Length; i++)
            {
                if (finalWave[i].isAttackable) return false;
            }
            return true;
        }

        public void setAllChildDead()
        {
            for (int i = 1; i < finalWave.Length; i++)
            {
                finalWave[i].curHealth = -1;
            }
        }

        public override Rectangle getRectangle()
        {
            return getAttackRectangle();
        }

        public override Rectangle getAttackRectangle()
        {
            return new Rectangle((int)Position.X + 120, (int)Position.Y + 130,
                (int)(CurAnimation.FrameWidth * sprite.Scale - 120), (int)(sprite.Scale * CurAnimation.FrameHeight-100));
        }

        public Rectangle getBound()
        {
            return new Rectangle((int)Position.X, (int)Position.Y,
               (int)(CurAnimation.FrameWidth * sprite.Scale + 150), (int)(sprite.Scale * CurAnimation.FrameHeight + 150));
        }

        public override void setAttackAnimation(){}
        public override void setWalkAnimation(){}

        public override void setDeathAnimation()
        {
            if (boss2State == Boss2State.STATE1 || boss2State == Boss2State.STATE3)
                CurAnimation = Asset.boss2_die1;
            else
                CurAnimation = Asset.boss2_die2;
        }

        public override void createSewage(List<Item> sewages)
        {
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(Asset.healthTexture, getAttackRectangle(), Color.White);
            //spriteBatch.Draw(Asset.healthTexture, getBound(), Color.Red);
            //spriteBatch.Draw(Asset.healthTexture, getRectangle(), Color.Green);


            if (isAlive)
            {

                if (isAttackable && isVisible)
                {
                    Vector2 pos = new Vector2((int)(Position.X + 15), (int)(Position.Y - 20));

                    spriteBatch.Draw(healthbar, new Rectangle((int)(pos.X), (int)pos.Y, (int)(healthbar.Width * 5), (int)(healthbar.Height * 3)),
                        null, Color.LightGray, 0f, Vector2.Zero, SpriteEffects.None, 0.17f);

                    spriteBatch.Draw(healthbar, new Rectangle((int)(pos.X), (int)pos.Y,
                                (int)(healthbar.Width * 5 * (double)curHealth / Health), healthbar.Height * 3),
                                null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);
                }

                if (isVisible)
                {
                    Vector2 tmp = new Vector2(60, 60);
                    if (boss2State == Boss2State.STATE1)
                    {
                        effectAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(-20, 158));
                    }

                    sprite.OrderLayer = curOrderLayer;
                    sprite.PlayAnimation(CurAnimation);
                    sprite.Draw(gameTime, spriteBatch, Position + tmp);

                }

                if (boss2State == Boss2State.STATE2 && enemy != null && isAttackable)
                {
                    foreach (BulletBoss2 bullet in bullets)
                    {
                        bullet.draw(gameTime, spriteBatch);
                    }
                }

                if (smokeState != SmokeState.NONE)
                {
                    smokeAnimationPlayer.OrderLayer = curOrderLayer;
                    if (smokeState == SmokeState.APPEAR) smokeAnimationPlayer.PlayAnimation(appear);
                    else if (smokeState == SmokeState.DISAPPEAR) smokeAnimationPlayer.PlayAnimation(disaapear);
                    smokeAnimationPlayer.Draw(gameTime, spriteBatch, Position);
                }
            }
        }


        public override Player getNearestRobot()
        {
            int check = 0;
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i].curHealth <= 0 || !player[i].isVisible || !player[i].isControl) check++;
                else break;
                if (check == player.Length) return null;
            }

            if (robot >= 0 && player[robot].curHealth > 0)
            {
                return player[robot];
            }

            robot = rd.Next(0, player.Length);

            while (player[robot].curHealth <= 0)
                robot = rd.Next(0, player.Length);

            return player[robot];
        }

        public Player getRobo()
        {
            float minLength = 100000;
            Player r = null;
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i] == null || !player[i].isVisible || !player[i].isControl || player[i].curHealth <= 0) continue;
                Vector2 dis = player[i].Position - Position;
                if (dis.Length() < minLength)
                {
                    r = player[i];
                    minLength = dis.Length();
                }
            }
            this.enemy = r;
            return r;
        }
    }
}
