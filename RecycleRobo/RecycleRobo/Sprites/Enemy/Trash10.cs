﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class Trash10 : RangMonster
    {
        public Trash10(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player)
        {
            range = 450;
            sprite.FrameToAction = 9;
            bulletTexture = Asset.bulletTrash10;

            soundReleaseBullet = Content.Load<SoundEffect>("sounds/trash/Trash10_attack");
            soundAttack = Content.Load<SoundEffect>("sounds/trash/Trash10_attack");
            soundDie = Content.Load<SoundEffect>("sounds/trash/Trash10_die"); 
            
            sewagesTexture = Content.Load<Texture2D>("Items/trash10_die");
            particleEffect = Content.Load<ParticleEffect>("Animations/trash/boss2/boss2bullet");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
        }

        public override void setWalkAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH10_WALK");
        }

        public override void setAttackAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH10_ATTACK");
            sprite.PlayAnimation(CurAnimation);
        }

        public override void setDeathAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH10_DIE");
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(CurAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * CurAnimation.FrameHeight));
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            return new Rectangle(tmp.X+30, tmp.Y-20, (int)(tmp.Width/2), tmp.Height);
        }

        public override Bullet createBullet(Vector2 target, Vector2 limitPoint)
        {
            Vector2 pos = Vector2.Zero;
            if (sprite.SpriteEffect == SpriteEffects.None)
                pos = Center + new Vector2(40, -50);
            else
                pos = Center + new Vector2(-50, -50);

            return new NormalBullet(bulletTexture, pos, target, damage, 2);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable)
            {
                Vector2 pos = new Vector2((int)(Position.X + 60), (int)Position.Y + 30);
                spriteBatch.Draw(healthbar, pos, null, Color.LightGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.17f);
                spriteBatch.Draw(healthbar, new Rectangle((int)pos.X, (int)pos.Y, (int)(healthbar.Width * (double)curHealth / Health), 4), 
                    null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);
            }
        }
    }
}
