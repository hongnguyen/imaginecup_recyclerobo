﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Collision.Shapes;

namespace RecycleRobo
{
    public abstract class RangMonster : Monster
    {

        public List<Bullet> bullets;
        public int range;
        public World world;
        public Texture2D bulletTexture;
        public SoundEffect soundReleaseBullet;

        public RangMonster(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player)
        {
            bullets = new List<Bullet>();
            world = new World(new Vector2(0, 10));
        }


        public override void updatePosition(GameTime gameTime)
        {
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);

            if (isAttackable)
            {
                updateEmotion(gameTime);
                Player r = getNearestRobot();
                if (!isAttack)
                    for (int i = 0; i < player.Length; i++)
                    {
                        if (player[i] == null || !player[i].isVisible || !player[i].isControl) continue;
                        if (Monster.ReferenceEquals(player[i].Enemy, this))
                        {
                            r = player[i];
                            break;
                        }
                    }
                if (r == null || !r.isVisible || r.curHealth <= 0)
                {
                    setWalkAnimation();
                    return;
                }

                updateBullets(gameTime,r);

                if ((r.Center.X - Center.X) < 2)
                {
                    sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                }
                else if (r.Center.X < Center.X)
                {
                    sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                }
                else
                {
                    sprite.SpriteEffect = SpriteEffects.None;
                }

                if (Vector2.Distance(r.Center, Center) <= range && r.isVisible
                    && 30 < this.Center.X && this.Center.X < GlobalClass.ScreenWidth - 30
                && 140 < this.Center.Y && this.Center.Y < GlobalClass.ScreenHeight - 30)
                {
                    //isCollide = true;
                    setAttackAnimation();
                    if (sprite.isFrameToAction)
                    {
                        soundReleaseBullet.Play();
                        Vector2 limitPoint = r.Center + new Vector2(0, r.CurAnimation.FrameHeight * r.sprite.Scale / 2f);
                        bullets.Add(createBullet(r.Center, limitPoint));
                        sprite.isFrameToAction = false;
                    }
                    return;
                }
                else
                {
                    isAttack = false;
                    isCollide = false;
                    setWalkAnimation();
                }

                MoveTo(r.Center, speed, gameTime);

                curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
            }
        }

        public void updateBullets(GameTime gameTime, Player r)
        {
            world.Step(Math.Min((float)gameTime.ElapsedGameTime.TotalSeconds, (1f / 30f)));
            
            foreach (Bullet bullet in bullets.ToArray())
            {
                bullet.Update(gameTime);
                foreach (Player robo in player)
                {
                    if (robo.curHealth <= 0) continue;
                    if (bullet.getRectangle().Intersects(robo.getRectangleWithBullet())
                        || Vector2.Distance(bullet.position, r.Center) <= 5)
                    {
                        robo.curHealth -= bullet.damage;
                        soundAttack.Play();
                        
                        Vector2 deltaPos = new Vector2 (10,15);
                        if (sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
                            deltaPos = new Vector2(-10, 15);
                        
                        Vector2 posTrigger = bullet.position + deltaPos;

                        particleEffect.Trigger(posTrigger);
                        particleEffect.Trigger(posTrigger);
                        bullets.Remove(bullet);
                    }
                }
                


                if (bullet.GetType().Name == "PhysicalBullet")
                {
                    PhysicalBullet bulletTMP = (PhysicalBullet)bullet;
                    if (!bulletTMP.isDown)
                    {
                        if (bulletTMP.position.Y < r.Center.Y - 6)
                        {
                            bulletTMP.isDown = true;
                        }
                    }

                    if (bulletTMP.position.Y >= bulletTMP.limitPoint.Y && bulletTMP.isDown)
                    {
                        bullets.Remove(bulletTMP);
                    }
                }
                else
                {
                    NormalBullet bulletTMP = (NormalBullet)bullet;
                    if (!bulletTMP.isOnScreen)
                    {
                        bullets.Remove(bulletTMP);
                    }
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable && enemy!=null)
            {
                foreach (Bullet bullet in bullets.ToArray())
                {
                    bullet.Draw(spriteBatch);
                }
            }
        }

        public abstract Bullet createBullet(Vector2 target, Vector2 limitPoint);
    }
}
