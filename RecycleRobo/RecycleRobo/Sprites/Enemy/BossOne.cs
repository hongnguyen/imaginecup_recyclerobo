﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using LoadData;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class BossOne : Monster
    {

        enum BossOneState
        {
            STATE1,
            STATE2,
            STATE3
        }
        BossOneState bossState;

        //bien danh cho can bang
        public int timeState1 = 14000;
        public int timeState2 = 8000;

        public int delaySwitchState = 0;
        public bool isCenter = false;
        public int robot = -10;
        Random rd = new Random();
        public Vector2 randomePos;
        public Monster[] finalWave;
        public bool finishedAni;

        public BossOne(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player) 
        {
            sprite.FrameToAction = 11;
            sprite.Scale = 1f;
            bossState = BossOneState.STATE1;
            
            //sound. Boss co the phai can them sound
            soundAttack = Content.Load<SoundEffect>("sounds/trash/Trash9_attack");
            soundDie = Content.Load<SoundEffect>("Animations/trash/boss1/boss1_die"); 

            particleEffect = Content.Load<ParticleEffect>("Particles/hitByTrash2");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();

            CurAnimation = GlobalClass.getAnimationByName("BOSS1_WALK");
        }

        public void loadWave()
        {
            SubWave waveData = Content.Load<SubWave>("Data/Map/Map1/BossWave");
            Monster[] childMonster = new Monster[waveData.totalMonster];
            new LoadCharacter(game).Monster(childMonster, player, waveData.monsters);
            
            finalWave = new Monster[waveData.totalMonster + 1];
            
            finalWave[0] = GamePlayScene.waveManager.wave.monsters[0];

            for (int i = 0; i < childMonster.Length; i++ )
            {
                finalWave[1 + i] = childMonster[i];
            }

            GamePlayScene.waveManager.wave.monsters = finalWave;
        }

        public override void setWalkAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("BOSS1_WALK");
        }

        public override void setAttackAnimation()
        {
            if (bossState == BossOneState.STATE1)
            {
                CurAnimation = GlobalClass.getAnimationByName("BOSS1_ATTACK_1");
                sprite.FrameToAction = 11;
            } 
            else if (bossState == BossOneState.STATE2)
            {
                CurAnimation = GlobalClass.getAnimationByName("BOSS1_ATTACK_2");
                sprite.FrameToAction = 13;
            }
            sprite.PlayAnimation(CurAnimation);
        }

        public override void setDeathAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("BOSS1_DIE");
        }

        public void setIdleAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("BOSS1_IDLE");
        }

        public override void createSewage(List<Item> sewages)
        {
        }

        public override Rectangle getRectangle()
        {
            if (bossState==BossOneState.STATE2)
                return new Rectangle((int)Position.X + 50, (int)Position.Y + 30, 
                    (int)(CurAnimation.FrameWidth * sprite.Scale - 50), (int)(sprite.Scale * CurAnimation.FrameHeight - 70));

            return new Rectangle((int)Position.X, (int)Position.Y, 
                (int)(CurAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * CurAnimation.FrameHeight));
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            if (bossState == BossOneState.STATE1)
            {

                if (sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
                    return new Rectangle(tmp.X + 60, tmp.Y - 30, tmp.Width - 50, tmp.Height + 30);
                else
                    return new Rectangle(tmp.X + 20, tmp.Y - 30, tmp.Width - 50, tmp.Height +30);
            }
            else if(bossState == BossOneState.STATE2)
            {
                return new Rectangle(tmp.X + 100, tmp.Y - 50, tmp.Width - 80, tmp.Height + 85);
            }
            else
            {
                return new Rectangle(tmp.X + 50, tmp.Y - 50, tmp.Width - 80, tmp.Height + 85);
            }
            
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable && isVisible)
            {
                Vector2 pos = new Vector2((int)(Position.X), (int)(Position.Y - 20));
                if (bossState == BossOneState.STATE1)
                {
                    if (sprite.SpriteEffect == SpriteEffects.None)
                        pos = new Vector2((int)(Position.X - 40), (int)(Position.Y - 20));
                    else if (isCollide)
                    {
                        pos = new Vector2((int)(Position.X + 75), (int)(Position.Y - 20));
                    }
                }
                else if (bossState == BossOneState.STATE2)
                    pos = new Vector2((int)(Position.X + 75), (int)(Position.Y - 20));

                spriteBatch.Draw(healthbar, new Rectangle((int)(pos.X), (int)pos.Y, (int)(healthbar.Width * 5), (int)(healthbar.Height*3)), 
                    null, Color.LightGray, 0f, Vector2.Zero, SpriteEffects.None, 0.17f);
                
                spriteBatch.Draw(healthbar, new Rectangle((int)(pos.X), (int)pos.Y,
                            (int)(healthbar.Width * 5 * (double)curHealth / Health), healthbar.Height*3), 
                            null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);

                //spriteBatch.Draw(Asset.healthTexture, getAttackRectangle(), Color.Red);
            }
        }

        public override void updatePosition(GameTime gameTime)
        {
            delaySwitchState += gameTime.ElapsedGameTime.Milliseconds;

            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);

            if (!isAttackable)
            {

                particleEffect = Content.Load<ParticleEffect>("Animations/trash/boss2/boss2_die_Effect");
                particleEffect.LoadContent(Content);
                particleEffect.Initialise();

                int x = 100;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));

                x = 80;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));

                x = 40;
                particleEffect.Trigger(Center);
                particleEffect.Trigger(Center + new Vector2(-x, 0));
                particleEffect.Trigger(Center + new Vector2(x, 0));
                particleEffect.Trigger(Center + new Vector2(0, x));
                particleEffect.Trigger(Center + new Vector2(0, -x));
                particleEffect.Trigger(Center + new Vector2(-x, -x));
                particleEffect.Trigger(Center + new Vector2(-x, x));
                particleEffect.Trigger(Center + new Vector2(x, -x));
                particleEffect.Trigger(Center + new Vector2(x, x));
            }

            {
                // ------------------ State 1-----------------------//
                if (bossState == BossOneState.STATE1)
                {
                    isVisible = true;
                    base.updatePosition(gameTime);
                    if (delaySwitchState > timeState1 && isAttackable)
                    {
                        delaySwitchState = 0;
                  //      Console.WriteLine("chuyen sang State 2");

                        if (Center.X > GlobalClass.ScreenWidth / 2)
                        {
                            randomePos = new Vector2(100, rd.Next(150, (int)GlobalClass.ScreenHeight - 120));
                            sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                        }
                        else
                            randomePos = new Vector2(GlobalClass.ScreenWidth - 120, rd.Next(150, (int)GlobalClass.ScreenHeight - 120));

                        bossState = BossOneState.STATE2;
                    }
                }
                // ------------------- State 2 --------------------------//
                else if (bossState == BossOneState.STATE2)
                {
                    if (isAttackable)
                    {
                        //float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
                        //particleEffect.Update(s);

                        setAttackAnimation();

                        if ((randomePos - Center).Length() > 5)
                            MoveTo(randomePos, 400, gameTime);
                        else
                        {
                            if (Center.X > GlobalClass.ScreenWidth / 2)
                            {
                                randomePos = new Vector2(100, rd.Next(150, (int)GlobalClass.ScreenHeight - 120));
                                sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                            }
                            else
                            {
                                randomePos = new Vector2(GlobalClass.ScreenWidth - 120, rd.Next(150, (int)GlobalClass.ScreenHeight - 120));
                                sprite.SpriteEffect = SpriteEffects.None;
                            }
                        }

                        if (sprite.isFrameToAction)
                        {
                            for (int i = 0; i < player.Length; i++)
                            {
                                if (player[i] == null || player[i].curHealth <=0 || !player[i].isVisible || !player[i].isControl) continue;
                                if (player[i] != null && player[i].curHealth > 0)
                                {
                                    //if (player[i].getRectangle().Intersects(this.getAttackRectangle()))
                                    //{
                                    //    player[i].curHealth -= damage;
                                    //    soundAttack.Play();
                                    //    particleEffect.Trigger(player[i].Center);
                                    //    particleEffect.Trigger(player[i].Center);
                                    //    particleEffect.Trigger(player[i].Center);
                                    //}
                                    if (Vector2.Distance(Center, player[i].Center) <= 350)
                                    {
                                        player[i].curHealth -= damage;
                                        soundAttack.Play();
                                        particleEffect.Trigger(player[i].Center);
                                        particleEffect.Trigger(player[i].Center);
                                        particleEffect.Trigger(player[i].Center);
                                    }
                                }
                            }
                            sprite.isFrameToAction = false;
                        }
                        if (delaySwitchState > timeState2)
                        {
                            delaySwitchState = 0;
                    //        Console.WriteLine("chuyen sang State 3");
                            loadWave();
                            isVisible = false;
                            bossState = BossOneState.STATE3;
                        }
                    }
                }
                // ------------------------- State 3 ----------------------------//
                else
                {
                    if (isAttackable)
                    {
                        Vector2 targetPos = new Vector2(GlobalClass.ScreenWidth / 2 - CurAnimation.FrameWidth * sprite.Scale / 2 + 200,
                            GlobalClass.ScreenHeight / 2 - CurAnimation.FrameHeight * sprite.Scale / 2 + 80);

                        if (targetPos.X < Center.X) sprite.SpriteEffect = SpriteEffects.FlipHorizontally;

                        if (Vector2.Distance(targetPos, Center) < 4)
                        {
                            setIdleAnimation();
                            isCenter = true;
                        }
                        else if (!isCenter)
                        {
                            setWalkAnimation();
                            MoveTo(targetPos, 100, gameTime);
                        }

                        if (isChildDeadAll())
                        {
                            isCenter = false;
                            delaySwitchState = 0;
                            bossState = BossOneState.STATE1;
                        }
                    }
                    else
                    {
                        setAllChildDead();
                    }
                }
            }
        }

        public bool isChildDeadAll()
        {
            for (int i = 1; i < finalWave.Length; i++ )
            {
                if (finalWave[i].isAttackable) return false;
            }
            return true;
        }

        public void setAllChildDead()
        {
            for (int i = 1; i < finalWave.Length; i++)
            {
                finalWave[i].curHealth = -1;
            }
        }

        public override Player getNearestRobot()
        {
            int check = 0;
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i].curHealth <= 0) check++;
                else break;
                if (check==player.Length) return null;
            }

            if (robot >= 0 && player[robot].curHealth > 0 )
            { 
                return player[robot];
            }

            robot = rd.Next(0, player.Length);

            while ( player[robot].curHealth <=0 )
                robot = rd.Next(0, player.Length);
           
       //     Console.WriteLine("danh robot" +  robot);
            return player[robot];
        }
    }

}
