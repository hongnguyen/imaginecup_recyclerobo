﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class Item
    {
        protected Texture2D image;
        public Texture2D Image
        {
            get { return image; }
        }
        public Vector2 position;
        public Vector2 originPosition;
        protected bool isTouched;

        protected float amount;
        public float Amount
        {
            get { return amount; }
        }

        public float scale;
        public SpriteEffects spriteEffect;
        public float layer;
        public AssNormal assCollect;
        public Color[] textureData;
        public Rectangle baseRect, rect;
        public Matrix transformMatrix; 
        public Item(Texture2D pTexture, Vector2 pos, float pScale, SpriteEffects spriteEff, float layer, float amount)
        {
            assCollect = new AssNormal();
            image = pTexture;
            position = pos;
            originPosition = pos;
            isTouched = false;
            scale = pScale;
            spriteEffect = spriteEff;
            this.layer = layer;
            assCollect.layer = layer - 0.001f;
            this.amount = amount / 150;

            textureData = new Color[image.Width * image.Height];
            image.GetData(textureData);
            baseRect = new Rectangle(0, 0, image.Width, image.Height);

            transformMatrix = RotatedRectangle.GetMatrixTransfrom(this.position, Vector2.Zero, 0f, scale);
            rect = RotatedRectangle.CalculateBoundingRectangle(baseRect, transformMatrix);

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            assCollect.Draw(spriteBatch);
            spriteBatch.Draw(image, position, null, Color.White, 0f, Vector2.Zero, scale, spriteEffect, layer);
        }

        public void UpdatePosition(GameTime gameTime)
        {
            assCollect.Update(gameTime, this);

        }
    }
}
