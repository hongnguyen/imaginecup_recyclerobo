﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class OceanSealForm : ElementForm
    {
        private int time;
        private SeriesPointRecognitor series;
        //chu Z
        private static Vector2[] offset = { new Vector2(-50, -30), 
                                          new Vector2(-25, -30), new Vector2(0, -30), 
                                          new Vector2(25, -30) ,new Vector2(50, -30),
                                          new Vector2(25, -5), new Vector2(0, 20), 
                                          new Vector2(-25, 45), new Vector2(-50, 70),
                                          new Vector2(-25, 70), new Vector2(0, 70), 
                                          new Vector2(25, 70), new Vector2(50, 70),};
        private Animation effectAnimation;
        private Monster[] monsters;
        private static float scale = 0.85f;
        public OceanSealForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.sealTile, deltaPos, 450);
            gestureAnimation = Asset.gestureZ;
            time = 0;
            series = new SeriesPointRecognitor(offset, 60, 60);
            areaAnimation = GlobalClass.getAnimationByName("NUM1_AREA_3");
            deltaAreaPos = new Vector2(-10, -27);
            isGestureType = true;
            this.effectAnimation = GlobalClass.getAnimationByName("OCEAN_PRISON");
            sound = Content.Load<SoundEffect>("sounds/elementForm/ocean_form");
            monsters = new Monster[0];
        }

        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            this.monsters = monsters;


            double[] d =  new double[monsters.Length];
            Random r = new Random();
            time += gameTime.ElapsedGameTime.Milliseconds;
            const float effectRadius = 200.0f;
            bool[] justParalized = new bool[monsters.Length];
            for (int i = 0; i < justParalized.Length; i++) 
            {
                d[i] = r.NextDouble();
                justParalized[i] = false;
            }
            int j = 0;

            if (series.recognize(gameTime, robot))
            {
                foreach (Monster monster in monsters)
                {
                    if(!monster.isAlive) continue;
                    monster.effect.PlayAnimation(effectAnimation);
                    monster.effect.Scale = scale;
                    monster.effect.OrderLayer = 0.0001f;
                    monster.effect.color = Color.White;

                    if (Vector2.Distance(robot.Center, monster.Center) < effectRadius )
                    {
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "OceanPrison");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "OceanPrison");
                        monster.curHealth -= 50;
                        if (d[j] > 0.7)
                        {
                            monster.curHealth -= 300;
                            justParalized[j] = true;
                            if (monster.movementEffectModifier["ocean"] == 1.0f)
                                monster.movementEffectModifier["ocean"] = 0f;
                            time = 0;
                            monster.effectToDraw += "oceanprison";
                        }
                    }
                    j++;
                }
            }
            if (time >= 4000)
            {
                for (int k = 0 ; k < justParalized.Length ; k++) 
                {
                    if (justParalized[k]) continue;
                    else
                    {
                        monsters[k].movementEffectModifier["ocean"] = 1.0f;
                        monsters[k].effectToDraw = monsters[k].effectToDraw.Replace("oceanprison", "");
                    }
                }
                time = 0;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gametime)
        {
            
            foreach (Monster monster in monsters)
            {
                if (!monster.isAlive || !monster.isAttackable || !monster.isVisible)
                    continue;
                if (monster.effectToDraw.StartsWith("oceanprison")) 
                {
                    Vector2 pos = new Vector2();
                    monster.effect.PlayAnimation(effectAnimation);
                    pos.X = monster.Center.X - effectAnimation.FrameWidth * scale / 2;
                    pos.Y = monster.Center.Y + monster.CurAnimation.FrameHeight * monster.sprite.Scale / 2
                        - effectAnimation.FrameHeight * scale / 2 - 20;
                    monster.effect.Draw(gametime, spriteBatch, pos);
                }
            }
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name + "_SEAL";
            return GlobalClass.getAnimationByName(key);
        }
        public override void reset()
        {
            foreach (Monster monster in monsters)
            {
                monster.effectToDraw = monster.effectToDraw.Replace("oceanprison", "");
                monster.movementEffectModifier["ocean"] = 1.0f;
            }
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(85, 27));
        }
    }
}
