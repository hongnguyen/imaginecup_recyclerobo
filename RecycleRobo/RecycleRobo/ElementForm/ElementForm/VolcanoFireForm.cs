﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class VolcanoFireForm : ElementForm
    {
        private int timeDrag;
        private int time;
        private int time2;
        private const float effectRadius = 80.0f;
        private const float FromPlayerEffectRadius = 120.0f;
        private const float dragTimethreshold = 200.0f;
        private Vector2 beginPos;

        public VolcanoFireForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.fireTile, deltaPos,500);
            time = 0;
            time2 = 0;
            timeDrag = 0;
            beginPos = new Vector2(-1,-1);
            areaAnimation = GlobalClass.getAnimationByName("NUM1_AREA_4");
            deltaAreaPos = new Vector2(-20, -20);
            this.isGestureType = true;
            sound = Content.Load<SoundEffect>("sounds/elementForm/Fire_Form");
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name+"_FIRE";
            return GlobalClass.getAnimationByName(key);
        }

        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            if (!robot.isSelected)
            {
                this.time = 0;
                this.timeDrag = 0;
                this.time2 = 0;
                return;
            }
            List<Monster> TMPmonsters = new List<Monster>();
            foreach(Monster monster in monsters)
            {
                if(monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            timeDrag += gameTime.ElapsedGameTime.Milliseconds;
            time += gameTime.ElapsedGameTime.Milliseconds;
            time2 += gameTime.ElapsedGameTime.Milliseconds;
            Random r = new Random();
            
            curState = Mouse.GetState();
            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
            Vector2 pos = new Vector2(mousePoint.X, mousePoint.Y);

            if (prevState.LeftButton == ButtonState.Released && curState.LeftButton == ButtonState.Pressed)
                beginPos = new Vector2(curState.X, curState.Y);

            if (curState.LeftButton == ButtonState.Released) timeDrag = 0;
            else timeDrag += gameTime.ElapsedGameTime.Milliseconds;

            bool holding = curState.LeftButton == ButtonState.Pressed 
                        && prevState.LeftButton == ButtonState.Pressed 
                        && timeDrag > dragTimethreshold ;
            if (holding) 
            {
                if (time2 >= 2500)
                    sound.Play();
                if (Vector2.Distance(pos, robot.Center) < FromPlayerEffectRadius)
                {
                    Scenes.GamePlayScene.sideEffect.sideEffectTrigger(pos, "VolcanoFire");
                    Scenes.GamePlayScene.sideEffect.sideEffectTrigger(pos, "VolcanoFire");
                    Scenes.GamePlayScene.sideEffect.sideEffectTrigger(pos, "VolcanoFire");
                    foreach (Monster monster in monsters){
                        if (Vector2.Distance(pos, monster.Center) < effectRadius
                            &&  monster.isAttackable
                            && time >= 100)
                        {
                            monster.curHealth -= (int)((200 + robot.levelOfRobot * 15) * (float)gameTime.ElapsedGameTime.Milliseconds / 50);
                        }
                    }
                }
            }
            if (time > 100) 
            {
                time = 0;
            }
            if (time2 > 2500)
                time2 = 0;
            prevState = curState;
        }
    }
}
