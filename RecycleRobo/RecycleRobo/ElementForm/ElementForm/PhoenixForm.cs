﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace RecycleRobo
{
    public class PhoenixForm : ElementForm
    {
        int count;
        public PhoenixForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.phoenixTile, deltaPos, 100);
            gestureAnimation = Asset.gestureTap;
            count = 0;
            areaAnimation = GlobalClass.getAnimationByName("NUM2_AREA_4");
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name + "_PHOENIX";
            return GlobalClass.getAnimationByName(key);
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(90, 24));
        }

        public override void Update(Player robot, GameTime gameTime)
        {
            curState = Mouse.GetState();
            if (curState.LeftButton==ButtonState.Pressed && prevState.LeftButton == ButtonState.Released)
            {
                if (count < 3)
                    count++;
                else
                {
                    Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
                    if (robot.getRectangle().Contains(mousePoint))
                    {
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(robot.Center, "PhoenixFire");
                        robot.curHealth += (int)(robot.initialHealth * 0.15f);
                        if (robot.curHealth > robot.initialHealth)
                            robot.curHealth = robot.initialHealth;
                    }
                    count = 0;
                }
            }

            prevState = curState;
        }
    }
}
