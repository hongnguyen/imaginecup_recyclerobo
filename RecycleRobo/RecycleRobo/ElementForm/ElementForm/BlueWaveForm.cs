﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class BlueWaveForm : ElementForm
    {
        private int timeDrag;
        private int time;
        private const float effectRadius = 150.0f;
        private const float FromPlayerEffectRadius = 250.0f;
        private const float dragTimethreshold = 200.0f;
        private Vector2 beginPos;
        public BlueWaveForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.waveTile, deltaPos, 900);
            time = 0;
            timeDrag = 0;
            areaAnimation = GlobalClass.getAnimationByName("NUM5_AREA_3");
            deltaAreaPos = new Vector2(-5, -40);
            this.isGestureType = true;
            sound = Content.Load<SoundEffect>("sounds/elementForm/Blue_wave_Form");
        }
        public override Animation getAnimationForm(String name)
        {
            String key = name+"_WAVE";
            return GlobalClass.getAnimationByName(key);
        }
        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            if (!robot.isSelected)
            {
                this.time = 0;
                this.timeDrag = 0;
                return;
            }
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            timeDrag += gameTime.ElapsedGameTime.Milliseconds;
            time += gameTime.ElapsedGameTime.Milliseconds;
            Random r = new Random();


            curState = Mouse.GetState();
            Vector2 pos = new Vector2(curState.X, curState.Y);

            if (prevState.LeftButton == ButtonState.Released && curState.LeftButton == ButtonState.Pressed)
                beginPos = new Vector2(curState.X, curState.Y);

            if (curState.LeftButton == ButtonState.Released) timeDrag = 0;
            else timeDrag += gameTime.ElapsedGameTime.Milliseconds;

            bool holding = curState.LeftButton == ButtonState.Pressed
                        && prevState.LeftButton == ButtonState.Pressed
                        && timeDrag > dragTimethreshold
                        && Vector2.Distance(pos,robot.Center) < FromPlayerEffectRadius;
            if (holding)
            {
                if(time >= 1000)
                    sound.Play();
                Scenes.GamePlayScene.sideEffect.sideEffectTrigger(pos, "blueWave");
                foreach (Monster monster in monsters)
                {
                    if (Vector2.Distance(pos, monster.Center) < effectRadius)
                    {
                        monster.movementEffectModifier["wave"] = 0.4f;
                        monster.curHealth -= (int)((100 + robot.levelOfRobot * 10) * (float)gameTime.ElapsedGameTime.Milliseconds / 500);
                    }
                    else
                    {
                        monster.movementEffectModifier["wave"] = 1.0f;
                    }
                }
            }
            else 
            {
                foreach (Monster monster in monsters) 
                {
                    monster.speed = monster.initialspeed;
                }
            }
            if (time > 1000)
            {
                time = 0;
            }
            prevState = curState;
        }
    }
}
