﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class LeafCutterForm : ElementForm
    {
        private float rotation;
        private const float effectRadius = 400f;
        private int time;
        private int time2;
        private Vector2 curPoint, prePoint;
        private Animation effectAnimation;
        private Monster[] monsters;
        private static float scale = 1.0f;
        private bool justEffect;
        public LeafCutterForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.cutterTile, deltaPos, 150);
            gestureAnimation = Asset.gestureO;
            curPoint = new Vector2(-1, -1);
            prePoint = new Vector2(-1, -1);
            time = 0;
            time2 = 0;
            rotation = 0.0f;
            areaAnimation = GlobalClass.getAnimationByName("NUM3_AREA_2");
            this.effectAnimation = GlobalClass.getAnimationByName("LEAFCUT");
            monsters = new Monster[0];
            deltaAreaPos = new Vector2(-10, -20);
            this.isGestureType = true;
            sound = Content.Load<SoundEffect>("sounds/elementForm/leaf_cut_form");
            justEffect = false;
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name+"_CUTTER";
            return GlobalClass.getAnimationByName(key);
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(70, 21));
        }

        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            if (!robot.isSelected) 
            {
                this.rotation = 0;
                this.time = 0;
                return;
            }
            curState = Mouse.GetState();
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();

            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
            Vector2 pos = new Vector2(mousePoint.X, mousePoint.Y);
            if (Vector2.Distance(pos, robot.Center) > 200f)
                rotation = 0;
            time += gameTime.ElapsedGameTime.Milliseconds;
            time2 += gameTime.ElapsedGameTime.Milliseconds;
            if (curState.LeftButton == ButtonState.Released) 
                rotation = 0;
            else if (time > 50f)
            {
                curPoint = pos;
                if (Vector2.Distance(curPoint, prePoint) < 10f) goto next;
                Vector2 first = new Vector2(curPoint.X - robot.Center.X, curPoint.Y - robot.Center.Y);
                Vector2 second = new Vector2(prePoint.X - robot.Center.X, prePoint.Y - robot.Center.Y);
                rotation += (float)Math.Acos(((first.X * second.X + first.Y * second.Y) / (first.Length() * second.Length())));
                prePoint = curPoint;
                time = 0;
            }
            next:
            

            if (Math.Abs(rotation) > 6.0f && !justEffect) 
            {
                time2 = 0;
                justEffect = true;
                sound.Play();
                Scenes.GamePlayScene.sideEffect.sideEffectTrigger(robot.Center, "leafcut");
                Scenes.GamePlayScene.sideEffect.sideEffectTrigger(robot.Center, "leafcut");
                Scenes.GamePlayScene.sideEffect.sideEffectTrigger(robot.Center, "leafcut");
                foreach (Monster monster in monsters) 
                {
                    if (Vector2.Distance(monster.Center, robot.Center) < effectRadius) 
                    {
                        monster.curHealth -= 200 + robot.levelOfRobot * 15;
                        
                    }
                }
                rotation = 0;
            }

            if (time2 > 1200) 
            {
                time2 = 0;
                justEffect = false;
            }
            prevState = curState;
        }
    }
}
