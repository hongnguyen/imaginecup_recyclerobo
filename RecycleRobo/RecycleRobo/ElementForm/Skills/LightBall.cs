﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectMercury;
using RecycleRobo.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class LightBall : Skill
    {

        public enum LightBallState
        {
            STATE1,
            STATE2,
            STATE3,
            COMPLETE  
        }

        public LightBallState state = LightBallState.STATE1;

        public Vector2 ballPos;
        public Vector2 cardlv1Pos;
        public Vector2 cardlv2Pos;
        public Vector2 cardlv3Pos;
        public Vector2 iconlv1Pos;
        public Vector2 iconlv2Pos;
        public Vector2 iconlv3Pos;
        public float rotate = 0;
        public float scaleBall = 0.2f;
        public float scaleCard = 0.625f;

        public AnimationPlayer animPlayLv1, animPlayLv2, animPlayLv3;

        public bool hopnhat = true;
        public bool isFire = false;
        public bool isAttack = false;
        public float timeHopnhat = 0f;
        public float timeTranslate = 0;

        //Quan can bang bien damage nhe!
        public int damage = 400;

        // sound
        private SoundEffect attackSound;

        public LightBall (ContentManager Content) 
        {
            ballPos = new Vector2(230, 400);
            
            cardlv1Pos = new Vector2(GlobalClass.ScreenWidth + Asset.num2_cardLV1.Width * scaleCard / 2, GlobalClass.ScreenHeight - Asset.num2_cardLV1.Height*scaleCard / 2 - 30);
            
            cardlv2Pos = new Vector2(GlobalClass.ScreenWidth - Asset.num2_cardLV2.Width * scaleCard / 2 - 20, -Asset.num2_cardLV2.Height * scaleCard / 2);
            
            cardlv3Pos = new Vector2(-Asset.num2_cardLV3.Width * scaleCard / 2, GlobalClass.ScreenHeight / 2);

            animPlayLv1.Scale = scaleCard;
            animPlayLv1.OrderLayer = 0.01f;
            animPlayLv1.color = Color.White;
            animPlayLv1.PlayAnimation(GlobalClass.getAnimationByName("NUMBER2_IDLE_1"));
            iconlv1Pos = new Vector2(-200, 400);

            animPlayLv2.Scale = scaleCard;
            animPlayLv2.OrderLayer = 0.01f;
            animPlayLv2.color = Color.White;
            animPlayLv2.PlayAnimation(GlobalClass.getAnimationByName("NUMBER2_IDLE_2"));
            iconlv2Pos = new Vector2(-50, 250);

            animPlayLv3.Scale = scaleCard;
            animPlayLv3.OrderLayer = 0.01f;
            animPlayLv3.color = Color.White;
            animPlayLv3.PlayAnimation(GlobalClass.getAnimationByName("NUMBER2_IDLE_3"));
            iconlv3Pos = new Vector2(-220, 300);


            skillEffect = Content.Load<ParticleEffect>("skills/skill1/hitBySkill1");
            skillEffect.LoadContent(Content);
            skillEffect.Initialise();


            //sound
            SoundEffect.MasterVolume = GlobalClass.volumeSoundFx / 100;

            skillSoundEffect = Content.Load<SoundEffect>("skills/lightball/giga light ball");
            skillSoundEffect.Play();

            skillSound = Content.Load<SoundEffect>("skills/skillSound");
            soundInstance = skillSound.CreateInstance();
            soundInstance.Play();

            attackSound = Content.Load<SoundEffect>("skills/skill1/_skill1Attack");

            isSkillComplete = false;
            RoboVisible(GamePlayScene.player, false);
        }

        public void Update(GameTime gameTime, WaveManager waveManager)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (state == LightBallState.STATE1)
            {
                timeTranslate += gameTime.ElapsedGameTime.Milliseconds;
                if (timeTranslate >= 4 && hopnhat)
                {
                    timeTranslate = 0;
                    if (cardlv1Pos.X <= GlobalClass.ScreenWidth - Asset.num2_cardLV1.Width * scaleCard / 2 - 20)
                        cardlv1Pos = new Vector2(GlobalClass.ScreenWidth - Asset.num2_cardLV1.Width * scaleCard / 2 - 20, cardlv1Pos.Y);
                    else cardlv1Pos += new Vector2(-40, 0);

                    if (cardlv2Pos.Y >= Asset.num2_cardLV2.Height * scaleCard / 2 + 30)
                        cardlv2Pos = new Vector2(cardlv2Pos.X, Asset.num2_cardLV2.Height * scaleCard / 2 + 30);
                    else cardlv2Pos += new Vector2(0, 25);

                    if (cardlv3Pos.X >= Asset.num2_cardLV3.Width * scaleCard / 2 + 20) cardlv3Pos = new Vector2(Asset.num2_cardLV3.Width * scaleCard / 2 + 20, cardlv3Pos.Y);
                    else cardlv3Pos += new Vector2(30, 0);

                }
                timeHopnhat += gameTime.ElapsedGameTime.Milliseconds;
                if (timeHopnhat >= 3000 && hopnhat)
                {
                    timeHopnhat = 0f;
                    timeTranslate = 0f;
                    state = LightBallState.STATE2;
                    hopnhat = false;
                }
            }
            
            if (state == LightBallState.STATE2)
            {
                timeTranslate += gameTime.ElapsedGameTime.Milliseconds;
                if (!hopnhat && timeTranslate >= 10)
                {
                    if (iconlv2Pos.X >= 200)
                    {
                        state = LightBallState.STATE3;
                        timeHopnhat = 0f;
                        timeTranslate = 0f;
                    }
                    else
                    {
                        iconlv1Pos += new Vector2(2, 0);
                        iconlv2Pos += new Vector2(2, 0);
                        iconlv3Pos += new Vector2(2, 0);
                    }
                }
            }
            
            if (state == LightBallState.STATE3)
            {
                timeHopnhat += gameTime.ElapsedGameTime.Milliseconds;
                if (timeHopnhat >=10)
                {
                    timeHopnhat = 0f;
                    rotate += 0.01f;
                    if (!isFire)scaleBall += 0.001f;
                    if (scaleBall >= 0.6f) isFire = true;
                }
                if (isFire)
                {
                    timeTranslate += gameTime.ElapsedGameTime.Milliseconds;
                    if (timeTranslate > 10)
                    {
                        timeTranslate = 0f;
                        ballPos += new Vector2(8, 0.5f);
                        skillEffect.Trigger(ballPos + new Vector2(-30, -50));
                        skillEffect.Trigger(ballPos + new Vector2(-30, 0));
                        skillEffect.Trigger(ballPos + new Vector2(-30, 50));
                    }

                    if (ballPos.X > GlobalClass.ScreenWidth + 20)
                    {
                        state = LightBallState.COMPLETE;
                        showParticles(waveManager);
                        Vector2 pos = new Vector2(GlobalClass.ScreenWidth - 30, ballPos.Y);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                        skillEffect.Trigger(pos);
                    }
                }
            }

            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            skillEffect.Update(s);
            
            if (state==LightBallState.COMPLETE)
            {
                if (!isAttack)
                {
                    attackSound.Play();
                    DamageAllEnemy(waveManager, damage);
                    isAttack = true;
                }

                timeHopnhat += gameTime.ElapsedGameTime.Milliseconds;
                if (timeHopnhat >= 1500)
                {
                    RoboVisible(GamePlayScene.player, true);
                    isSkillComplete = true;
                    soundInstance.Stop();
                }
            }
        }

        public void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            if (!isSkillComplete)
            {
                spriteBatch.End();
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());            //virtualScreen.Draw(spriteBatch);
                GameManager.particlesRender.RenderEffect(skillEffect, ref GameManager.transform);

                if (state == LightBallState.STATE1)
                {
                    spriteBatch.Draw(Asset.num2_cardLV1, cardlv1Pos, null, Color.White, 0f,
                        new Vector2(Asset.num2_cardLV1.Width / 2, Asset.num2_cardLV1.Height / 2), scaleCard, SpriteEffects.None, 0.1f);

                    spriteBatch.Draw(Asset.num2_cardLV2, cardlv2Pos, null, Color.White, 0f,
                        new Vector2(Asset.num2_cardLV2.Width / 2, Asset.num2_cardLV2.Height / 2), scaleCard, SpriteEffects.None, 0.1f);

                    spriteBatch.Draw(Asset.num2_cardLV3, cardlv3Pos, null, Color.White, 0f,
                        new Vector2(Asset.num2_cardLV3.Width / 2, Asset.num2_cardLV3.Height / 2), scaleCard, SpriteEffects.None, 0.1f);
                }
                else
                {
                    animPlayLv1.Draw(gametime, spriteBatch, iconlv1Pos);
                    animPlayLv2.DrawRotate(gametime, spriteBatch, iconlv2Pos, 0.5f);
                    animPlayLv3.Draw(gametime, spriteBatch, iconlv3Pos);
                    if (state == LightBallState.STATE3)
                    {
                        spriteBatch.Draw(Asset.lightball, ballPos, null, Color.White, (rotate * 3.141592f), new Vector2(Asset.lightball.Width / 2, Asset.lightball.Height / 2), scaleBall, SpriteEffects.None, 0.1f);
                    }
                }
            }
        }

    }
}
