using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ProjectMercury;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
    public enum HolyHealState
    {
        ANNOUNCEMENT_SKILL,
        CREATE_HOLY,
        HOLY_COMPLETE,
        SKILL_COMPLETE
    }
    public class HolyHeal : Skill
    {
        AnimationPlayer numberFourAni;
        Animation curAniNum4;
        private Texture2D announceNum4;
        HolyHealState curState;
        private Vector2 positionAnn;
        int timeCount = 0;
        bool isStop = false;
        private int healAmount = 40; //x250 times

        public HolyHeal(ContentManager content)
        {
            numberFourAni.Scale = 0.625f;
            numberFourAni.OrderLayer = 0.02f;
            numberFourAni.color = Color.White;
            curState = HolyHealState.ANNOUNCEMENT_SKILL;

            if (BuildTeamScene.Robo_Evolution[3]==1)
                announceNum4 = content.Load<Texture2D>("skills/skill3/holyHeal_card_lvl1");
            else if (BuildTeamScene.Robo_Evolution[3]==2)
                announceNum4 = content.Load<Texture2D>("skills/skill3/skill3_num4");
            else
                announceNum4 = content.Load<Texture2D>("skills/skill3/holyHeal_card_lvl3");

            positionAnn = new Vector2(GlobalClass.ScreenWidth / 2 - Convert.ToInt32(announceNum4.Width * 0.625) / 2, GlobalClass.ScreenHeight);
            skillEffect = content.Load<ParticleEffect>("skills/skill3/skill3Heal");
            skillEffect.LoadContent(content);
            skillEffect.Initialise();
            skillSoundEffect = content.Load<SoundEffect>("skills/skill3/skill3");
            SoundEffect.MasterVolume = 1f;
            skillSoundEffect.Play();
            indexSkill = 3;
        }

        private void NumberFourVisible(Player[] player, bool value)
        {
            foreach (Player p in player.ToArray())
            {
                if (p != null && p.curHealth > 0.5)
                {
                    if (p.GetType().Name == "NumberFour")
                    {
                        p.isVisible = value;
                    }
                    p.isControl = value;
                }

            }
            return;
        }

        public void Update(GameTime gameTime, Player[] player)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (curState == HolyHealState.ANNOUNCEMENT_SKILL)
            {
                NumberFourVisible(player, false);
                if (isStop)
                {
                    timeCount += gameTime.ElapsedGameTime.Milliseconds / 16;
                    if (timeCount < 80)
                        positionAnn.Y += 20;
                    else
                    {
                        timeCount = 0;
                        curState = HolyHealState.CREATE_HOLY;
                        curAniNum4 = GlobalClass.getAnimationByName("CREATE_HOLY_"+ BuildTeamScene.Robo_Evolution[3]);
                    }

                }

                if (positionAnn.Y <= 0 && !isStop)
                {
                    positionAnn.Y = 0;
                    timeCount += gameTime.ElapsedGameTime.Milliseconds / 16;
                    if (timeCount >= 150)
                    {
                        timeCount = 0;
                        isStop = true;
                    }
                }
                else if (!isStop)
                {
                    if (positionAnn.Y <= 0) positionAnn.Y = 0;
                    else positionAnn.Y -= 40;
                }
            }
            if (curState == HolyHealState.CREATE_HOLY)
            {

                timeCount += gameTime.ElapsedGameTime.Milliseconds / 16;
                skillEffect.Trigger(new Vector2(100, 0));
                skillEffect.Trigger(new Vector2(800, 0));
                skillEffect.Trigger(new Vector2(100, 200));
                skillEffect.Trigger(new Vector2(800, 200));
                skillEffect.Trigger(new Vector2(100, 100));
                skillEffect.Trigger(new Vector2(800, 100));

                foreach (Player p in player)
                {
                    if (p != null && p.curHealth < p.initialHealth && timeCount < 250 && p.curHealth > 0.5)
                    {
                        p.curHealth += healAmount;
                        if (p.curHealth > p.initialHealth) p.curHealth = p.initialHealth;
                    }
                }
                if (timeCount > 250)
                {
                    curState = HolyHealState.HOLY_COMPLETE;
                    timeCount = 0;
                }
            }
            if (curState == HolyHealState.HOLY_COMPLETE)
            {
                //Console.WriteLine("XOG HOLY HEAL");
                NumberFourVisible(player, true);
                isSkillComplete = true;
            }

            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            skillEffect.Update(2f * s);

        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            numberFourAni.PlayAnimation(curAniNum4);

            switch (curState)
            {
                case HolyHealState.ANNOUNCEMENT_SKILL:
                    spriteBatch.Draw(announceNum4, positionAnn, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.001f);
                    break;
                case HolyHealState.CREATE_HOLY:
                    numberFourAni.Draw(gameTime, spriteBatch, new Vector2(GlobalClass.ScreenWidth / 2 - 100, GlobalClass.ScreenHeight / 2 - 120));
                    spriteBatch.End();
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());            //virtualScreen.Draw(spriteBatch);
                    GameManager.particlesRender.RenderEffect(skillEffect, ref GameManager.transform);
                    break;
                case HolyHealState.HOLY_COMPLETE:
                    break;
            }
        }
    }
}