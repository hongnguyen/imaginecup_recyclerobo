using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectMercury;
using RecycleRobo.Scenes;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class Projectile
    {
        AnimationPlayer projectileAnimation;
        public Vector2 position;
        public Vector2 direction;
        public float speed;
        public int damage = 20;
        public bool isAttack;

        public Projectile(Vector2 pos, Vector2 direct, float speed)
        {
            projectileAnimation.Scale = 0.65f;
            projectileAnimation.OrderLayer = 0.001f;
            projectileAnimation.color = Color.White;
            projectileAnimation.PlayAnimation(GlobalClass.getAnimationByName("SKILL2_PROJECTILE"));
            this.position = pos;
            this.direction = direct;
            this.speed = speed;
            isAttack = false;
        }

        public void Update(GameTime gameTime)
        {
            position.X += (float)(direction.X * speed * gameTime.ElapsedGameTime.TotalSeconds);
            position.Y += (float)(direction.Y * speed * gameTime.ElapsedGameTime.TotalSeconds);

            if (position.X > GlobalClass.ScreenWidth)
            {
                direction.X *= -1;
                projectileAnimation.SpriteEffect = SpriteEffects.FlipHorizontally;
                position.X = GlobalClass.ScreenWidth;
            }

            else if (position.X < 0)
            {
                direction.X *= -1;
                projectileAnimation.SpriteEffect = SpriteEffects.None;
                position.X = 0;
            }

            if (position.Y > GlobalClass.ScreenHeight)
            {
                direction.Y *= -1;
                position.Y = GlobalClass.ScreenHeight;
            }

            else if (position.Y < 0)
            {
                direction.Y *= -1;
                position.Y = 0;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            projectileAnimation.Draw(gameTime, spriteBatch, position);
        }

        public Rectangle getBounding()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)(projectileAnimation.Animation.FrameWidth * projectileAnimation.Scale),
                                                         (int)(projectileAnimation.Animation.FrameHeight * projectileAnimation.Scale));
        }
    }

    public class GigaSlash : Skill
    {
        enum GigaSlashState
        {
            ANNOUNCEMENT_SKILL,
            TAKE_ROBO,
            COMBINE_SKILL,
            SHOW_COMBINATION,
            GIGA_ATTACKING,
            CREATE_PROJECTILE_DONE,
        }

        Texture2D announceNum1, announceNum3;
        float x_num1, y_num1, x_num3, y_num3;

        Texture2D skillCombination;
        float x_combi, y_combi;

        GigaSlashState curState;

        private Texture2D timeHole, skyPair, landPair;
        private Vector2 skyPairPos, landPairPos;

        AnimationPlayer gigaSlashAnimation;
        Animation curAnimation;

        bool isStop; float timecount = 0;

        List<Projectile> projectiles;

        SoundEffect projectileSound;

        Random r = new Random();

        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };

        private Rectangle clipRect;

        public GigaSlash(ContentManager content)
        {
            gigaSlashAnimation.OrderLayer = 0.001f;
            gigaSlashAnimation.Scale = 1f;
            gigaSlashAnimation.color = Color.White;
            gigaSlashAnimation.FrameToAction = 14;

            curState = GigaSlashState.ANNOUNCEMENT_SKILL;

            announceNum1 = Asset.announceNum1;
            x_num1 = 150; y_num1 = GlobalClass.ScreenHeight;

            announceNum3 = Asset.announceNum3;
            x_num3 = 600; y_num3 = -announceNum3.Height * 0.63f;

            skillCombination = Asset.skillCombination;
            x_combi = GlobalClass.ScreenWidth / 2 - skillCombination.Width / 2 * 0.625f;
            y_combi = GlobalClass.ScreenHeight;

            isStop = false;

            timeHole = Asset.timeHole;
            skyPair = Asset.skyPair;
            landPair = Asset.landPair;

            skyPairPos = new Vector2(-30, 250);
            landPairPos = new Vector2(-13, 300);

            projectiles = new List<Projectile>();

            skillEffect = Asset.effectGiga;
            skillEffect.LoadContent(content);
            skillEffect.Initialise();

            isSkillComplete = false;
            //if (!GlobalClass.isFullScreen)
            {
                clipRect = new Rectangle(84, 166, 1100, 450);
                clipRect = RotatedRectangle.CalculateBoundingRectangle(clipRect, GameManager.transform);
               //clipRect.Width += 37;
            }
            //else
            //{
            //    clipRect = new Rectangle(84, 166, 1281, 450);
            //    clipRect = new Rectangle(84, 166, 1077, 450);
            //    Matrix transfrom = RotatedRectangle.GetMatrixTransfrom(new Vector2(clipRect.X, clipRect.Y), Vector2.Zero, 0f, 1f);
            //    clipRect = RotatedRectangle.CalculateBoundingRectangle(clipRect, GameManager.transform);
            //    clipRect.Width += 43;
            //    clipRect.X += 71;
            //    clipRect.Y -= 6;
            //}

            SoundEffect.MasterVolume = GlobalClass.volumeSoundFx / 100;

            skillSoundEffect = content.Load<SoundEffect>("skills/skill2/skill2");
            skillSoundEffect.Play();

            skillSound = content.Load<SoundEffect>("skills/skillSound");
            soundInstance = skillSound.CreateInstance();
            soundInstance.Play();

            projectileSound = content.Load<SoundEffect>("skills/skill2/_skill2Attack");

            indexSkill = 2;
        }

        public void Update(GameTime gameTime, Player[] player, WaveManager waveManager)
        {
            //Console.WriteLine(GlobalClass.ScreenWidth);
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (curState == GigaSlashState.ANNOUNCEMENT_SKILL)
            {
                RoboVisible(player, false);
                if (y_num3 >= GlobalClass.ScreenWidth + 300)
                {
                    timecount = 0;
                    //curState = GigaSlashState.COMBINE_SKILL;
                    //curAnimation = GlobalClass.getAnimationByName("SKILL2_COMBINE");
                    curState = GigaSlashState.TAKE_ROBO;
                }

                if (y_num1 <= 0 && !isStop)
                {
                    y_num1 += 0;
                    y_num3 += 0;
                    timecount += gameTime.ElapsedGameTime.Milliseconds;
                    if (timecount >= 1950)
                    {
                        timecount = 0;
                        isStop = true;
                    }
                }
                else
                {
                    y_num1 -= 40;
                    y_num3 += 40;
                }
            }

            if (curState == GigaSlashState.TAKE_ROBO)
            {
                timecount += gameTime.ElapsedGameTime.Milliseconds;
                if (timecount >= 30)
                {
                    if (landPairPos.X < 210)
                    {
                        skyPairPos.X += 2;
                        landPairPos.X += 2;
                    }
                    else
                    {
                        skyPairPos.X += 15; 
                        landPairPos.X = 210;
                    }
                    //timecount = 0;
                    //curState = GigaSlashState.COMBINE_SKILL;
                    //curAnimation = GlobalClass.getAnimationByName("SKILL2_COMBINE");
                }

                if (skyPairPos.X >= GlobalClass.ScreenWidth)
                {
                    timecount = 0;
                    curState = GigaSlashState.COMBINE_SKILL;
                    curAnimation = GlobalClass.getAnimationByName("SKILL2_COMBINE");
                }
            }

            if (curState == GigaSlashState.COMBINE_SKILL)
            {
                if (gigaSlashAnimation.IsDone)
                    curAnimation = GlobalClass.getAnimationByName("SKILL2_SHINE");

                timecount += gameTime.ElapsedGameTime.Milliseconds;
                if (timecount >= 3000)
                {
                    timecount = 0;
                    curState = GigaSlashState.SHOW_COMBINATION;
                }
                isStop = false;
            }

            if (curState == GigaSlashState.SHOW_COMBINATION)
            {

                if (y_combi <= -skillCombination.Height * 0.625f - 200)
                {
                    curState = GigaSlashState.GIGA_ATTACKING;
                    curAnimation = GlobalClass.getAnimationByName("SKILL2_ATTACK");
                    gigaSlashAnimation.PlayAnimation(curAnimation);
                    return;
                }
                if (y_combi <= 50 && !isStop)
                {
                    y_combi -= 3;
                    timecount += gameTime.ElapsedGameTime.Milliseconds;
                    if (timecount >= 2000)
                    {
                        timecount = 0;
                        isStop = true;
                    }
                }
                else
                {
                    y_combi -= 50;
                }
            }

            if (curState == GigaSlashState.GIGA_ATTACKING)
            {

                timecount += gameTime.ElapsedGameTime.Milliseconds;

                if (timecount <= 3700)
                {
                    if (gigaSlashAnimation.isFrameToAction)
                    {
                        //Console.WriteLine("THROW PROJECTILE");
                        float[] directX = { 1, 2, 3, 1.5f, 2.2f, 3.5f, 3, 4, -1 };
                        float[] directY = { 1, 2, -3, -1.5f, 2.2f, -3.5f, 0, 1, -1 };

                        for (int k = 0; k <= 5; k++)
                        {
                            int i = r.Next(0, 9);
                            Projectile projectile = new Projectile(new Vector2(90 + 200,
                                                                                95 + gigaSlashAnimation.Animation.FrameHeight / 2),
                                                                                new Vector2(directX[i], directY[i]), 400);
                            projectiles.Add(projectile);
                        }

                        gigaSlashAnimation.isFrameToAction = false;
                    }
                }
                else
                {
                    curAnimation = GlobalClass.getAnimationByName("SKILL2_SHINE");
                    curState = GigaSlashState.CREATE_PROJECTILE_DONE;
                    timecount = 0;
                }
            }

            if (curState == GigaSlashState.CREATE_PROJECTILE_DONE)
            {
                timecount += gameTime.ElapsedGameTime.Milliseconds;
                if (timecount >= 3500)
                {
                    RoboVisible(player, true);
                    isSkillComplete = true;
                    soundInstance.Stop();
                }
            }

            foreach (Projectile projectile in projectiles)
                projectile.Update(gameTime);

            DamageAllEnemy(waveManager);

            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            skillEffect.Update(s);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            if (!isSkillComplete)
            {
                gigaSlashAnimation.PlayAnimation(curAnimation);
                foreach (Projectile projectile in projectiles)
                    projectile.Draw(gameTime, spriteBatch);

                spriteBatch.End();
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());            //virtualScreen.Draw(spriteBatch);
                GameManager.particlesRender.RenderEffect(skillEffect, ref GameManager.transform);

                switch (curState)
                {
                    case GigaSlashState.ANNOUNCEMENT_SKILL:
                        spriteBatch.Draw(announceNum1, new Vector2(x_num1, y_num1), null, Color.White, 0f, Vector2.Zero, 0.63f, SpriteEffects.None, 0.1f);
                        spriteBatch.Draw(announceNum3, new Vector2(x_num3, y_num3), null, Color.White, 0f, Vector2.Zero, 0.63f, SpriteEffects.None, 0.1f);
                        break;
                    case GigaSlashState.TAKE_ROBO:

                        spriteBatch.Draw(timeHole, new Vector2(0, 80), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.09f);

                        spriteBatch.End();
                        spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, _rasterizerState, 
                            null,GameManager.transform);



                        spriteBatch.GraphicsDevice.ScissorRectangle = clipRect;

                        spriteBatch.Draw(landPair, landPairPos, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.084f);
                        spriteBatch.Draw(skyPair, skyPairPos, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.08f);

                        spriteBatch.End();
                        spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());

                        break;
                    case GigaSlashState.COMBINE_SKILL:
                        gigaSlashAnimation.Draw(gameTime, spriteBatch, new Vector2(150, 190));
                        break;
                    case GigaSlashState.SHOW_COMBINATION:
                        spriteBatch.Draw(skillCombination, new Vector2(x_combi, y_combi), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.1f);
                        break;
                    case GigaSlashState.GIGA_ATTACKING:
                        gigaSlashAnimation.Draw(gameTime, spriteBatch, new Vector2(190, 85));
                        break;
                    case GigaSlashState.CREATE_PROJECTILE_DONE:
                        gigaSlashAnimation.Draw(gameTime, spriteBatch, new Vector2(100, 165));
                        break;
                }
            }
        }

        public void DamageAllEnemy(WaveManager waveManager)
        {
            foreach (Projectile projectile in projectiles)
            {
                for (int i = 0; i < waveManager.wave.monsters.Length; i++)
                {
                    if (waveManager.wave.monsters[i].isAttackable)
                    {
                        if (projectile.getBounding().Intersects(waveManager.wave.monsters[i].getRectangle()) && !projectile.isAttack)
                        {
                            projectile.isAttack = true;
                            projectileSound.Play();
                            skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                            waveManager.wave.monsters[i].curHealth -= projectile.damage;
                            break;
                        }

                        if (projectile.getBounding().Intersects(waveManager.wave.monsters[i].getRectangle()))
                        {
                            projectile.isAttack = false;
                        }
                    }
                }
            }
        }
    }
}