﻿﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class SkillManager
    {
        public List<Skill> skills;

        ContentManager Content;
        Player[] player;
        WaveManager waveManager;

        public bool isUsingSkill;

        public SkillManager(ContentManager Content, Player[] player, WaveManager waveManager)
        {
            this.Content = Content;
            this.player = player;
            this.waveManager = waveManager;
            skills = new List<Skill>();
            isUsingSkill = false;
        }

        public void Update(GameTime gameTime)
        {
            // update skill
            foreach (Skill skill in skills.ToArray())
            {

                //Console.WriteLine("duyet skill");
                if (skill.isSkillComplete)
                {
                    isUsingSkill = false;
                    //Console.WriteLine("da xong");
                    skills.Remove(skill);
                }
                else
                {
                    switch (skill.GetType().Name)
                    {
                        case "LightSword":
                            LightSword lightsword = (LightSword)skill;
                            lightsword.Update(gameTime, player, waveManager);
                            break;
                        case "LightBall":
                            LightBall lightball = (LightBall)skill;
                            lightball.Update(gameTime, waveManager);
                            break;
                        case "HolyHeal":
                            HolyHeal holyHeal = (HolyHeal)skill;
                            holyHeal.Update(gameTime, player);
                            break;
                        case "AquaHorn":
                            AquaHorn aquaHorn = (AquaHorn)skill;
                            aquaHorn.Update(waveManager);
                            break;
                        case "GigaSlash":
                            GigaSlash gigaSlash = (GigaSlash)skill;
                            gigaSlash.Update(gameTime, player, waveManager);
                            break;

                    }
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (Skill skill in skills)
            {
                if (skill.GetType().Name == "LightSword")
                {
                    LightSword ligthsword = (LightSword)skill;
                    ligthsword.Draw(gameTime, spriteBatch);
                }
                else if (skill.GetType().Name == "LightBall")
                {
                    LightBall lightball = (LightBall)skill;
                    lightball.Draw(gameTime, spriteBatch);
                }
                else if (skill.GetType().Name == "GigaSlash")
                {
                    GigaSlash gigaSlash = (GigaSlash)skill;
                    gigaSlash.Draw(gameTime, spriteBatch);
                }
                else if (skill.GetType().Name == "HolyHeal")
                {
                    HolyHeal holyHeal = (HolyHeal)skill;
                    holyHeal.Draw(gameTime, spriteBatch);
                }
                else if (skill.GetType().Name == "AquaHorn")
                {
                    AquaHorn aquaHorn = (AquaHorn)skill;
                    aquaHorn.Draw(gameTime, spriteBatch);
                }
            }

        }


        public void clearAll()
        {
            skills.Clear();
        }
    }
}