﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace RecycleRobo
{
    public class FormManager
    {
        public ContentManager Content;
        public List<FormData> formData = new List<FormData>();
        
        public SkillTileManager skillTileManager;
        public List<ElementForm> elements = new List<ElementForm>();

        public Texture2D selectedTile;
        private ElementForm currentForm;
        public ElementForm CurrentForm
        {
            get { return currentForm;  }
        }
        
        public Texture2D circleTexture;
        public Vector2 circlePosition;
        public Vector2[] positionForm = new Vector2[4];
        public Vector2 clickPosition;

        public bool isShowForm = false;

        private MouseState curState, prevState;

        public FormManager(ContentManager Content, List<FormData> formData, int numberOfForm, ElementForm curForm)
        {
            this.Content = Content;
            this.currentForm = curForm;
            this.formData = formData;
            skillTileManager = new SkillTileManager(Content);
            initListDeltaPosition(numberOfForm);
            initElementsForm(numberOfForm);
            selectedTile = Asset.selectedTile;
        }

        public void initListDeltaPosition(int numberOfForm)
        {

            for (int i = 0; i < 4; i++ )
            {
                positionForm[i] = new Vector2(70 + i*170, 10);
            }
        }

        public void initElementsForm(int numberOfForm)
        {
            for (int i = 0; i < numberOfForm; i++)
            {
                FormData data = formData.ElementAt(i);
                ElementForm tmp = null;

                if (data.typeForm == "balance1")
                {
                    tmp = new BalanceForm(Content, data.typeForm, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "balance4")
                {
                    tmp = new BalanceForm(Content, data.typeForm, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "thorn1")
                {
                    tmp = new StramoniumThornForm(Content, data.typeForm, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "thorn5")
                {
                    tmp = new StramoniumThornForm(Content, data.typeForm, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "seal")
                {
                    tmp = new OceanSealForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "fire")
                {
                    tmp = new VolcanoFireForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "speed")
                {
                    tmp = new SpeedEagleForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "phoenix")
                {
                    tmp = new PhoenixForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "angerbear")
                {
                    tmp = new AngerBearForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "leafCut")
                {
                    tmp = new LeafCutterForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "iceFang")
                {
                    tmp = new IceFangForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "greeHerb")
                {
                    tmp = new GreeHerbForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "shield")
                {
                    tmp = new GreenShieldForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "wave")
                {
                    tmp = new BlueWaveForm(Content, positionForm[i], data.bonus);
                }
                else if (data.typeForm == "steam")
                {
                    tmp = new HotSteamForm(Content, positionForm[i], data.bonus);
                }
                else
                {
                    skillTileManager.createTileSkill(data.typeForm, positionForm[i]);
                } 

                if (tmp!=null)
                {
                    elements.Add(tmp);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gametime)
        {
            foreach (ElementForm e in elements)
                e.Draw(spriteBatch, gametime);
            
            if (isShowForm)
            {
                spriteBatch.Draw(selectedTile, currentForm.Position + new Vector2 (-30, -30), null, Color.White, 
                    0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.16f);
                skillTileManager.Draw(spriteBatch);
                foreach (ElementForm e in elements)
                {
                    e.Draw(spriteBatch);
                    if (e.iconTile.tileState == IconTile.TileState.DETAIL) 
                        e.drawGestureAnimation(spriteBatch, gametime);
                }
            }
        }

        public void Update(GameTime gameTime, bool isRoboSelected)
        {
            curState = Mouse.GetState();

            if (isRoboSelected == true) isShowForm = true;
            else isShowForm = false;

            if (isShowForm)
            {
                Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
                foreach (ElementForm e in elements)
                {
                    e.setHover(mousePoint);
                }
            }

            if (curState.LeftButton == ButtonState.Released && prevState.LeftButton == ButtonState.Pressed && isShowForm)
            {
                skillTileManager.isActive = true;
                foreach (ElementForm e in elements)
                {
                    if (e.isHover) 
                    {
                        currentForm.reset();
                        currentForm = e;
                    }
                    e.isHover = false;
                }
            }

            foreach (ElementForm e in elements) e.updateTile(gameTime);

            skillTileManager.Update(gameTime);
            skillTileManager.isActive = false;

            prevState = curState;
        }
    }
}
