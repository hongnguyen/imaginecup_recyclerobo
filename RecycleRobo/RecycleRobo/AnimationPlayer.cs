﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;

namespace RecycleRobo
{
    public struct AnimationPlayer
    {
        Animation animation;
        public Animation Animation
        {
            get { return animation; }
        }

        int frameindex;
        public int FrameIndex
        {
            get { return frameindex; }
        }

        public int FrameToAction;
        public bool isFrameToAction;

        public Vector2 Original
        {
            get { return new Vector2(0, 0); }
        }

        float scale;
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        private float time;

        SpriteEffects spriteEffect;
        public SpriteEffects SpriteEffect
        {
            set { spriteEffect = value; }
            get { return spriteEffect; }
        }

        float orderLayer;
        public float OrderLayer
        {
            set { orderLayer = value; }
            get { return orderLayer; }
        }

        bool isDone;
        public bool IsDone
        {
            get { return isDone; }
        }

        public Color color;

        public void PlayAnimation(Animation animation)
        {
            if (Animation == animation)
                return;

            this.animation = animation;
            this.frameindex = 0;
            this.time = 0.0f;
            isDone = false;
            isFrameToAction = false;
        }

        public void Draw(GameTime gametime, SpriteBatch spriteBatch, Vector2 Position)
        {
            if (!GlobalClass.isPause)
            {
                if (Animation == null)
                {
                    return;
                }

                time += (float)gametime.ElapsedGameTime.TotalSeconds;
                while (time > Animation.FrameTime)
                {
                    time -= Animation.FrameTime;
                    if (Animation.IsLooping)
                    {
                        frameindex = (frameindex + 1) % Animation.FrameCount;
                        if (frameindex == FrameToAction)
                        {
                            isFrameToAction = true;
                        }
                    }
                    else
                    {
                        frameindex = Math.Min(frameindex + 1, Animation.FrameCount - 1);
                        if (frameindex >= Animation.FrameCount - 1)
                        {
                            isDone = true;
                        }
                    }
                }
            }
            Rectangle rectTMP = new Rectangle((int)Position.X, (int)Position.Y,(int) (Animation.frameWidthDXT*Scale), (int)(Animation.frameHeightDXT*Scale));
            spriteBatch.Draw(Animation.Texture, rectTMP, Animation.ListFrame[frameindex], color, 0.0f, Original, spriteEffect, orderLayer);
        }

        public void DrawRotate(GameTime gametime, SpriteBatch spriteBatch, Vector2 Position, float rotate)
        {
            if (!GlobalClass.isPause)
            {
                if (Animation == null)
                {
                    return;
                }

                time += (float)gametime.ElapsedGameTime.TotalSeconds;
                while (time > Animation.FrameTime)
                {
                    time -= Animation.FrameTime;
                    if (Animation.IsLooping)
                    {
                        frameindex = (frameindex + 1) % Animation.FrameCount;
                        if (frameindex == FrameToAction)
                        {
                            isFrameToAction = true;
                        }
                    }
                    else
                    {
                        frameindex = Math.Min(frameindex + 1, Animation.FrameCount - 1);
                        if (frameindex >= Animation.FrameCount - 1)
                        {
                            isDone = true;
                        }
                    }
                }
            }
            spriteBatch.Draw(Animation.Texture, Position, Animation.ListFrame[frameindex], color, rotate, Original, Scale, spriteEffect, orderLayer);
        }
    }
}